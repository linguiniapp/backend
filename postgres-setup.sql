create database linguinidb;
\c linguinidb
create schema app;

create user linguiniapp with password 'asd';
alter user linguiniapp set search_path=app;

revoke all on database linguinidb from public;

grant connect on database linguinidb to linguiniapp;
grant create, usage on schema app to linguiniapp;
alter default privileges for user linguiniapp in schema app grant select, insert, update on tables to linguiniapp;
