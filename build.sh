#!/bin/bash

echo "Instalando herramientas para entorno virtual de Python"
sudo apt-get -qy install virtualenv

echo "Creando entorno virtual de Python"

virtualenv -p /usr/bin/python3 python_virtualenv

echo "Activando entorno virtual"
source ./python_virtualenv/bin/activate

echo "Instalando dependencias"
pip install -r src/requirements.txt
pip install gunicorn

echo "Desactivando entorno virtual"
deactivate

echo "Preparando script de activación"
cp activate_base activate
sed -i "s:\@path:$(pwd):" activate
echo "alias linguiniapp='. $(pwd)/activate'" >> ~/.bashrc

echo "Construyendo contenedor de base de datos"
sudo docker rm -f linguini-db > /dev/null
sudo docker run --read-only --name linguini-db --tmpfs /run/postgresql --tmpfs /tmp -v linguinipostgres:/var/lib/postgresql/data -p 5432:5432 -e POSTGRES_PASSWORD=postgres -d postgres:10-alpine

until sudo docker exec linguini-db sh -c 'psql -h localhost -U postgres -c "select 1" >/dev/null 2>&1'
do
  echo "Esperando al contenedor de base de datos..."
  sleep 1
done

echo "Configurando esquema y permisos"
cat postgres-setup.sql | sudo docker exec -i linguini-db sh -c 'cat > /tmp/linguini-setup.sql'
sudo docker exec linguini-db sh -c 'PGPASSWORD=postgres psql -U postgres -f /tmp/linguini-setup.sql'

echo "Construyendo contenedor de pgadmin"
sudo docker rm -f linguini-pgadmin
sudo docker create --name linguini-pgadmin -e PGADMIN_DEFAULT_EMAIL=dbadmin@linguiniapp.com -e PGADMIN_DEFAULT_PASSWORD=asd -p 8080:80 --link linguini-db:db dpage/pgadmin4

docker stop linguini-db
