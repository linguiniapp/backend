.. LingüiniApp documentation master file, created by
   sphinx-quickstart on Tue Oct  9 21:38:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentación de LingüiniApp
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: exp_pedido

=========================
Clases
=========================
.. autoclass:: ExpPedido
    :members:
