from datetime import timedelta, datetime

from modelo.entidades import Mesa, EstadoMesa
from modelo.entidades import ReservaEstado, EstadoReserva, Reserva
from modelo.persistencia import Persistencia


class ExpMesa:

    @staticmethod
    def get_mesa(sucursal, numero):
        estado_disponible = Persistencia.get_session().query(EstadoMesa)\
            .filter(EstadoMesa.nombre == 'disponible').one()
        # return Persistencia.get_session().query(Mesa)\
        #     .join(Mesa.sector, aliased=True)\
        #     .filter_by(sucursal=sucursal)\
        #     .filter(Mesa.numero == numero, Mesa.estado == estado_disponible)\
        #     .one_or_none()
        return Persistencia.get_session().query(Mesa)\
            .filter(Mesa.sucursal == sucursal,
                    Mesa.numero == numero,
                    Mesa.estado == estado_disponible).one_or_none()

    @staticmethod
    def check_mesa_disponible(mesas, fecha_hora):
        # estado_reserva_solicitada = Persistencia.get_session()\
        #     .query(EstadoReserva)\
        #     .filter(EstadoReserva.nombre == 'solicitada').one()

        for mesa in mesas:
            if not mesa.reservable:
                return False

        estado_reserva_anulada = Persistencia.get_session()\
            .query(EstadoReserva)\
            .filter(EstadoReserva.nombre == 'anulada').one()

        # TODO: Parametrizar en base de datos
        tiempo_reserva = timedelta(hours=1)

        historico_reservas_solicitadas = Persistencia.get_session()\
            .query(ReservaEstado).join(ReservaEstado.reserva, aliased=True)\
            .filter(Reserva.fecha_hora < (fecha_hora + tiempo_reserva),
                    Reserva.fecha_hora > (fecha_hora - tiempo_reserva)).all()

        reservas_anuladas = []
        for reserva_estado in historico_reservas_solicitadas:
            if reserva_estado.reserva in reservas_anuladas:
                continue
            if reserva_estado.estado == estado_reserva_anulada:
                reservas_anuladas.append(reserva_estado.reserva)

        reservas_vigentes = []
        for re in historico_reservas_solicitadas:
            if re.reserva not in reservas_anuladas:
                reservas_vigentes.append(re.reserva)

        for rv in reservas_vigentes:
            for mesa in mesas:
                if mesa in rv.mesas:
                    return False

        return True

    @staticmethod
    def ocupar_mesa(mesa):
        estado_ocupada = Persistencia.get_session().query(EstadoMesa)\
            .filter(EstadoMesa.nombre == 'ocupada').one()
        mesa.estado = estado_ocupada
        Persistencia.get_session().add(mesa)

    @staticmethod
    def liberar_mesa(mesa):
        estado_disponible = Persistencia.get_session().query(EstadoMesa)\
            .filter(EstadoMesa.nombre == 'disponible').one()
        mesa.estado = estado_disponible
        Persistencia.get_session().add(mesa)

    @staticmethod
    def crear_mesa(numero, posicion_x, posicion_y,
                   capacidad, sucursal, reservable):
        estado_mesa = Persistencia.get_session()\
            .query(EstadoMesa).filter(EstadoMesa.nombre == 'disponible').one()

        # ultimo = Persistencia.get_session().query(Mesa)\
        #     .join(Mesa.sector, aliased=True)\
        #     .filter_by(sucursal=sector.sucursal)\
        #     .order_by(Mesa.numero.desc()).first()

        ultimo = Persistencia.get_session().query(Mesa)\
            .filter(Mesa.sucursal == sucursal)\
            .order_by(Mesa.numero.desc()).first()

        if numero is None:
            if ultimo is None:
                numero = 1
            else:
                numero = ultimo.numero + 1
        else:
            mesa_existente = Persistencia.get_session()\
                .query(Mesa).filter(Mesa.sucursal == sucursal,
                                    Mesa.numero == numero).one_or_none()
            if mesa_existente is not None:
                mesa, ok = ExpMesa.modificar_mesa(sucursal, mesa_existente,
                                                  None, capacidad,
                                                  None, reservable,
                                                  posicion_x, posicion_y)
                if ok:
                    return mesa, False
                else:
                    return None, False

        mesa = Mesa(numero=numero,
                    capacidad_personas=capacidad,
                    estado=estado_mesa,
                    sucursal=sucursal,
                    posicion_x=posicion_x,
                    posicion_y=posicion_y,
                    reservable=reservable)

        Persistencia.get_session().add(mesa)
        Persistencia.get_session().commit()

        return mesa, True

    @staticmethod
    def modificar_mesa(sucursal, mesa, numero_mesa, capacidad,
                       estado, reservable, posicion_x, posicion_y):
        if numero_mesa is not None:
            mesa_existente = ExpMesa.get_mesa(sucursal, numero_mesa)
            if mesa_existente is not None:
                return mesa, False
            else:
                mesa.numero = numero_mesa

        if capacidad is not None:
            mesa.capacidad_personas = capacidad

        if estado is not None:
            estado_mesa = Persistencia.get_session().query(EstadoMesa)\
                .filter(EstadoMesa.nombre == estado).one_or_none()
            if estado_mesa is not None:
                mesa.estado = estado_mesa
            else:
                return mesa, False

        if reservable is not None:
            mesa.reservable = reservable

        if posicion_x is not None:
            mesa.posicion_x = posicion_x

        if posicion_y is not None:
            mesa.posicion_y = posicion_y

        Persistencia.get_session().add(mesa)
        Persistencia.get_session().commit()

        return mesa, True

    @staticmethod
    def eliminar_mesa(mesa):
        if mesa.estado.nombre == 'disponible':
            if mesa.reservable and Persistencia.query(Reserva).filter(
                    Reserva.fecha_hora >= datetime.now(),
                    Reserva.mesas.contains(mesa)).all():
                return 409
            else:
                Persistencia.delete(mesa)
                if Persistencia.commit():
                    return 0
                else:
                    return 500
        else:
            return 409
