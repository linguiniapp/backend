from base64 import b64encode
from datetime import datetime
from os.path import abspath
from time import sleep

from modelo.db_init import db_check, db_reset
from modelo.entidades import *
from modelo.persistencia import Persistencia


def load_image(name, index=0, prefix='data:image/png;base64,', suffix='.png'):
    base = abspath('test_data/img') + '/'
    filename = name + ('_' + str(index) if index else '') + suffix

    with open(base + filename, 'rb') as imgFile:
        return prefix + b64encode(imgFile.read()).decode()


def test_data():
    print("Cargando datos de prueba...")

    mesa_habilitada = Persistencia.query(EstadoMesa)\
        .filter(EstadoMesa.nombre == 'disponible').one()

    sucursal_abierta = Persistencia.query(EstadoSucursal)\
        .filter(EstadoSucursal.nombre == 'abierta').one()
    sucursal_cerrada = Persistencia.query(EstadoSucursal)\
        .filter(EstadoSucursal.nombre == 'cerrada').one()

    producto_disponible = Persistencia.query(EstadoProducto)\
        .filter(EstadoProducto.nombre == 'disponible').one()

    carta_habilitada = Persistencia.query(EstadoCarta)\
        .filter(EstadoCarta.nombre == 'habilitada').one()

    cartaproducto_enstock = Persistencia.query(EstadoCartaProducto)\
        .filter(EstadoCartaProducto.nombre == 'en stock').one()

    data = {}
    data['restaurante'] = Restaurante(
        cuit="00-00000000-0",
        descripcion="Restaurante de ejemplo para LingüiniApp",
        nombre="Lingüini Restó",
        foto=load_image('restaurante')
    )
    data['sucursal_1'] = Sucursal(
        numero=2,
        nombre='Sucursal Lingüini Uno',
        direccion='Martinez de Rozas 215, Mendoza, Mendoza',
        latitud=-32.897915,
        longitud=-68.856686,
        foto=load_image('sucursal', index=1),
        estado=sucursal_abierta,
        restaurante=data['restaurante'],
        puntuacion='1:2:3:4:5'
    )
    data['sucursal_1_mesa_1'] = Mesa(
        numero=1,
        capacidad_personas=4,
        reservable=True,
        posicion_x=25,
        posicion_y=20,
        estado=mesa_habilitada,
        sucursal=data['sucursal_1']
    )
    data['sucursal_1_mesa_2'] = Mesa(
        numero=2,
        capacidad_personas=4,
        reservable=True,
        posicion_x=35,
        posicion_y=20,
        estado=mesa_habilitada,
        sucursal=data['sucursal_1']
    )
    data['sucursal_1_mesa_3'] = Mesa(
        numero=3,
        capacidad_personas=4,
        reservable=True,
        posicion_x=35,
        posicion_y=30,
        estado=mesa_habilitada,
        sucursal=data['sucursal_1']
    )
    data['sucursal_2'] = Sucursal(
        numero=1,
        nombre='Sucursal Lingüini Dos',
        direccion='Olascoaga 350, Mendoza, Mendoza',
        latitud=-32.896506,
        longitud=-68.855393,
        hora_inicio_reserva=datetime.strptime('20:30', '%H:%M').time(),
        hora_fin_reserva=datetime.strptime('23:30', '%H:%M').time(),
        dias_reservables=WeekMask(
            lunes=False,
            martes=True,
            miercoles=True,
            jueves=True,
            viernes=False,
            sabado=False,
            domingo=True
        ),
        telefono='0261-1234567',
        info_contacto='Facebook: /linguiniapp\nTwitter: @LinguiniApp',
        foto=load_image('sucursal', index=2),
        estado=sucursal_cerrada,
        restaurante=data['restaurante'],
        puntuacion='3:2:5:26:13'
    )
    data['sucursal_2_mesa_1'] = Mesa(
        numero=1,
        capacidad_personas=4,
        reservable=True,
        posicion_x=35,
        posicion_y=20,
        estado=mesa_habilitada,
        sucursal=data['sucursal_2']
    )
    data['categoria_carnes'] = CategoriaProducto(
        codigo=1,
        nombre='Carnes',
        foto=load_image('categoria', 1)
    )
    data['categoria_ensaladas'] = CategoriaProducto(
        codigo=2,
        nombre='Ensaladas',
        foto=load_image('categoria', 2)
    )
    data['categoria_pastas'] = CategoriaProducto(
        codigo=3,
        nombre='Pastas',
        foto=load_image('categoria', 3)
    )
    data['categoria_pizza'] = CategoriaProducto(
        codigo=4,
        nombre='Pizza',
        foto=load_image('categoria', 4)
    )
    data['categoria_postres'] = CategoriaProducto(
        codigo=5,
        nombre='Postres',
        foto=load_image('categoria', 5)
    )
    data['categoria_jugos'] = CategoriaProducto(
        codigo=6,
        nombre='Jugos',
        foto=load_image('categoria', 6)
    )
    data['categoria_vinos'] = CategoriaProducto(
        codigo=7,
        nombre='Vinos',
        foto=load_image('categoria', 7)
    )
    data['producto_bife_de_chorizo'] = Producto(
        codigo=1,
        nombre='Bife de chorizo',
        descripcion='Bife de chorizo condimentado con'
                    'pimienta y finas hierbas',
        estado=producto_disponible,
        categorias=[data['categoria_carnes']],
        foto=load_image('producto', 1)
    )
    data['producto_flan'] = Producto(
        codigo=2,
        nombre='Flan',
        descripcion='Flan de huevo casero con caramelo',
        estado=producto_disponible,
        categorias=[data['categoria_postres']],
        foto=load_image('producto', 2)
    )
    data['producto_vino_tinto_cabernet'] = Producto(
        codigo=3,
        nombre='Cabernet Sauvignon',
        descripcion='Vino tinto, variedad Cabernet Sauvignon, cosecha 2006',
        estado=producto_disponible,
        categorias=[data['categoria_vinos']],
        foto=load_image('producto', 3)
    )
    data['producto_vino_blanco_chardonnay'] = Producto(
        codigo=4,
        nombre='Chardonnay',
        descripcion='Vino blanco, variedad Chardonnay',
        estado=producto_disponible,
        categorias=[data['categoria_vinos']],
        foto=load_image('producto', 4)
    )
    data['carta_producto_bife_de_chorizo'] = CartaProducto(
        producto=data['producto_bife_de_chorizo'],
        estado=cartaproducto_enstock,
        precio=250.00
    )
    data['carta_producto_flan'] = CartaProducto(
        producto=data['producto_flan'],
        estado=cartaproducto_enstock,
        precio=50.00
    )
    data['carta_producto_vino_tinto_cabernet'] = CartaProducto(
        producto=data['producto_vino_tinto_cabernet'],
        estado=cartaproducto_enstock,
        precio=650.00
    )
    data['carta_producto_vino_blanco_chardonnay'] = CartaProducto(
        producto=data['producto_vino_blanco_chardonnay'],
        estado=cartaproducto_enstock,
        precio=400.00
    )
    data['carta_1'] = Carta(
        numero=1,
        estado=carta_habilitada,
        carta_productos=[
            data['carta_producto_bife_de_chorizo'],
            data['carta_producto_flan'],
            data['carta_producto_vino_tinto_cabernet'],
            data['carta_producto_vino_blanco_chardonnay']
        ]
    )
    data['sucursal_1_oferta'] = SucursalOferta(
        fecha_desde=datetime.now(),
        sucursal=data['sucursal_1'],
        carta=data['carta_1']
    )
    data['sucursal_2_oferta'] = SucursalOferta(
        fecha_desde=datetime.now(),
        sucursal=data['sucursal_2'],
        carta=data['carta_1']
    )

    [Persistencia.add(d) for d in data.values()]
    if Persistencia.commit():
        print("Datos cargados!")
    else:
        print("Error al cargar los datos")


def main():
    Persistencia.init()
    while db_check():
        print('Esperando base de datos...')
        sleep(3)

    t = db_reset(True)
    t.join()
    test_data()


if __name__ == '__main__':
    main()
