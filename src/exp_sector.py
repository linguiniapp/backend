# from modelo.entidades import Sector, EstadoSector
# from modelo.persistencia import Persistencia


# class ExpSector:
#
#     @staticmethod
#     def get_sector(sucursal, numero):
#         if numero is None:
#             sector = Persistencia.get_session().query(Sector).filter(Sector.sucursal == sucursal).first()
#         else:
#             sector = Persistencia.get_session().query(Sector).filter(Sector.sucursal == sucursal, Sector.numero == numero).one_or_none()
#
#         return sector
#
#     @staticmethod
#     def crear_sector(sucursal, piso, vertices):
#
#         ultimo = Persistencia.get_session().query(Sector) \
#             .filter(Sector.sucursal == sucursal) \
#             .order_by(Sector.numero.desc()).first()
#
#         if ultimo is None:
#             numero = 1
#         else:
#             numero = ultimo.numero + 1
#
#         estado_disponible = Persistencia.get_session().query(EstadoSector).filter(EstadoSector.nombre == 'disponible').one()
#         sector = Sector(numero=numero, piso=piso, estado=estado_disponible, sucursal=sucursal)
#
#         vert_string = ''
#         if vertices is not None:
#             for v in vertices:
#                 vert_string += str(v.x) + ',' + str(v.y) + '-'
#             vert_string = vert_string[:-1]
#             sector.vertices = vert_string
#
#         Persistencia.get_session().add(sector)
#         Persistencia.get_session().commit()
#
#         return True, sector
