from datetime import datetime
from random import randint

from sqlalchemy import not_

from auth.exp_auth import ExpAuth
from modelo.entidades import Empleado, EmpleadoEstado, EstadoEmpleado
from modelo.persistencia import Persistencia


class ExpEmpleado:

    @staticmethod
    def get_empleado(numero_o_cuil):
        try:
            numero = int(numero_o_cuil)
            empleado = Persistencia.query(Empleado)\
                .filter(Empleado.numero == numero).one_or_none()
        except ValueError:
            cuil = numero_o_cuil
            empleado = Persistencia.query(Empleado)\
                .filter(Empleado.cuil == cuil).one_or_none()

        return empleado or None

    @staticmethod
    def get_empleados(sucursal, activos):
        if sucursal is None:
            if activos is None:
                empleados = Persistencia.query(Empleado).all()
            elif activos:
                estado_suspendido = Persistencia.query(EstadoEmpleado)\
                    .filter(EstadoEmpleado.nombre == 'suspendido').one()
                estado_deshabilitado = Persistencia.query(EstadoEmpleado)\
                    .filter(EstadoEmpleado.nombre == 'deshabilitado').one()
                empleados = Persistencia.query(Empleado) \
                    .join(Empleado.historico_estados, aliased=True) \
                    .filter(not_(Empleado.historico_estados.contains(
                                EmpleadoEstado.estado == estado_suspendido)),
                            not_(Empleado.historico_estados.contains(
                                EmpleadoEstado.estado == estado_deshabilitado))
                            ).all()
            else:
                estado_habilitado = Persistencia.query(EstadoEmpleado)\
                    .filter(EstadoEmpleado.nombre == 'activo').one()
                empleados = Persistencia.query(Empleado) \
                    .join(Empleado.historico_estados, aliased=True) \
                    .filter(Empleado.sucursal == sucursal,
                            not_(Empleado.historico_estados.contains(
                                EmpleadoEstado.estado == estado_habilitado))
                            ).all()
        else:
            if activos is None:
                empleados = Persistencia.query(Empleado) \
                    .filter(Empleado.sucursal == sucursal).all()
            elif activos:
                estado_suspendido = Persistencia.query(EstadoEmpleado)\
                    .filter(EstadoEmpleado.nombre == 'suspendido').one()
                estado_deshabilitado = Persistencia.query(EstadoEmpleado)\
                    .filter(EstadoEmpleado.nombre == 'deshabilitado').one()
                empleados = Persistencia.query(Empleado) \
                    .join(Empleado.historico_estados, aliased=True) \
                    .filter(Empleado.sucursal == sucursal,
                            not_(Empleado.historico_estados.contains(
                                EmpleadoEstado.estado == estado_suspendido)),
                            not_(Empleado.historico_estados.contains(
                                EmpleadoEstado.estado == estado_deshabilitado))
                            ).all()
            else:
                estado_habilitado = Persistencia.query(EstadoEmpleado)\
                    .filter(EstadoEmpleado.nombre == 'habilitado').one()
                empleados = Persistencia.query(Empleado) \
                    .join(Empleado.historico_estados, aliased=True) \
                    .filter(Empleado.sucursal == sucursal,
                            not_(Empleado.historico_estados.contains(
                                EmpleadoEstado.estado == estado_habilitado))
                            ).all()

        if empleados is not None:
            return empleados, 0
        else:
            return [], 404

    @staticmethod
    def crear_empleado(sucursal, nombre, apellido, sexo,
                       email, telefono, fecha_nac, cuil):
        estado_habilitado = Persistencia.query(EstadoEmpleado)\
            .filter(EstadoEmpleado.nombre == 'activo').one()

        if email is None:
            email = '@'.join(['.'.join([nombre, apellido]), 'linguiniapp.com'])

        ultimo = Persistencia.query(Empleado)\
            .order_by(Empleado.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        empleado = Empleado(
            numero=numero,
            nombre=nombre,
            apellido=apellido,
            email=email,
            cuil=cuil,
            tipo='empleado',
            sucursal=sucursal
        )

        empleado_estado = EmpleadoEstado(
            fecha_hora=datetime.now(),
            descripcion='Alta de empleado',
            estado=estado_habilitado,
            empleado=empleado
        )

        if sexo is not None:
            empleado.sexo = sexo

        if telefono is not None:
            empleado.telefono = telefono

        if fecha_nac is not None:
            empleado.fecha_nacimiento = fecha_nac

        Persistencia.add(empleado)
        Persistencia.add(empleado_estado)

        result = ExpAuth.crear_usuario_empleado(empleado)

        if result:
            return None, result

        if Persistencia.commit():
            return empleado, 0
        else:
            return None, 500

    @staticmethod
    def modificar_empleado(empleado, sucursal, nombre, apellido, sexo,
                           email, telefono, fecha_nac, cuil):

        if sucursal != empleado.sucursal:
            empleado.sucursal = sucursal

        if nombre != empleado.nombre:
            empleado.nombre = nombre

        if apellido != empleado.apellido:
            empleado.apellido = apellido

        if sexo != empleado.sexo:
            empleado.sexo = sexo

        if email != empleado.email:
            empleado.email = email

        if telefono != empleado.telefono:
            empleado.telefono = telefono

        if fecha_nac != empleado.fecha_nacimiento:
            empleado.fecha_nacimiento = fecha_nac

        if cuil != empleado.cuil:
            empleado_existente = Persistencia.query(Empleado)\
                .filter(Empleado.cuil == cuil).one_or_none()
            if empleado_existente is not None:
                return empleado, 409
            else:
                empleado.cuil = cuil

        Persistencia.add(empleado)

        if Persistencia.commit():
            return empleado, 0
        else:
            return None, 500

    @staticmethod
    def habilitar_empleado(empleado, comentario):
        estado = Persistencia.query(EstadoEmpleado)\
            .filter(EstadoEmpleado.nombre == 'activo').one()

        empleado_estado = EmpleadoEstado(
            fecha_hora=datetime.now(),
            descripcion=comentario,
            estado=estado,
            empleado=empleado
        )

        Persistencia.add(empleado_estado)

        if Persistencia.commit():
            return empleado, 0
        else:
            return empleado, 500

    @staticmethod
    def deshabilitar_empleado(empleado, comentario):
        estado = Persistencia.query(EstadoEmpleado)\
            .filter(EstadoEmpleado.nombre == 'deshabilitado').one()

        empleado_estado = EmpleadoEstado(
            fecha_hora=datetime.now(),
            descripcion=comentario,
            estado=estado,
            empleado=empleado
        )

        Persistencia.add(empleado_estado)

        if Persistencia.commit():
            return empleado, 0
        else:
            return empleado, 500

    @staticmethod
    def suspender_empleado(empleado, comentario):
        estado = Persistencia.query(EstadoEmpleado)\
            .filter(EstadoEmpleado.nombre == 'suspendido').one()

        empleado_estado = EmpleadoEstado(
            fecha_hora=datetime.now(),
            descripcion=comentario,
            estado=estado,
            empleado=empleado
        )

        Persistencia.add(empleado_estado)

        if Persistencia.commit():
            return empleado, 0
        else:
            return empleado, 500

    @staticmethod
    def asignar_mozo(estadia, empleado=None):
        if not empleado or empleado.tipo != 'empleado':
            empleado = None

        acciones_mozo = [
            ExpAuth.get_accion_por_nombre('ver_pedidos'),
            ExpAuth.get_accion_por_nombre('pedido_presencial'),
            ExpAuth.get_accion_por_nombre('ver_clientes'),
            ExpAuth.get_accion_por_nombre('anular_pedido'),
        ]

        empleado_valido = True
        if empleado is not None:
            for accion in acciones_mozo:
                if accion not in empleado.rol.acciones:
                    empleado_valido = False
                    break
        else:
            empleado_valido = False

        if empleado_valido:
            estadia.mozo = empleado
        else:
            # Asigna un mozo al azar
            estado_activo = Persistencia.query(EstadoEmpleado)\
                .filter(EstadoEmpleado.nombre == 'activo').one()
            empleado_estados = Persistencia.query(EmpleadoEstado)\
                .order_by(EmpleadoEstado.fecha_hora.desc()).all()

            empleados_validos = []
            for empleado_estado in empleado_estados:
                if empleado_estado == estado_activo:
                    empleado = empleado_estado.empleado
                    empleado_valido = False
                    for accion in acciones_mozo:
                        if accion not in empleado.rol.acciones:
                            empleado_valido = False
                            break
                    if empleado_valido:
                        empleados_validos.append(empleado)

            if empleados_validos:
                estadia.mozo = empleados_validos[
                    randint(0, len(empleados_validos))]
            else:
                return None

        return estadia.mozo
