from exp_producto import ExpProducto
from exp_sucursal import ExpSucursal
from modelo.entidades import Carta, EstadoCarta
from modelo.entidades import CartaProducto, EstadoCartaProducto
from modelo.persistencia import Persistencia


class ExpCarta:

    @staticmethod
    def get_carta(numero_carta):
        return Persistencia.get_session().query(Carta)\
            .filter(Carta.numero == numero_carta).one_or_none()

    @staticmethod
    def ver_carta(sucursal):
        oferta = ExpSucursal.get_oferta_actual(sucursal)

        if oferta is None:
            return None

        carta = oferta.carta

        estado_carta_habilitada = Persistencia.get_session()\
            .query(EstadoCarta)\
            .filter(EstadoCarta.nombre == 'habilitada').one()

        if carta.estado == estado_carta_habilitada:
            # return carta, True
            return carta
        else:
            # return carta, False
            return Carta()

    # @staticmethod
    # def ver_detalle_producto(sucursal, producto):
    #     carta = ExpCarta.ver_carta(sucursal)
    #     # estado_enstock = Persistencia.get_session()\
    #     # .query(EstadoCartaProducto)\
    #     # .filter(EstadoCartaProducto.nombre == 'en stock').one()
    #
    #     # carta_producto = Persistencia.get_session().query(CartaProducto)\
    #     #     .filter(CartaProducto.carta == carta,
    #     #             CartaProducto.producto == producto,
    #     #             CartaProducto.estado == estado_enstock).one_or_none()
    #
    #     carta_producto = Persistencia.get_session().query(CartaProducto)\
    #         .filter(CartaProducto.carta == carta,
    #                 CartaProducto.producto == producto).one_or_none()
    #
    #     return carta_producto

    @staticmethod
    def ver_producto_carta(carta, producto):
        return Persistencia.get_session().query(CartaProducto)\
            .filter(CartaProducto.carta == carta,
                    CartaProducto.producto == producto).one_or_none()

    @staticmethod
    def ver_productos_carta(carta, categorias):
        productos_categoria = ExpProducto.ver_productos(categorias)

        lista_producto_carta = []
        for producto in productos_categoria:
            producto_carta = ExpCarta.ver_producto_carta(carta, producto)
            if producto_carta is not None:
                lista_producto_carta.append(producto_carta)

        return lista_producto_carta

    @staticmethod
    def crear_carta(lista_producto_precio):
        ultimo = Persistencia.get_session().query(Carta)\
            .order_by(Carta.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        estado_habilitada = Persistencia.get_session().query(EstadoCarta)\
            .filter(EstadoCarta.nombre == 'habilitada').one()

        carta = Carta(numero=numero, estado=estado_habilitada)

        estado_carta_prod = Persistencia.get_session()\
            .query(EstadoCartaProducto)\
            .filter(EstadoCartaProducto.nombre == 'en stock').one()

        productos_agregados = []
        for producto, precio in lista_producto_precio:
            if producto.codigo in productos_agregados:
                continue
            else:
                productos_agregados.append(producto.codigo)
            carta_prod = CartaProducto(
                producto=producto,
                precio=precio,
                estado=estado_carta_prod,
                carta=carta
            )
            Persistencia.get_session().add(carta_prod)

        Persistencia.get_session().add(carta)

        if Persistencia.commit():
            return carta, True
        else:
            return None, False
