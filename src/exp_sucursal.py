from datetime import datetime
from decimal import Decimal

from sqlalchemy import or_

from modelo.entidades import Caja
from modelo.entidades import Sucursal, EstadoSucursal, WeekMask, SucursalOferta
from modelo.persistencia import Persistencia


class ExpSucursal:

    @staticmethod
    def get_sucursal(numero_sucursal):
        return Persistencia.get_session().query(Sucursal)\
            .filter(Sucursal.numero == numero_sucursal).one_or_none()

    @staticmethod
    def get_lista_sucursales(restaurante):
        lista = Persistencia.get_session().query(Sucursal)\
            .filter(Sucursal.restaurante == restaurante)\
            .order_by(Sucursal.numero.asc()).all()

        return lista

    @staticmethod
    def ver_caja_activa(sucursal):
        return Persistencia.query(Caja)\
            .filter(Caja.sucursal == sucursal,
                    Caja.fecha_hora_cierre.is_(None)).one_or_none()

    @staticmethod
    def crear_sucursal(nombre, direccion, restaurante, latitud, longitud,
                       telefono, info_contacto, foto, dias_reservables,
                       hora_inicio_reserva, hora_fin_reserva):
        ultimo = Persistencia.get_session().query(Sucursal)\
            .order_by(Sucursal.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        if Persistencia.get_session().query(Sucursal)\
                .filter(Sucursal.restaurante == restaurante,
                        Sucursal.nombre == nombre)\
                .one_or_none():
            return None, False

        estado = Persistencia.get_session().query(EstadoSucursal)\
            .filter(EstadoSucursal.nombre == 'cerrada').one()
        try:
            sucursal = Sucursal(numero=numero,
                                nombre=nombre,
                                direccion=direccion,
                                restaurante=restaurante,
                                latitud=latitud,
                                longitud=longitud,
                                estado=estado,
                                puntuacion='0:0:0:0:0')

            if telefono is not None:
                sucursal.telefono = telefono

            if info_contacto is not None:
                sucursal.info_contacto = info_contacto

            if foto is not None:
                sucursal.foto = foto

            if dias_reservables is not None:
                wm = WeekMask(lunes=dias_reservables[0],
                              martes=dias_reservables[1],
                              miercoles=dias_reservables[2],
                              jueves=dias_reservables[3],
                              viernes=dias_reservables[4],
                              sabado=dias_reservables[5],
                              domingo=dias_reservables[6])
                Persistencia.get_session().add(wm)
                sucursal.dias_reservables = wm

            if (hora_inicio_reserva is not None
                    and hora_fin_reserva is not None):
                sucursal.hora_inicio_reserva = hora_inicio_reserva
                sucursal.hora_fin_reserva = hora_fin_reserva

            Persistencia.get_session().add(sucursal)
            Persistencia.get_session().commit()

        except Exception:
            Persistencia.get_session().rollback()
            raise

        return sucursal, True

    @staticmethod
    def modificar_sucursal(sucursal, nombre, direccion, foto,
                           hora_inicio_reserva, hora_fin_reserva,
                           dias_reservables, telefono, info_contacto,
                           latitud, longitud):

        if nombre is not None:
            sucursal.nombre = nombre

        if direccion is not None:
            sucursal.direccion = direccion

        if foto is not None:
            sucursal.foto = foto

        if hora_inicio_reserva is not None and hora_fin_reserva is not None:
            sucursal.hora_inicio_reserva = hora_inicio_reserva
            sucursal.hora_fin_reserva = hora_fin_reserva

        if dias_reservables is not None:
            wm = WeekMask(lunes=dias_reservables[0],
                          martes=dias_reservables[1],
                          miercoles=dias_reservables[2],
                          jueves=dias_reservables[3],
                          viernes=dias_reservables[4],
                          sabado=dias_reservables[5],
                          domingo=dias_reservables[6])
            Persistencia.get_session().add(wm)
            sucursal.dias_reservables = wm

        if telefono is not None:
            sucursal.telefono = telefono

        if info_contacto is not None:
            sucursal.info_contacto = info_contacto

        if latitud is not None:
            sucursal.latitud = latitud

        if longitud is not None:
            sucursal.longitud = longitud

        Persistencia.get_session().add(sucursal)
        Persistencia.get_session().commit()

        return sucursal, True

    @staticmethod
    def get_oferta_actual(sucursal):
        fecha_actual = datetime.now().date()

        try:
            oferta = Persistencia.get_session().query(SucursalOferta) \
                .filter(SucursalOferta.sucursal == sucursal,
                        SucursalOferta.fecha_desde <= fecha_actual,
                        or_(SucursalOferta.fecha_hasta.is_(None),
                            SucursalOferta.fecha_hasta >= fecha_actual))\
                .one_or_none()
        # TODO: Especificar excepcion
        except Exception as e:
            print(type(e))
            print(e)
            oferta = Persistencia.get_session().query(SucursalOferta) \
                .filter(SucursalOferta.sucursal == sucursal,
                        SucursalOferta.fecha_desde <= fecha_actual,
                        or_(SucursalOferta.fecha_hasta.is_(None),
                            SucursalOferta.fecha_hasta >= fecha_actual)) \
                .order_by(SucursalOferta.fecha_desde.desc()).first()

        if oferta is None:
            # return None, False
            return None
        else:
            return oferta

    @staticmethod
    def asignar_carta(sucursal, carta):
        oferta = ExpSucursal.get_oferta_actual(sucursal)

        if oferta is None:
            oferta = SucursalOferta(
                fecha_desde=datetime.now(),
                sucursal=sucursal,
                carta=carta
            )
        else:
            oferta.carta = carta

        Persistencia.get_session().add(oferta)
        Persistencia.get_session().commit()

        return oferta, True

    @staticmethod
    def abrir_caja(sucursal, monto_inicial, comentarios, cajero):

        caja = ExpSucursal.ver_caja_activa(sucursal)

        if caja is not None:
            return caja, False

        ultimo = Persistencia.query(Caja).order_by(Caja.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        monto_inicial = Decimal(monto_inicial).quantize(Decimal('1.00'))

        caja = Caja(numero=numero,
                    fecha_hora_apertura=datetime.now(),
                    monto_inicial=monto_inicial,
                    monto_final_estimado=monto_inicial,
                    sucursal=sucursal)

        if cajero:
            caja.cajero = cajero

        if comentarios is not None:
            caja.comentarios = comentarios

        Persistencia.get_session().add(caja)
        Persistencia.get_session().commit()

        return sucursal, True

    @staticmethod
    def cerrar_caja(sucursal,  # monto_final_estimado,
                    monto_final_real, comentarios):

        caja = ExpSucursal.ver_caja_activa(sucursal)

        if caja is None:
            return None, False

        monto_final_real = Decimal(monto_final_real).quantize(Decimal('1.00'))

        caja.fecha_hora_cierre = datetime.now()
        caja.monto_final_real = monto_final_real
        caja.diferencia = caja.monto_final_real - caja.monto_final_estimado

        if comentarios is not None:
            caja.comentarios += '; ' + comentarios

        Persistencia.add(caja)
        if Persistencia.commit():
            return sucursal, True
        else:
            return sucursal, False

    @staticmethod
    def deshabilitar_sucursal(sucursal):
        estado_deshabilitada = Persistencia.query(EstadoSucursal).filter(
            EstadoSucursal.nombre == 'deshabilitada'
        )

        sucursal.estado = estado_deshabilitada
        Persistencia.add(sucursal)

        if Persistencia.commit():
            return 0,
        else:
            return 500
