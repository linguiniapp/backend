from modelo.entidades import Restaurante
from modelo.persistencia import Persistencia


class ExpRestaurante:

    @staticmethod
    def get_restaurante(cuil):
        if cuil is None:
            return Persistencia.get_session().query(Restaurante).first()
        else:
            return Persistencia.get_session().query(Restaurante)\
                .filter(Restaurante.cuil == cuil).one_or_none()

    @staticmethod
    def crear_restaurante(cuil, nombre, descripcion, foto):
        rest_exist = Persistencia.get_session().query(Restaurante)\
            .filter(Restaurante.cuil == cuil).one_or_none()

        if rest_exist is not None:
            return rest_exist, False

        restaurante = Restaurante(cuil=cuil,
                                  nombre=nombre)

        if descripcion is not None:
            restaurante.descripcion = descripcion

        if foto is not None:
            restaurante.foto = foto

        Persistencia.get_session().add(restaurante)
        Persistencia.get_session().commit()

        return restaurante, True

    @staticmethod
    def modificar_restaurante(restaurante, descripcion, foto):
        if descripcion is not None:
            restaurante.descripcion = descripcion

        if foto is not None:
            restaurante.foto = foto

        Persistencia.get_session().add(restaurante)
        Persistencia.get_session().commit()

        return restaurante, True
