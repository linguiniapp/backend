import graphene
from graphene import Mutation
from graphql import GraphQLError

from auth.exp_auth import ExpAuth
from exp_cliente import ExpCliente
from exp_empleado import ExpEmpleado
from modelo.entidades import Empleado
from schema.extra_types import ClienteInput
from schema.nodes import RolNode, EmpleadoNode, UsuarioNode


class AuthQL:

    def verificar_usuario(self, info, **kwargs):
        return info.context.user

    def ver_permisos(self, info, **kwargs):
        tipo = info.context.user.tipo_usuario.nombre
        if tipo == 'anonimo':
            return []
        else:
            return ExpAuth.get_permisos(info.context.user.persona.rol)

    def ver_roles(self, info, **kwargs):
        user = info.context.user
        if (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_roles',
                                                  user.persona.rol))):
            return ExpAuth.get_roles()

    def ver_acciones(self, info, **kwargs):
        user = info.context.user
        if (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_acciones',
                                                  user.persona.rol))):
            return ExpAuth.get_acciones()
        else:
            raise GraphQLError('401 - No está autorizado')

    class Login(Mutation):
        class Arguments:
            username = graphene.String()
            password = graphene.String()

        ok = graphene.Boolean()
        token = graphene.String()
        user = graphene.Field(UsuarioNode)

        def mutate(self, info, username, password):
            token, user = ExpAuth.autenticar_usuario(username, password)
            if token is None:
                ok = False
            else:
                ok = True
            return AuthQL.Login(ok=ok, token=token, user=user)

    class CrearUsuarioCliente(Mutation):
        class Arguments:
            username = graphene.String()
            password = graphene.String()
            datos_cliente = ClienteInput()

        ok = graphene.Boolean()
        email = graphene.String()

        def mutate(self, info, username, password, datos_cliente):
            cliente = ExpCliente.crear_cliente(datos_cliente, commit=False)

            if cliente is None:
                result = False
            else:
                result = ExpAuth.crear_usuario_cliente(username,
                                                       password,
                                                       cliente)

            return AuthQL.CrearUsuarioCliente(ok=result, email=cliente.email)

    class CambiarEstadoUsuario(Mutation):
        class Arguments:
            username = graphene.String()
            estado = graphene.String()

        ok = graphene.Boolean()
        estado_anterior = graphene.String()
        estado_actual = graphene.String()

        def mutate(self, info, username, nombre_estado):
            usuario = ExpAuth.get_usuario(username)

            if usuario is None:
                result = False
                e_anterior = None
                e_actual = None
            else:
                e_anterior = usuario.estado.nombre
                result = ExpAuth.cambiar_estado_usuario(usuario, nombre_estado)
                e_actual = usuario.estado.nombre

            return AuthQL.CambiarEstadoUsuario(ok=result,
                                               estado_anterior=e_anterior,
                                               estado_actual=e_actual)

    class CrearRol(Mutation):
        class Arguments:
            nombre = graphene.String(required=True)
            acciones = graphene.List(graphene.Int, required=True)

        ok = graphene.Int()
        rol = graphene.Field(RolNode)

        def mutate(self, info, nombre, acciones):

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('crear_rol',
                                                      user.persona.rol))):
                acciones_auth = [ExpAuth.get_accion(a) for a in acciones]
                rol, ok = ExpAuth.crear_rol(nombre, acciones_auth)
            else:
                rol = None,
                ok = 401

            return AuthQL.CrearRol(ok=ok, rol=rol)

    class ModificarRol(Mutation):
        class Arguments:
            codigo = graphene.Int(required=True)
            nombre = graphene.String()
            agregar_acciones = graphene.List(graphene.Int)
            quitar_acciones = graphene.List(graphene.Int)

        ok = graphene.Int()
        rol = graphene.Field(RolNode)

        def mutate(self, info, codigo, nombre=None,
                   agregar_acciones=tuple([]), quitar_acciones=tuple([])):

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('modificar_rol',
                                                      user.persona.rol))):
                rol = ExpAuth.get_rol(codigo)
                if rol is None:
                    ok = 404
                else:
                    agregar = [ExpAuth.get_accion(a) for a in agregar_acciones]
                    quitar = [ExpAuth.get_accion(a) for a in quitar_acciones]
                    rol, ok = ExpAuth.modificar_rol(rol, nombre,
                                                    agregar, quitar)
            else:
                rol = None
                ok = 401

            return AuthQL.ModificarRol(ok=ok, rol=rol)

    class EliminarRol(Mutation):
        class Arguments:
            codigo = graphene.Int(required=True)

        ok = graphene.Int()
        codigo_rol = graphene.Int()

        def mutate(self, info, codigo):

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('eliminar_rol',
                                                      user.persona.rol))):
                rol = ExpAuth.get_rol(codigo)
                if rol is None:
                    ok = 404
                else:
                    ok = ExpAuth.eliminar_rol(rol)
            else:
                ok = 401

            return AuthQL.EliminarRol(ok=ok, codigo_rol=codigo)

    class AsignarRol(Mutation):
        class Arguments:
            codigo_rol = graphene.Int(required=True)
            numero_o_cuil_empleado = graphene.String(required=True)

        ok = graphene.Int()
        rol = graphene.Field(RolNode)
        empleado = graphene.Field(EmpleadoNode)

        def mutate(self, info, codigo_rol, numero_o_cuil_empleado):
            rol = ExpAuth.get_rol(codigo_rol)
            empleado = ExpEmpleado.get_empleado(numero_o_cuil_empleado)

            if rol is None or empleado is Empleado():
                return AuthQL.AsignarRol(ok=404, rol=None, empleado=None)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('asignar_rol',
                                                      user.persona.rol)
                        and user.persona.sucursal == empleado.sucursal)):
                ok = ExpAuth.asignar_rol(rol, empleado)
            else:
                ok = 401
                empleado = None
                rol = None

            return AuthQL.AsignarRol(ok=ok, rol=rol, empleado=empleado)

    class QuitarRol(Mutation):
        class Arguments:
            numero_o_cuil_empleado = graphene.String(required=True)

        ok = graphene.Int()
        rol = graphene.Field(RolNode)
        empleado = graphene.Field(EmpleadoNode)

        def mutate(self, info, numero_o_cuil_empleado):
            empleado = ExpEmpleado.get_empleado(numero_o_cuil_empleado)
            rol = empleado.rol

            if rol is None or empleado is Empleado():
                return AuthQL.AsignarRol(ok=404, rol=None, empleado=None)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('asignar_rol',
                                                      user.persona.rol)
                        and user.persona.sucursal == empleado.sucursal)):
                ok = ExpAuth.quitar_rol(empleado)
            else:
                ok = 401
                empleado = None
                rol = None

            return AuthQL.QuitarRol(ok=ok, rol=rol, empleado=empleado)

    class RecoverPassword(Mutation):
        class Arguments:
            email = graphene.String(required=True)

        ok = graphene.Int()

        def mutate(self, info, email):
            ok = ExpAuth.recover_password(email)
            return AuthQL.RecoverPassword(ok=ok)

    class ResetPassword(Mutation):
        class Arguments:
            token = graphene.String(required=True)
            password = graphene.String(required=True)

        ok = graphene.Int()

        def mutate(self, info, token, password):
            ok = ExpAuth.reset_password(token, password)
            return AuthQL.ResetPassword(ok=ok)

    class ChangePassword(Mutation):
        class Arguments:
            old_pass = graphene.String(required=True)
            new_pass = graphene.String(required=True)

        ok = graphene.Int()

        def mutate(self, info, old_pass, new_pass):
            user = info.context.user
            if user.tipo_usuario.nombre == 'anonimo':
                ok = 401
            else:
                ok = ExpAuth.change_password(user, old_pass, new_pass)

            return AuthQL.ChangePassword(ok=ok)

    class ChangeAdminEmail(Mutation):
        class Arguments:
            new_email = graphene.String()

        ok = graphene.Int()

        def mutate(self, info, new_email):

            user = info.context.user
            if user.tipo_usuario.nombre == 'admin':
                ok = ExpAuth.cambiar_email_admin(user, new_email)
            else:
                ok = 403

            return AuthQL.ChangeAdminEmail(ok=ok)
