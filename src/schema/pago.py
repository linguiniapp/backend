import graphene
from graphene import Mutation

from auth.exp_auth import ExpAuth
from exp_estadia import ExpEstadia
from exp_mesa import ExpMesa
from exp_pago import ExpPago
from exp_pedido import ExpPedido
from exp_sucursal import ExpSucursal
from exp_qr import ExpQr
from schema.extra_types import MedioPagoInput
from schema.nodes import DTOPreFacturaNode, DTOPreFacturaDetalleNode
from schema.nodes import FacturaNode, FacturaConnection


class PagoQL:

    class EmitirPrefactura(Mutation):

        class Arguments:
            numero_sucursal = graphene.Int()
            numero_mesa = graphene.Int()
            verificar_mozo = graphene.Boolean()

        ok = graphene.Int()
        prefactura = graphene.Field(DTOPreFacturaNode)

        def mutate(self, info, numero_sucursal=None, numero_mesa=None,
                   verificar_mozo=False):

            sucursal = ExpSucursal.get_sucursal(numero_sucursal)

            ok = 0
            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('emitir_prefactura',
                                                      user.persona.rol)
                        and user.persona.sucursal == sucursal)):
                mesa = ExpMesa.get_mesa(sucursal, numero_mesa)
                estadia = ExpEstadia.ver_estadia_actual_mesa(mesa)
                if verificar_mozo and user.persona != estadia.mozo:
                    ok = 401
            elif user.tipo_usuario.nombre == 'cliente':
                estadia = ExpEstadia.ver_estadia_actual_cliente(user.persona)
            else:
                ok = 401
                estadia = None

            if not ok:
                if estadia is not None:
                    prefactura_obj, ok = ExpPago.emitir_prefactura(estadia)
                    detalles = []
                    for detalle in prefactura_obj.detalles:
                        detalles.append(DTOPreFacturaDetalleNode(
                            nombre=detalle.nombre,
                            precio=detalle.precio,
                            cantidad=detalle.cantidad,
                            subtotal=detalle.subtotal,
                        ))
                    prefactura = DTOPreFacturaNode(
                        sucursal=prefactura_obj.sucursal,
                        mesas=prefactura_obj.mesas,
                        estadia=prefactura_obj.estadia,
                        total=prefactura_obj.total,
                        detalles=detalles
                    )
                else:
                    ok = 404
                    prefactura = None
            else:
                prefactura = None

            return PagoQL.EmitirPrefactura(ok=ok, prefactura=prefactura)

    class RealizarPagoCliente(Mutation):
        class Arguments:
            numeros_pedidos = graphene.List(graphene.Int)
            medio_pago = MedioPagoInput(required=True)

        ok = graphene.Int()
        factura = graphene.Field(FacturaNode)
        qr_string = graphene.String()

        def mutate(self, info, medio_pago, numeros_pedidos=tuple([])):
            user = info.context.user
            if (user.tipo_usuario.nombre == 'cliente'
                    and ExpAuth.verificar_permiso('realizar_pago',
                                                  user.persona.rol)):
                estadia = ExpEstadia.ver_estadia_actual_cliente(user.persona)
                if estadia is not None:
                    pedidos = []
                    pedidos_estadia = ExpPedido.ver_pedidos_impagos(estadia)
                    if numeros_pedidos:
                        # Filtrar info de pedidos seleccionados
                        for num_pedido in list(set(numeros_pedidos)):
                            pedido = ExpPedido.get_pedido(num_pedido)
                            if pedido in pedidos_estadia:
                                pedidos.append(pedido)
                    else:
                        pedidos = pedidos_estadia

                    tipo_id = medio_pago.datos_factura.tipo_id_cliente
                    if (tipo_id not in [
                                'dni',
                                'token',
                                'numero',
                                'cuil',
                                'cuit',
                                'pasaporte'
                            ]):
                        return PagoQL.RealizarPagoCliente(ok=400,
                                                          factura=None,
                                                          qr_string=None)

                    medio_pago.cuenta_puntos = user.persona.puntos
                    factura, ok = ExpPago.realizar_pago(pedidos,
                                                        [medio_pago])
                else:
                    ok = 404
                    factura = [None, None]
            else:
                ok = 401
                factura = [None, None]

            if isinstance(factura, tuple):
                qr = factura[1]
                factura = factura[0]
            else:
                qr = None

            return PagoQL.RealizarPagoCliente(ok=ok,
                                              factura=factura,
                                              qr_string=qr)

    class RealizarPagoCaja(Mutation):
        class Arguments:
            numeros_pedidos = graphene.List(graphene.Int)
            medios_pago = graphene.List(MedioPagoInput, required=True)
            numero_sucursal = graphene.Int(required=True)
            numero_mesa = graphene.Int(required=True)

        ok = graphene.Int()
        facturas = FacturaConnection()

        def mutate(self, info, medios_pago, numero_sucursal, numero_mesa,
                   numeros_pedidos=tuple([])):

            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            user = info.context.user
            if (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('realizar_pago',
                                                  user.persona.rol)
                    and user.persona.sucursal == sucursal):
                mesa = ExpMesa.get_mesa(sucursal, numero_mesa)
                estadia = ExpEstadia.ver_estadia_actual_mesa(mesa)
                if estadia is not None:
                    pedidos = []
                    pedidos_estadia = ExpPedido.ver_pedidos_impagos(estadia)
                    if numeros_pedidos:
                        # Filtrar info de pedidos seleccionados
                        for num_pedido in list(set(numeros_pedidos)):
                            pedido = ExpPedido.get_pedido(num_pedido)
                            if pedido in pedidos_estadia:
                                pedidos.append(pedido)
                    else:
                        pedidos = pedidos_estadia

                    for medio_pago in medios_pago:
                        tipo_id = medio_pago.datos_factura.tipo_id_cliente
                        if (tipo_id not in [
                            'dni',
                            'cuil',
                            'cuit',
                            'pasaporte',
                            'ci',
                            'lc',
                            'le'
                        ]):
                            return PagoQL.RealizarPagoCliente(ok=400,
                                                              factura=None,
                                                              qr_string=None)
                    facturas, ok = ExpPago.realizar_pago(pedidos,
                                                         medios_pago,
                                                         True)
                else:
                    ok = 404
                    facturas = None
            else:
                ok = 401
                facturas = None

            return PagoQL.RealizarPagoCliente(ok=ok, facturas=facturas)

    class AcreditarPagoEfectivo(Mutation):
        class Arguments:
            monto = graphene.String(required=True)
            codigo_pago = graphene.String()
            qr_string = graphene.String()

        ok = graphene.Int()
        factura = graphene.Field(FacturaNode)

        def mutate(self, info, monto, codigo_pago=None, qr_string=None):

            user = info.context.user
            user_type = user.tipo_usuario.nombre
            if not (user_type == 'empleado' or user_type == 'admin'):
                return PagoQL.AcreditarPagoEfectivo(ok=403, factura=None)

            if ExpAuth.verificar_permiso('acreditar_pago', user.persona.rol):
                if qr_string:
                    qr_data = ExpQr.decode_qr_string(qr_string)
                    factura = qr_data.get('instance')
                elif codigo_pago:
                    factura = ExpPago.leer_codigo_pago(codigo_pago)
                    if factura is None:
                        return PagoQL.AcreditarPagoEfectivo(ok=404,
                                                            factura=None)
                else:
                    return PagoQL.AcreditarPagoEfectivo(ok=400, factura=None)
            else:
                return PagoQL.AcreditarPagoEfectivo(ok=401, factura=None)

            ok, factura = ExpPago.acreditar_efectivo(monto, factura)
            return PagoQL.AcreditarPagoEfectivo(ok=ok, factura=factura)
