import graphene
from graphene import Mutation

from exp_mesa import ExpMesa
# from exp_sector import ExpSector
from exp_sucursal import ExpSucursal
from auth.exp_auth import ExpAuth
from schema.nodes import MesaNode


class MesaQL:

    class CrearMesa(Mutation):
        class Arguments:
            numero_mesa = graphene.Int()
            capacidad = graphene.Int()
            # numero_sector = graphene.Int()
            numero_sucursal = graphene.Int(required=True)
            posicion_x = graphene.Int()
            posicion_y = graphene.Int()
            reservable = graphene.Boolean()

        ok = graphene.Boolean()
        mesa = graphene.Field(MesaNode)

        def mutate(self, info, numero_sucursal, posicion_x=0, posicion_y=0,
                   capacidad=0, numero_mesa=None, reservable=True):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            if sucursal is None:
                ok = False
                mesa = None
            else:
                # sector = ExpSector.get_sector(sucursal)
                # if sector is None:
                #     ok = False
                #     mesa = None
                # else:
                user = info.context.user
                if ExpAuth.verificar_permiso('crear_mesa', user.persona.rol):
                    mesa, ok = ExpMesa.crear_mesa(numero_mesa,
                                                  posicion_x, posicion_y,
                                                  capacidad, sucursal,
                                                  reservable)
                else:
                    ok = 401
                    mesa = None

            return MesaQL.CrearMesa(ok=ok, mesa=mesa)

    class ModificarMesa(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int(required=True)
            numero_mesa = graphene.Int(required=True)
            numero_nuevo = graphene.Int()
            capacidad = graphene.Int()
            nombre_estado = graphene.String()
            reservable = graphene.Boolean()
            posicion_x = graphene.Int()
            posicion_y = graphene.Int()

        ok = graphene.Boolean()
        mesa = graphene.Field(MesaNode)

        def mutate(self, info, numero_sucursal, numero_mesa,
                   posicion_x=None, posicion_y=None,
                   numero_nuevo=None, capacidad=None,
                   nombre_estado=None, reservable=None):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            mesa = ExpMesa.get_mesa(sucursal, numero_mesa)

            user = info.context.user
            if ExpAuth.verificar_permiso('modificar_mesa', user.persona.rol):
                mesa, ok = ExpMesa.modificar_mesa(
                    sucursal, mesa, numero_nuevo, capacidad,
                    nombre_estado, reservable, posicion_x, posicion_y)
            else:
                ok = 401
                mesa = None

            return MesaQL.ModificarMesa(ok=ok, mesa=mesa)

    class EliminarMesa(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int(required=True)
            numero_mesa = graphene.Int(required=True)

        ok = graphene.Int()

        def mutate(self, info, numero_sucursal, numero_mesa):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            mesa = ExpMesa.get_mesa(sucursal, numero_mesa)

            user = info.context.user
            if ExpAuth.verificar_permiso('eliminar_mesa', user.persona.rol):
                ok = ExpMesa.eliminar_mesa(mesa)
            else:
                ok = 401

            return MesaQL.EliminarMesa(ok=ok)
