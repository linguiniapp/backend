import graphene
from graphene import Mutation
from graphql import GraphQLError

from auth.exp_auth import ExpAuth
from exp_producto import ExpProducto
from exp_recompensa import ExpRecompensa
from schema.extra_types import RecompensaInput
from schema.nodes import RecompensaNode


class RecompensaQL:

    def ver_recompensas(self, info, **kwargs):

        user = info.context.user
        if ExpAuth.verificar_permiso('ver_recompensas',
                                     user.persona.rol):
            return ExpRecompensa.ver_recompensas()
        else:
            raise GraphQLError('401 - No está autorizado a ver recompensas')

    class CrearRecompensa(Mutation):
        class Arguments:
            nombre = graphene.String()
            puntos = graphene.Int(required=True)
            detalles = graphene.List(RecompensaInput)

        ok = graphene.Int()
        recompensa = graphene.Field(RecompensaNode)

        def mutate(self, info, nombre, puntos, detalles):

            user = info.context.user
            if ExpAuth.verificar_permiso('crear_recompensa',
                                         user.persona.rol):

                lista_producto_cantidad = [
                    (ExpProducto.get_producto(d.codigo), d.cantidad)
                    for d in detalles
                ]

                recompensa, ok = ExpRecompensa.crear_recompensa(
                    nombre,
                    puntos,
                    lista_producto_cantidad
                )
            else:
                ok = 401
                recompensa = None

            return RecompensaQL.CrearRecompensa(ok=ok, recompensa=recompensa)
