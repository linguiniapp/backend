from datetime import datetime

import graphene
from graphene import relay, Connection
from graphene_sqlalchemy import SQLAlchemyConnectionField
from graphene_sqlalchemy import SQLAlchemyObjectType

from exp_carta import ExpCarta
from exp_mesa import ExpMesa
from exp_pedido import ExpPedido
from exp_producto import ExpProducto
from exp_promocion import ExpPromocion
from exp_sucursal import ExpSucursal
from exp_estadia import ExpEstadia
from modelo.entidades import Accion
from modelo.entidades import Admin
from modelo.entidades import Caja
# from exp_sector import ExpSector
from modelo.entidades import Carta
from modelo.entidades import CartaProducto
from modelo.entidades import CategoriaProducto
from modelo.entidades import Cliente
from modelo.entidades import PuntosCliente
from modelo.entidades import Cuotas
from modelo.entidades import DetalleFactura
from modelo.entidades import DetallePedido
from modelo.entidades import DetallePromocion
from modelo.entidades import DetalleRecompensa
from modelo.entidades import Empleado
from modelo.entidades import EmpleadoEstado
from modelo.entidades import Estadia
from modelo.entidades import EstadoCarta
from modelo.entidades import EstadoCartaProducto
from modelo.entidades import EstadoCliente
from modelo.entidades import EstadoEmpleado
from modelo.entidades import EstadoMesa
from modelo.entidades import EstadoPedido
from modelo.entidades import EstadoProducto
from modelo.entidades import EstadoPromocion
from modelo.entidades import EstadoRecompensa
from modelo.entidades import Factura
from modelo.entidades import ListaEspera
from modelo.entidades import ListaEsperaDetalle
from modelo.entidades import MedioPago
from modelo.entidades import Mesa
from modelo.entidades import Pedido
from modelo.entidades import PedidoEstado
from modelo.entidades import PedidoPromocion
from modelo.entidades import PedidoRecompensa
from modelo.entidades import Periodicidad
from modelo.entidades import Persona
from modelo.entidades import Producto
from modelo.entidades import Promocion
from modelo.entidades import ProveedorPago
from modelo.entidades import Recompensa
from modelo.entidades import Reserva
from modelo.entidades import Restaurante
from modelo.entidades import Rol
# from modelo.entidades import Sector
from modelo.entidades import Sucursal
from modelo.entidades import SucursalOferta
from modelo.entidades import TipoFactura
from modelo.entidades import TipoMedioPago
from modelo.entidades import TipoUsuario
from modelo.entidades import Usuario
from modelo.entidades import WeekMask
from modelo.entidades import PuntuacionSucursal


class ListaEsperaNode(SQLAlchemyObjectType):
    class Meta:
        model = ListaEspera
        interfaces = (relay.Node, )


class PuntuacionNode(SQLAlchemyObjectType):
    class Meta:
        model = PuntuacionSucursal
        interfaces = (relay.Node, )


class PuntuacionConnection(Connection):
    class Meta:
        node = PuntuacionNode


class ListaEsperaDetalleNode(SQLAlchemyObjectType):
    class Meta:
        model = ListaEsperaDetalle
        interfaces = (relay.Node, )


# class SectorNode(SQLAlchemyObjectType):
#     class Meta:
#         model = Sector
#         interfaces = (relay.Node, )


class CajaNode(SQLAlchemyObjectType):
    class Meta:
        model = Caja
        interfaces = (relay.Node, )


class CajaConnection(Connection):
    class Meta:
        node = CajaNode


class SucursalNode(SQLAlchemyObjectType):
    class Meta:
        model = Sucursal
        interfaces = (relay.Node, )

    caja = graphene.Field(CajaNode)

    def resolve_caja(self, info, **kwargs):
        return ExpSucursal.ver_caja_activa(self)


class SucursalOfertaNode(SQLAlchemyObjectType):
    class Meta:
        model = SucursalOferta
        interfaces = (relay.Node, )

    # sector = graphene.Field(SectorNode,
    #                         numero_sector=graphene.Int(required=True))
    #
    # def resolve_sector(self, info, numero_sector):
    #     sector = ExpSector.get_sector(self, numero_sector)
    #     print(sector)
    #     return sector


class SucursalConnection(Connection):
    class Meta:
        node = SucursalNode


class RestauranteNode(SQLAlchemyObjectType):
    class Meta:
        model = Restaurante
        interfaces = (relay.Node,)


class ReservaNode(SQLAlchemyObjectType):
    class Meta:
        model = Reserva
        interfaces = (relay.Node, )


class ReservaConnection(Connection):
    class Meta:
        node = ReservaNode


class MesaNode(SQLAlchemyObjectType):
    class Meta:
        model = Mesa
        interfaces = (relay.Node, )

    reserva_existente = graphene.Field(
        graphene.Boolean,
        fecha_hora_reserva=graphene.DateTime()
    )

    pedido_pago = graphene.Field(graphene.Boolean)

    def resolve_reserva_existente(self, info,
                                  fecha_hora_reserva=datetime.now(), **kwargs):
        return not ExpMesa.check_mesa_disponible([self], fecha_hora_reserva)

    def resolve_pedido_pago(self, info, **kwargs):
        estadia_actual = ExpEstadia.ver_estadia_actual_mesa(self)
        if ExpPedido.ver_pedidos_impagos(estadia_actual):
            return False
        else:
            return True


class MesaConnection(Connection):
    class Meta:
        node = MesaNode


class EstadoMesaNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoMesa
        interfaces = (relay.Node, )


class CategoriaNode(SQLAlchemyObjectType):
    class Meta:
        model = CategoriaProducto
        interfaces = (relay.Node, )


class CategoriaConnection(Connection):
    class Meta:
        node = CategoriaNode


class ProductoNode(SQLAlchemyObjectType):
    class Meta:
        model = Producto
        interfaces = (relay.Node, )


class ProductoConnection(Connection):
    class Meta:
        node = ProductoNode


class EstadoProductoNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoProducto
        interfaces = (relay.Node, )


class PromocionNode(SQLAlchemyObjectType):
    class Meta:
        model = Promocion
        interfaces = (relay.Node, )

    def resolve_precio(self, info, **kwargs):
        return ExpPromocion.get_precio(self)


class DetallePromocionNode(SQLAlchemyObjectType):
    class Meta:
        model = DetallePromocion
        interfaces = (relay.Node, )


class EstadoPromocionNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoPromocion
        interfaces = (relay.Node, )


class PromocionConnection(Connection):
    class Meta:
        node = PromocionNode


class PeriodicidadNode(SQLAlchemyObjectType):
    class Meta:
        model = Periodicidad
        interfaces = (relay.Node, )


class PeriodicidadConnection(Connection):
    class Meta:
        node = PeriodicidadNode


class WeekMaskNode(SQLAlchemyObjectType):
    class Meta:
        model = WeekMask
        interfaces = (relay.Node, )


class EstadoCartaNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoCarta
        interfaces = (relay.Node, )


class CartaProductoNode(SQLAlchemyObjectType):
    class Meta:
        model = CartaProducto
        interfaces = (relay.Node, )


class CartaProductoConnection(Connection):
    class Meta:
        node = CartaProductoNode


class EstadoCartaProductoNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoCartaProducto
        interfaces = (relay.Node, )


class CartaNode(SQLAlchemyObjectType):
    class Meta:
        model = Carta
        interfaces = (relay.Node, )

    carta_productos = SQLAlchemyConnectionField(CartaProductoConnection,
                                                cod_categoria=graphene.Int())

    def resolve_carta_productos(self, info, cod_categoria=None, **kwargs):
        categoria = ExpProducto.get_categoria(cod_categoria)
        if categoria is None:
            cat_param = None
        else:
            cat_param = [categoria]

        return ExpCarta.ver_productos_carta(self, cat_param)


class AccionNode(SQLAlchemyObjectType):
    class Meta:
        model = Accion
        interfaces = (relay.Node, )


class AccionConnection(Connection):
    class Meta:
        node = AccionNode


class RolNode(SQLAlchemyObjectType):
    class Meta:
        model = Rol
        interfaces = (relay.Node, )


class RolConnection(Connection):
    class Meta:
        node = RolNode


class AdminNode(SQLAlchemyObjectType):
    class Meta:
        model = Admin
        interfaces = (relay.Node, )


class TipoUsuarioNode(SQLAlchemyObjectType):
    class Meta:
        model = TipoUsuario
        interfaces = (relay.Node, )


class PuntosClienteNode(SQLAlchemyObjectType):
    class Meta:
        model = PuntosCliente
        interfaces = (relay.Node, )


class ClienteNode(SQLAlchemyObjectType):
    class Meta:
        model = Cliente
        interfaces = (relay.Node, )


class ClienteConnection(Connection):
    class Meta:
        node = ClienteNode


class EstadoClienteNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoCliente
        interfaces = (relay.Node, )


class EstadoEmpleadoNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoEmpleado
        interfaces = (relay.Node, )


class EmpleadoEstadoNode(SQLAlchemyObjectType):
    class Meta:
        model = EmpleadoEstado
        interfaces = (relay.Node, )


class EmpleadoNode(SQLAlchemyObjectType):
    class Meta:
        model = Empleado
        interfaces = (relay.Node, )


class EmpleadoConnection(Connection):
    class Meta:
        node = EmpleadoNode


class PersonaNode(SQLAlchemyObjectType):
    class Meta:
        model = Persona
        interfaces = (relay.Node, )


class DatosPersonaNode(graphene.Union):
    class Meta:
        types = (EmpleadoNode, ClienteNode, AdminNode)


class UsuarioNode(SQLAlchemyObjectType):
    class Meta:
        model = Usuario
        interfaces = (relay.Node, )
        only_fields = ('nombre', 'tipo_usuario', 'persona', 'acciones')

    cliente = graphene.Field(ClienteNode)
    empleado = graphene.Field(EmpleadoNode)
    admin = graphene.Field(AdminNode)

    def resolve_cliente(self, info, **kwargs):
        if isinstance(self.persona, Cliente):
            return self.persona
        else:
            return None

    def resolve_empleado(self, info, **kwargs):
        if isinstance(self.persona, Empleado):
            return self.persona
        else:
            return None

    def resolve_admin(self, info, **kwargs):
        if isinstance(self.persona, Admin):
            return self.persona
        else:
            return None


class EstadiaNode(SQLAlchemyObjectType):
    class Meta:
        model = Estadia
        interfaces = (relay.Node, )


class EstadoPedidoNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoPedido
        interfaces = (relay.Node, )


class PedidoEstadoNode(SQLAlchemyObjectType):
    class Meta:
        model = PedidoEstado
        interfaces = (relay.Node, )


class PedidoNode(SQLAlchemyObjectType):
    class Meta:
        model = Pedido
        interfaces = (relay.Node, )

    estado_actual = graphene.Field(PedidoEstadoNode)

    def resolve_estado_actual(self: Pedido, info, **kwargs):
        return ExpPedido.ver_estado_actual_pedido(self)


class PedidoDetalleNode(SQLAlchemyObjectType):
    class Meta:
        model = DetallePedido
        interfaces = (relay.Node, )


class PedidoPromocionNode(SQLAlchemyObjectType):
    class Meta:
        model = PedidoPromocion
        interfaces = (relay.Node, )


class PedidoRecompensaNode(SQLAlchemyObjectType):
    class Meta:
        model = PedidoRecompensa
        interfaces = (relay.Node, )


class PedidoConnection(Connection):
    class Meta:
        node = PedidoNode


class RecompensaNode(SQLAlchemyObjectType):
    class Meta:
        model = Recompensa
        interfaces = (relay.Node, )


class DetalleRecompensaNode(SQLAlchemyObjectType):
    class Meta:
        model = DetalleRecompensa
        interfaces = (relay.Node, )


class RecompensaConnection(Connection):
    class Meta:
        node = RecompensaNode


class EstadoRecompensaNode(SQLAlchemyObjectType):
    class Meta:
        model = EstadoRecompensa
        interfaces = (relay.Node, )


class FacturaNode(SQLAlchemyObjectType):
    class Meta:
        model = Factura
        interfaces = (relay.Node, )


class FacturaConnection(Connection):
    class Meta:
        node = FacturaNode


class TipoFacturaNode(SQLAlchemyObjectType):
    class Meta:
        model = TipoFactura
        interfaces = (relay.Node, )


class FacturaDetalleNode(SQLAlchemyObjectType):
    class Meta:
        model = DetalleFactura
        interfaces = (relay.Node, )


class ProveedorPagoNode(SQLAlchemyObjectType):
    class Meta:
        model = ProveedorPago
        interfaces = (relay.Node, )


class MedioPagoNode(SQLAlchemyObjectType):
    class Meta:
        model = MedioPago
        interfaces = (relay.Node, )


class TipoMedioPagoNode(SQLAlchemyObjectType):
    class Meta:
        model = TipoMedioPago
        interfaces = (relay.Node, )


class CuotasNode(SQLAlchemyObjectType):
    class Meta:
        model = Cuotas
        interfaces = (relay.Node, )


class DTOPreFacturaDetalleNode(graphene.ObjectType):
    nombre = graphene.String()
    precio = graphene.String()
    cantidad = graphene.Int()
    subtotal = graphene.String()


class DTOPreFacturaNode(graphene.ObjectType):
    sucursal = graphene.Int()
    mesas = graphene.List(graphene.Int)
    estadia = graphene.Field(EstadiaNode)
    total = graphene.String()
    detalles = graphene.List(DTOPreFacturaDetalleNode)


class DTOReporteCaja(graphene.ObjectType):
    fecha_hora = graphene.DateTime()
    fecha_desde = graphene.Date()
    fecha_hasta = graphene.Date()
    sucursal = graphene.Field(SucursalNode)
    cajas = SQLAlchemyConnectionField(CajaConnection)


class ReporteMozoNode(graphene.ObjectType):
    empleado = graphene.Field(EmpleadoNode)
    pedidos = SQLAlchemyConnectionField(PedidoConnection)
    cantidad_pedidos = graphene.Int()
    cantidad_productos = graphene.Int()
    monto_productos = graphene.String()


class ReporteMozoConnection(Connection):
    class Meta:
        node = ReporteMozoNode


class DTOReporteMozos(graphene.ObjectType):
    fecha_hora = graphene.DateTime()
    fecha_desde = graphene.Date()
    fecha_hasta = graphene.Date()
    numero_sucursal = graphene.Int()
    nombre_sucursal = graphene.String()
    mozos = graphene.relay.ConnectionField(ReporteMozoConnection)


class DTOReporteCalificaciones(graphene.ObjectType):
    fecha_hora = graphene.DateTime()
    fecha_desde = graphene.Date()
    fecha_hasta = graphene.Date()
    numero_sucursal = graphene.Int()
    nombre_sucursal = graphene.String()
    calificaciones = SQLAlchemyConnectionField(PuntuacionConnection)
    promedio_sucursal = graphene.String()
    detalle_sucursal = graphene.List(graphene.Int)


class ReporteVentaNodo(graphene.ObjectType):
    fecha_factura = graphene.String()
    numero_factura = graphene.Int()
    numero_sucursal = graphene.Int()
    nombre_sucursal = graphene.String()
    monto_facturado = graphene.String()


class ReporteVentaConnection(Connection):
    class Meta:
        node = ReporteVentaNodo


class DTOReporteVentas(graphene.ObjectType):
    fecha_hora = graphene.DateTime()
    fecha_desde = graphene.Date()
    fecha_hasta = graphene.Date()
    ventas = graphene.relay.ConnectionField(ReporteVentaConnection)


class ReporteProductoNode(graphene.ObjectType):
    codigo = graphene.Int()
    nombre = graphene.String()
    precio_unitario = graphene.String()
    cantidad_vendida = graphene.Int()
    monto_ventas = graphene.String()


class ReporteProductoConnection(Connection):
    class Meta:
        node = ReporteProductoNode


class DTOReporteProductos(graphene.ObjectType):
    fecha_hora = graphene.DateTime()
    fecha_desde = graphene.Date()
    fecha_hasta = graphene.Date()
    productos = graphene.relay.ConnectionField(ReporteProductoConnection)
