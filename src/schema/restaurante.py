import graphene
from graphene import Mutation

from exp_restaurante import ExpRestaurante
from schema.nodes import RestauranteNode


class RestauranteQL:

    def get_restaurante(self, info, cuil_restaurante, **kwargs):
        return ExpRestaurante.get_restaurante(cuil_restaurante)

    class CrearRestaurante(Mutation):
        class Arguments:
            cuil = graphene.String(required=True)
            nombre = graphene.String(required=True)
            descripcion = graphene.String()
            # foto = FileUpload()
            foto = graphene.String()

        ok = graphene.Boolean()
        restaurante = graphene.Field(RestauranteNode)

        def mutate(self, info, cuil, nombre, descripcion=None, foto=None):
            # if foto is not None:
                # foto = foto.stream.read()

            restaurante, ok = ExpRestaurante.crear_restaurante(
                cuil, nombre, descripcion, foto)

            return RestauranteQL.CrearRestaurante(
                ok=ok, restaurante=restaurante)

    class ModificarRestaurante(Mutation):
        class Arguments:
            cuil = graphene.String()
            descripcion = graphene.String()
            # foto = FileUpload()
            foto = graphene.String()

        ok = graphene.Boolean()
        restaurante = graphene.Field(RestauranteNode)

        def mutate(self, info, cuil=None, descripcion=None, foto=None):
            restaurante = ExpRestaurante.get_restaurante(cuil)

            # if foto is not None:
                # foto = foto.stream.read()

            if restaurante is None:
                restaurante = None
                ok = False
            else:
                restaurante, ok = ExpRestaurante.modificar_restaurante(
                    restaurante, descripcion, foto)

            return RestauranteQL.ModificarRestaurante(
                ok=ok, restaurante=restaurante)
