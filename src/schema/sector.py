# import graphene
# from graphene import Mutation
#
# from exp_sector import ExpSector
# from exp_sucursal import ExpSucursal
#
# from schema.nodes import SectorNode
# from schema.extra_types import VerticeInput
#
#
# class SectorQL:
#
#     def get_sector(self, info, numero_sucursal, numero_sector, **kwargs):
#         return ExpSector.get_sector(numero_sucursal, numero_sector)
#
#     class CrearSector(Mutation):
#         class Arguments:
#             numero_sucursal = graphene.Int(required=True)
#             piso = graphene.Int(required=True)
#             vertices = graphene.List(VerticeInput)
#
#         ok = graphene.Boolean()
#         sector = graphene.Field(SectorNode)
#
#         def mutate(self, info, numero_sucursal, piso, vertices=None):
#             sucursal = ExpSucursal.get_sucursal(numero_sucursal)
#
#             if sucursal is None:
#                 result = False
#                 sector = None
#             else:
#                 result, sector = ExpSector.crear_sector(sucursal, piso, vertices)
#
#             return SectorQL.CrearSector(ok=result, sector=sector)
