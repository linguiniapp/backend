import graphene
from graphene import Mutation
from graphql import GraphQLError

from auth.exp_auth import ExpAuth
from exp_empleado import ExpEmpleado
from exp_sucursal import ExpSucursal
from schema.extra_types import EmpleadoInput
from schema.nodes import EmpleadoNode


class EmpleadoQL:

    def ver_empleado(self, info, numero_o_cuil=None, **kwargs):
        user = info.context.user
        if numero_o_cuil is None:
            if user.tipo_usuario.nombre == 'empleado':
                return user.persona
            else:
                raise GraphQLError('400:No se ingresó número de empleado')

        if (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_empleados',
                                                  user.persona.rol))):
                return ExpEmpleado.get_empleado(numero_o_cuil)

    def ver_empleados(self, info, numero_sucursal=None,
                      activos=None, **kwargs):
        if numero_sucursal is not None:
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            if sucursal is None:
                return [404]
        else:
            sucursal = None

        user = info.context.user
        if (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_empleados',
                                                  user.persona.rol)
                    and user.persona.sucursal == sucursal)):
            empleados, ok = ExpEmpleado.get_empleados(sucursal, activos)
        else:
            return [401]

        if ok:
            return [ok]
        else:
            return empleados

    class CrearEmpleado(Mutation):
        class Arguments:
            info_empleado = EmpleadoInput(required=True)
            numero_sucursal = graphene.Int(required=True)

        ok = graphene.Int()
        empleado = graphene.Field(EmpleadoNode)

        def mutate(self, info, info_empleado, numero_sucursal):
            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('crear_empleado',
                                                      user.persona.rol))):
                sucursal = ExpSucursal.get_sucursal(numero_sucursal)
                if sucursal is None:
                    ok = 404
                    empleado = None
                else:
                    empleado, ok = ExpEmpleado.crear_empleado(
                        sucursal,
                        info_empleado.nombre,
                        info_empleado.apellido,
                        info_empleado.sexo,
                        info_empleado.email,
                        info_empleado.telefono,
                        info_empleado.fecha_nacimiento,
                        info_empleado.cuil
                    )
            else:
                ok = 401
                empleado = None

            return EmpleadoQL.CrearEmpleado(ok=ok, empleado=empleado)

    class ModificarEmpleado(Mutation):
        class Arguments:
            numero_o_cuil = graphene.String()
            numero_sucursal = graphene.Int()
            nombre = graphene.String()
            apellido = graphene.String()
            sexo = graphene.String()
            email = graphene.String()
            telefono = graphene.String()
            fecha_nac = graphene.Date()
            cuil = graphene.String()

        ok = graphene.Int()
        empleado = graphene.Field(EmpleadoNode)

        def mutate(self, info, numero_o_cuil=None, numero_sucursal=None,
                   nombre=None, apellido=None, sexo=None, email=None,
                   telefono=None, fecha_nac=None, cuil=None):
            user = info.context.user

            ok = 0
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('modificar_empleado',
                                                      user.persona.rol))):
                if (numero_o_cuil is None
                        and user.tipo_usuario.nombre == 'empleado'):
                    empleado = user.persona
                elif numero_o_cuil is not None:
                    empleado = ExpEmpleado.get_empleado(numero_o_cuil)
                else:
                    empleado = None
                    ok = 400

                if empleado is None and not ok:
                    ok = 404
                else:
                    if numero_sucursal is not None:
                        sucursal = ExpSucursal.get_sucursal(numero_sucursal)
                    else:
                        sucursal = None
                    empleado, ok = ExpEmpleado.modificar_empleado(
                        empleado,
                        sucursal,
                        nombre,
                        apellido,
                        sexo,
                        email,
                        telefono,
                        fecha_nac,
                        cuil
                    )
            else:
                ok = 401
                empleado = None

            return EmpleadoQL.ModificarEmpleado(ok=ok, empleado=empleado)

    class DeshabilitarEmpleado(Mutation):
        numero_o_cuil = graphene.String(required=True)
        comentario = graphene.String(required=True)

        ok = graphene.Int()
        empleado = graphene.Field(EmpleadoNode)

        def mutate(self, info, numero_o_cuil, comentario):
            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('deshabilitar_empleado',
                                                      user.persona.rol))):
                empleado = ExpEmpleado.get_empleado(numero_o_cuil)
                if empleado is None:
                    ok = 404
                else:
                    empleado, ok = ExpEmpleado.deshabilitar_empleado(
                        empleado, comentario)
            else:
                empleado = None
                ok = 401

            return EmpleadoQL.DeshabilitarEmpleado(ok=ok, empleado=empleado)

    class SuspenderEmpleado(Mutation):
        numero_o_cuil = graphene.String(required=True)
        comentario = graphene.String(required=True)

        ok = graphene.Int()
        empleado = graphene.Field(EmpleadoNode)

        def mutate(self, info, numero_o_cuil, comentario):
            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('suspender_empleado',
                                                      user.persona.rol))):
                empleado = ExpEmpleado.get_empleado(numero_o_cuil)
                if empleado is None:
                    ok = 404
                else:
                    empleado, ok = ExpEmpleado.suspender_empleado(empleado,
                                                                  comentario)
            else:
                empleado = None
                ok = 401

            return EmpleadoQL.SuspenderEmpleado(ok=ok, empleado=empleado)

    class HabilitarEmpleado(Mutation):
        numero_o_cuil = graphene.String(required=True)
        comentario = graphene.String(required=True)

        ok = graphene.Int()
        empleado = graphene.Field(EmpleadoNode)

        def mutate(self, info, numero_o_cuil, comentario):
            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('habilitar_empleado',
                                                      user.persona.rol))):
                empleado = ExpEmpleado.get_empleado(numero_o_cuil)
                if empleado is None:
                    ok = 404
                else:
                    empleado, ok = ExpEmpleado.habilitar_empleado(empleado,
                                                                  comentario)
            else:
                empleado = None
                ok = 401

            return EmpleadoQL.HabilitarEmpleado(ok=ok, empleado=empleado)
