import graphene
from graphene import Mutation
from graphql import GraphQLError

from auth.exp_auth import ExpAuth
from exp_cliente import ExpCliente
from schema.extra_types import ClienteInput
from schema.nodes import ClienteNode


class ClienteQL:

    def ver_clientes(self, info, lista_estados=None, **kwargs):

        if lista_estados is not None:
            lista_estados = list(set(lista_estados))
        else:
            lista_estados = []

        user = info.context.user
        if (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_clientes',
                                                  user.persona.rol))):
            return ExpCliente.ver_clientes(lista_estados)
        else:
            # return 401
            raise GraphQLError('401 - No está autorizado a ver clientes')

    def ver_cliente(self, info, numero_cliente=None, dni_cliente=None):
        user = info.context.user
        if user.tipo_usuario.nombre == 'cliente':
            return ExpCliente.get_cliente(user.persona.numero)
        elif (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_clientes',
                                                  user.persona.rol))):
            if numero_cliente is not None:
                return ExpCliente.get_cliente(numero=numero_cliente)
            elif dni_cliente is not None:
                return ExpCliente.get_cliente(dni=dni_cliente)
            else:
                raise GraphQLError('404 - No se encontró el cliente')

    class ModificarCliente(Mutation):
        class Arguments:
            numero_o_dni = graphene.Int()
            info_cliente = ClienteInput(required=True)

        ok = graphene.Int()
        cliente = graphene.Field(ClienteNode)

        def mutate(self, info, info_cliente, numero_o_dni=None):
            user = info.context.user

            if numero_o_dni is not None:
                cliente = ExpCliente.get_cliente(numero_o_dni)
            elif user.tipo_usuario.nombre == 'cliente':
                cliente = user.persona
            else:
                return ClienteQL.ModificarCliente(ok=400, cliente=None)

            if (ExpAuth.verificar_permiso('modificar_cliente',
                                          user.persona.rol)
                    and (user.persona == cliente
                         if user.tipo_usuario.nombre == 'cliente'
                         else True)):
                cliente, ok = ExpCliente.modificar_cliente(cliente,
                                                           info_cliente)
            else:
                cliente = None
                ok = False

            return ClienteQL.ModificarCliente(ok=ok, cliente=cliente)

    class DeshabilitarCliente(Mutation):
        class Arguments:
            numero_o_dni = graphene.Int(required=True)

        ok = graphene.Int()
        cliente = graphene.Field(ClienteNode)

        def mutate(self, info, numero_o_dni):
            cliente = ExpCliente.get_cliente(numero_o_dni)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('deshabilitar_cliente',
                                                      user.persona.rol))
                    or (user.tipo_usuario.nombre == 'cliente'
                        and user.persona == cliente)):
                cliente, ok = ExpCliente.deshabilitar_cliente(cliente)
            else:
                cliente = None
                ok = False

            return ClienteQL.DeshabilitarCliente(ok=ok, cliente=cliente)

    class SuspenderCliente(Mutation):
        class Arguments:
            numero_o_dni = graphene.Int(required=True)

        ok = graphene.Boolean()
        cliente = graphene.Field(ClienteNode)

        def mutate(self, info, numero_o_dni):
            cliente = ExpCliente.get_cliente(numero_o_dni)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('suspender_cliente',
                                                      user.persona.rol))):
                cliente, ok = ExpCliente.suspender_cliente(cliente)
            else:
                ok = False
                cliente = None

            return ClienteQL.SuspenderCliente(ok=ok, cliente=cliente)
