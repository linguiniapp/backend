import graphene
from graphene import Mutation

from auth.exp_auth import ExpAuth
from exp_carta import ExpCarta
from exp_producto import ExpProducto
from exp_sucursal import ExpSucursal
from schema.extra_types import DetalleCartaProductoInput
from schema.nodes import CartaNode


class CartaQL:

    def ver_carta(self, info, numero_sucursal):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)
        return ExpCarta.ver_carta(sucursal)

    # def ver_detalles(self, info, numero_sucursal, codigo_producto):
    #     sucursal = ExpSucursal.get_sucursal(numero_sucursal)
    #     producto = ExpProducto.get_producto(codigo_producto)
    #     return ExpCarta.ver_detalle_producto(sucursal, producto)

    class CrearCarta(Mutation):
        class Arguments:
            lista_productos = graphene.List(DetalleCartaProductoInput,
                                            required=True)

        ok = graphene.Boolean()
        carta = graphene.Field(CartaNode)

        def mutate(self, info, lista_productos):
            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('crear_carta',
                                                      user.persona.rol))):
                productos = []
                for prod_det in lista_productos:
                    producto = ExpProducto.get_producto(prod_det.numero)
                    if producto is not None:
                        productos.append((producto, prod_det.precio))

                if productos:
                    carta, ok = ExpCarta.crear_carta(productos)
                else:
                    ok = False
                    carta = None
            else:
                ok = False
                carta = None

            return CartaQL.CrearCarta(ok=ok, carta=carta)
