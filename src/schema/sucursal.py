import graphene
from graphene import Mutation

from auth.exp_auth import ExpAuth
from exp_carta import ExpCarta
from exp_restaurante import ExpRestaurante
from exp_sucursal import ExpSucursal
from schema.extra_types import WeekMaskInput
from schema.nodes import SucursalNode, SucursalOfertaNode


class SucursalQL:

    def get_sucursal(self, info, numero_sucursal, **kwargs):
        return ExpSucursal.get_sucursal(numero_sucursal)

    def ver_caja_activa(self, info, numero_sucursal, **kwargs):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)

        user = info.context.user
        if (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_caja',
                                                  user.persona.rol)
                    and user.persona.sucursal == sucursal)):
            return ExpSucursal.ver_caja_activa(sucursal)
        else:
            return 401

    class CrearSucursal(Mutation):
        class Arguments:
            nombre = graphene.String(required=True)
            direccion = graphene.String(required=True)
            cuil_restaurante = graphene.String()
            latitud = graphene.Float(required=True)
            longitud = graphene.Float(required=True)
            telefono = graphene.String()
            info_contacto = graphene.String()
            foto = graphene.String()
            hora_inicio_reserva = graphene.Time()
            hora_fin_reserva = graphene.Time()
            dias_reservables = WeekMaskInput()

        ok = graphene.Boolean()
        sucursal = graphene.Field(SucursalNode)

        def mutate(self, info, nombre, direccion, latitud, longitud,
                   cuil_restaurante=None, telefono=None, info_contacto=None,
                   foto=None, dias_reservables=None,
                   hora_inicio_reserva=None, hora_fin_reserva=None):
            restaurante = ExpRestaurante.get_restaurante(cuil_restaurante)

            if restaurante is None:
                result = False
                sucursal = None
            else:
                user = info.context.user
                if (user.tipo_usuario.nombre == 'admin'
                        or (user.tipo_usuario.nombre == 'empleado'
                            and ExpAuth.verificar_permiso('crear_sucursal',
                                                          user.persona.rol))):
                    sucursal, result = ExpSucursal.crear_sucursal(
                        nombre, direccion, restaurante, latitud, longitud,
                        telefono, info_contacto, foto, dias_reservables,
                        hora_inicio_reserva, hora_fin_reserva)
                else:
                    result = False
                    sucursal = None

            return SucursalQL.CrearSucursal(ok=result, sucursal=sucursal)

    class ModificarSucursal(Mutation):
        class Arguments:
            numero = graphene.Int(required=True)
            nombre = graphene.String()
            direccion = graphene.String()
            foto = graphene.String()
            hora_inicio_reserva = graphene.Time()
            hora_fin_reserva = graphene.Time()
            dias_reservables = WeekMaskInput()
            telefono = graphene.String()
            info_contacto = graphene.String()
            latitud = graphene.Float()
            longitud = graphene.Float()

        ok = graphene.Boolean()
        sucursal = graphene.Field(SucursalNode)

        def mutate(self, info, numero, nombre=None, direccion=None,
                   hora_inicio_reserva=None, hora_fin_reserva=None, foto=None,
                   dias_reservables=None, telefono=None, info_contacto=None,
                   latitud=None, longitud=None):
            sucursal = ExpSucursal.get_sucursal(numero)

            if dias_reservables is not None:
                dias_reservables_list = [
                    dias_reservables.lunes,
                    dias_reservables.martes,
                    dias_reservables.miercoles,
                    dias_reservables.jueves,
                    dias_reservables.viernes,
                    dias_reservables.sabado,
                    dias_reservables.domingo
                ]
            else:
                dias_reservables_list = None

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('modificar_sucursal',
                                                      user.persona.rol))):
                sucursal, result = ExpSucursal.modificar_sucursal(
                    sucursal, nombre, direccion, foto,
                    hora_inicio_reserva, hora_fin_reserva,
                    dias_reservables_list, telefono, info_contacto,
                    latitud, longitud)
            else:
                sucursal = None
                result = False

            return SucursalQL.ModificarSucursal(ok=result, numero=numero)

    def ver_sucursales(self, info, cuil_restaurante=None, **kwargs):
        if cuil_restaurante is None:
            restaurante = ExpRestaurante.get_restaurante(cuil_restaurante)
        else:
            restaurante = ExpRestaurante.get_restaurante(cuil_restaurante)

        if restaurante is None:
            return 404

        return ExpSucursal.get_lista_sucursales(restaurante)

    class AsignarCarta(Mutation):
        class Arguments:
            numero_carta = graphene.Int(required=True)
            numero_sucursal = graphene.Int(required=True)

        ok = graphene.Boolean()
        oferta = graphene.Field(SucursalOfertaNode)

        def mutate(self, info, numero_carta, numero_sucursal):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            carta = ExpCarta.get_carta(numero_carta)

            oferta, ok = ExpSucursal.asignar_carta(sucursal, carta)

            return SucursalQL.AsignarCarta(ok=ok,
                                           oferta=oferta)

    class AbrirCaja(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int(required=True)
            # monto_inicial = graphene.Float(required=True, precision=(8, 2))
            monto_inicial = graphene.String(required=True)
            comentarios = graphene.String()

        ok = graphene.Boolean()
        sucursal = graphene.Field(SucursalNode)

        def mutate(self, info, numero_sucursal,
                   monto_inicial, comentarios=None):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('abrir_caja',
                                                      user.persona.rol)
                        and user.persona.sucursal == sucursal)):
                cajero = (user.persona
                          if user.tipo_usuario.nombre == 'empleado' else None)
                sucursal, ok = ExpSucursal.abrir_caja(sucursal,
                                                      monto_inicial,
                                                      comentarios,
                                                      cajero)
            else:
                ok = False

            if not ok:
                sucursal = None

            return SucursalQL.AbrirCaja(ok=ok, sucursal=sucursal)

    class CerrarCaja(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int(required=True)
            # monto_final_estimado = graphene.Float(required=True,)
            monto_final_real = graphene.String(required=True)
            comentarios = graphene.String()

        ok = graphene.Boolean()
        sucursal = graphene.Field(SucursalNode)

        def mutate(self, info, numero_sucursal,  # monto_final_estimado,
                   monto_final_real, comentarios=None):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('cerrar_caja',
                                                      user.persona.rol)
                        and user.persona.sucursal == sucursal)):
                sucursal, ok = ExpSucursal.cerrar_caja(sucursal,
                                                       # monto_final_estimado,
                                                       monto_final_real,
                                                       comentarios)
            else:
                ok = False

            if not ok:
                sucursal = None

            return SucursalQL.AbrirCaja(ok=ok, sucursal=sucursal)

    class DeshabilitarSucursal(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int(required=True)

        ok = graphene.Int()

        def mutate(self, info, numero_sucursal):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)

            user = info.context.user
            if ExpAuth.verificar_permiso('deshabilitar_sucursal',
                                         user.persona.rol):
                ok = ExpSucursal.deshabilitar_sucursal(sucursal)
            else:
                ok = 401

            return SucursalQL.DeshabilitarSucursal(ok=ok)
