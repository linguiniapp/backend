import graphene
from graphene import InputObjectType


class ClienteInput(InputObjectType):
    nombre = graphene.String(required=True)
    apellido = graphene.String(required=True)
    sexo = graphene.String()
    email = graphene.String(required=True)
    telefono = graphene.String()
    fecha_nacimiento = graphene.Date()
    dni = graphene.String(required=True)
    direccion = graphene.String()


class EmpleadoInput(InputObjectType):
    nombre = graphene.String(required=True)
    apellido = graphene.String(required=True)
    sexo = graphene.String()
    email = graphene.String()
    telefono = graphene.String()
    fecha_nacimiento = graphene.Date()
    cuil = graphene.String(required=True)


class VerticeInput(InputObjectType):
    x = graphene.Int()
    y = graphene.Int()


class WeekMaskInput(InputObjectType):
    lunes = graphene.Boolean(default_value=False)
    martes = graphene.Boolean(default_value=False)
    miercoles = graphene.Boolean(default_value=False)
    jueves = graphene.Boolean(default_value=False)
    viernes = graphene.Boolean(default_value=False)
    sabado = graphene.Boolean(default_value=False)
    domingo = graphene.Boolean(default_value=False)


class DetalleProductoInput(InputObjectType):
    numero = graphene.Int(required=True)
    cantidad = graphene.Int()
    precio = graphene.Float(precision=(6, 2))


class CartaProductoInput(InputObjectType):
    numero = graphene.Int(required=True)
    cantidad = graphene.Int(required=True)
    comentarios = graphene.String()


class DetalleCartaProductoInput(InputObjectType):
    numero = graphene.Int(required=True)
    precio = graphene.Float(required=True, precision=(6, 2))


class RecompensaInput(InputObjectType):
    codigo = graphene.Int(required=True)
    cantidad = graphene.Int(required=True)


class DatosFactura(InputObjectType):
    tipo_factura = graphene.String(required=True)
    tipo_id_cliente = graphene.String(required=True)
    id_cliente = graphene.String(required=True)
    nombre = graphene.String(required=True)
    apellido = graphene.String(required=True)
    email = graphene.String(required=True)


class MedioPagoInput(InputObjectType):
    codigo = graphene.Int(required=True)
    importe = graphene.String(required=True)
    cuotas = graphene.Int()
    datos_factura = DatosFactura(required=True)
    datos_pago = graphene.String()
    dni_cuenta_puntos = graphene.String()


# class FileUpload(graphene.Scalar):
#     @staticmethod
#     def serialize(value):
#         return None
#
#     @staticmethod
#     def parse_literal(node):
#         return None
#
#     @staticmethod
#     def parse_value(value):
#         return value
