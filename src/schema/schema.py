from graphene import ObjectType
from graphql import GraphQLError

from schema.auth import AuthQL
from schema.carta import CartaQL
from schema.cliente import ClienteQL
from schema.empleado import EmpleadoQL
from schema.estadia import EstadiaQL
from schema.lista_espera import ListaEsperaQL
from schema.mesa import MesaQL
from schema.nodes import *
from schema.pago import PagoQL
from schema.pedido import PedidoQL
from schema.producto import ProductoQL
from schema.promocion import PromocionQL
from schema.recompensa import RecompensaQL
from schema.reserva import ReservaQL
from schema.restaurante import RestauranteQL
# from schema.sector import SectorQL
from schema.sucursal import SucursalQL
from schema.reporte import ReporteQL


def default_resolver_true(self, info, **kwargs):
    raise GraphQLError("458 - I'm a teapot")


class Mutation(ObjectType):
    login = AuthQL.Login.Field()
    recover_password = AuthQL.RecoverPassword.Field()
    reset_password = AuthQL.ResetPassword.Field()
    change_password = AuthQL.ChangePassword.Field()
    change_admin_email = AuthQL.ChangeAdminEmail.Field()

    crear_usuario_cliente = AuthQL.CrearUsuarioCliente.Field()
    cambiar_estado_usuario = AuthQL.CambiarEstadoUsuario.Field()

    modificar_cliente = ClienteQL.ModificarCliente.Field()
    deshabilitar_cliente = ClienteQL.DeshabilitarCliente.Field()
    suspender_cliente = ClienteQL.SuspenderCliente.Field()

    abrir_lista_espera = ListaEsperaQL.AbrirListaEspera.Field()
    agregar_lista_espera = ListaEsperaQL.AgregarListaEspera.Field()
    eliminar_lista_espera = ListaEsperaQL.EliminarListaEspera.Field()
    cerrar_lista_espera = ListaEsperaQL.CerrarListaEspera.Field()

    abrir_caja = SucursalQL.AbrirCaja.Field()
    cerrar_caja = SucursalQL.CerrarCaja.Field()

    crear_restaurante = RestauranteQL.CrearRestaurante.Field()
    modificar_restaurante = RestauranteQL.ModificarRestaurante.Field()

    crear_sucursal = SucursalQL.CrearSucursal.Field()
    modificar_sucursal = SucursalQL.ModificarSucursal.Field()
    deshabilitar_sucursal = SucursalQL.DeshabilitarSucursal.Field()

    # crear_sector = SectorQL.CrearSector.Field()
    # modificar_sector = SectorQL.ModificarSector.Field()
    # eliminar_sector = SectorQL.EliminarSector.Field()

    crear_mesa = MesaQL.CrearMesa.Field()
    modificar_mesa = MesaQL.ModificarMesa.Field()
    eliminar_mesa = MesaQL.EliminarMesa.Field()

    crear_reserva = ReservaQL.CrearReserva.Field()
    modificar_reserva = ReservaQL.ModificarReserva.Field()
    anular_reserva = ReservaQL.AnularReserva.Field()

    crear_producto = ProductoQL.CrearProducto.Field()
    modificar_producto = ProductoQL.ModificarProducto.Field()
    deshabilitar_producto = ProductoQL.EliminarProducto.Field()

    crear_promocion = PromocionQL.CrearPromocion.Field()
    modificar_promocion = PromocionQL.ModificarPromocion.Field()
    deshabilitar_promocion = PromocionQL.DeshabilitarPromocion.Field()

    crear_recompensa = RecompensaQL.CrearRecompensa.Field()
    # modificar_recompensa = graphene.Int(resolver=default_resolver_true)
    # deshabilitar_recompensa = graphene.Int(resolver=default_resolver_true)

    crear_categoria = ProductoQL.CrearCategoria.Field()
    modificar_categoria = ProductoQL.ModificarCategoria.Field()
    eliminar_categoria = ProductoQL.EliminarCategoria.Field()

    agregar_pedido_remoto = PedidoQL.AgregarPedidoRemoto.Field()
    agregar_pedido_presencial = PedidoQL.AgregarPedidoPresencial.Field()
    anular_pedido = PedidoQL.AnularPedido.Field()
    confirmar_pedido = PedidoQL.ConfirmarPedido.Field()

    # crear_oferta_sucursal = SucursalQL.CrearOfertaSucursal.Field()
    # agregar_promocion_sucursal = graphene.Int(resolver=default_resolver_true)
    # agregar_recompensa_sucursal = graphene.Int(resolver=default_resolver_true)
    agregar_carta_sucursal = SucursalQL.AsignarCarta.Field()

    crear_carta = CartaQL.CrearCarta.Field()
    # modificar_carta = CartaQL.ModificarCarta.Field()

    crear_empleado = EmpleadoQL.CrearEmpleado.Field()
    modificar_empleado = EmpleadoQL.ModificarEmpleado.Field()
    habilitar_empleado = EmpleadoQL.HabilitarEmpleado.Field()
    deshabilitar_empleado = EmpleadoQL.DeshabilitarEmpleado.Field()
    suspender_empleado = EmpleadoQL.SuspenderEmpleado.Field()

    crear_rol = AuthQL.CrearRol.Field()
    modificar_rol = AuthQL.ModificarRol.Field()
    eliminar_rol = AuthQL.EliminarRol.Field()

    asignar_rol = AuthQL.AsignarRol.Field()
    quitar_rol = AuthQL.QuitarRol.Field()

    avanzar_estado_pedido = PedidoQL.AvanzarEstado.Field()

    check_in = EstadiaQL.CheckIn.Field()
    check_out = EstadiaQL.CheckOut.Field()

    ver_estadia = PagoQL.EmitirPrefactura.Field()
    calificar_estadia = EstadiaQL.CalificarEstadia.Field()

    realizar_pago_cliente = PagoQL.RealizarPagoCliente.Field()
    realizar_pago_caja = PagoQL.RealizarPagoCaja.Field()
    acreditar_pago_efectivo = PagoQL.AcreditarPagoEfectivo.Field()


class Query(ObjectType):
    node = relay.Node.Field()

    verificar_usuario = graphene.Field(UsuarioNode)
    resolve_verificar_usuario = AuthQL.verificar_usuario

    ver_permisos = SQLAlchemyConnectionField(AccionConnection)
    resolve_ver_permisos = AuthQL.ver_permisos

    ver_clientes = SQLAlchemyConnectionField(
        ClienteConnection,
        lista_estados=graphene.List(graphene.String)
    )
    resolve_ver_clientes = ClienteQL.ver_clientes

    ver_cliente = graphene.Field(
        ClienteNode,
        numero_cliente=graphene.Int(),
        dni_cliente=graphene.Int())
    resolve_ver_cliente = ClienteQL.ver_cliente

    ver_lista_espera = graphene.Field(
        ListaEsperaNode,
        numero_sucursal=graphene.Int(required=True)
    )
    resolve_ver_lista_espera = ListaEsperaQL.ver_lista_espera

    ver_sucursales = SQLAlchemyConnectionField(
        SucursalConnection,
        cuil_restaurante=graphene.String()
    )
    resolve_ver_sucursales = SucursalQL.ver_sucursales

    ver_sucursal = graphene.Field(
        SucursalNode,
        numero_sucursal=graphene.Int(required=True)
    )
    resolve_ver_sucursal = SucursalQL.get_sucursal

    ver_caja_activa = graphene.Field(
        CajaNode,
        numero_sucursal=graphene.Int(required=True)
    )
    resolve_ver_caja_activa = SucursalQL.ver_caja_activa

    ver_reservas = SQLAlchemyConnectionField(
        ReservaConnection,
        numero_sucursal=graphene.Int(required=True),
        fecha_hora_desde=graphene.DateTime(),
        fecha_hora_hasta=graphene.DateTime()
    )
    resolve_ver_reservas = ReservaQL.ver_reservas

    ver_reserva = graphene.Field(
        ReservaNode,
        numero_reserva=graphene.Int(required=True)
    )
    resolve_ver_reserva = ReservaQL.ver_reserva

    ver_producto = graphene.Field(
        ProductoNode,
        codigo_producto=graphene.Int(required=True)
    )
    resolve_ver_producto = ProductoQL.get_producto

    ver_productos = SQLAlchemyConnectionField(
        ProductoConnection,
        categorias=graphene.List(graphene.Int)
    )
    resolve_ver_productos = ProductoQL.ver_productos

    ver_carta = graphene.Field(
        CartaNode,
        numero_sucursal=graphene.Int(required=True)
    )
    resolve_ver_carta = CartaQL.ver_carta

    ver_promociones = SQLAlchemyConnectionField(
        PromocionConnection,
        numero_sucursal=graphene.Int(required=True))
    resolve_ver_promociones = PromocionQL.ver_promociones

    ver_promociones_activas = SQLAlchemyConnectionField(
        PromocionConnection,
        numero_sucursal=graphene.Int(required=True))
    resolve_ver_promociones_activas = PromocionQL.ver_promociones_activas

    ver_pedido = graphene.Field(
        PedidoNode,
        numero_pedido=graphene.Int(),
        numero_reserva=graphene.Int(),
        numero_sucursal=graphene.Int(),
        numero_mesa=graphene.Int()
    )
    resolve_ver_pedido = PedidoQL.ver_pedido

    ver_pedidos = SQLAlchemyConnectionField(
        PedidoConnection,
        numero_sucursal=graphene.Int(),
        nombre_estados=graphene.List(graphene.String),
        monitor=graphene.Boolean()
    )
    resolve_ver_pedidos = PedidoQL.ver_pedidos

    ver_pedido_reserva = SQLAlchemyConnectionField(
        PedidoConnection,
        numero_reserva=graphene.Int())
    resolve_ver_pedido_reserva = PedidoQL.ver_pedido_reserva

    ver_periodicidad = SQLAlchemyConnectionField(PeriodicidadConnection)
    resolve_ver_periodicidad = PromocionQL.ver_periodicidad

    ver_categorias = SQLAlchemyConnectionField(CategoriaConnection)
    resolve_ver_categoroas = ProductoQL.ver_categorias

    ver_categoria = graphene.Field(CategoriaNode,
                                   cod_categoria=graphene.Int(required=True))
    resolve_ver_categoria = ProductoQL.ver_categoria

    ver_roles = SQLAlchemyConnectionField(RolConnection)
    resolve_ver_roles = AuthQL.ver_roles

    ver_acciones = SQLAlchemyConnectionField(AccionConnection)
    resolve_ver_acciones = AuthQL.ver_acciones

    ver_empleados = SQLAlchemyConnectionField(EmpleadoConnection,
                                              numero_sucursal=graphene.Int(),
                                              activos=graphene.Boolean())
    resolve_ver_empleados = EmpleadoQL.ver_empleados

    ver_empleado = graphene.Field(EmpleadoNode,
                                  numero_o_cuil=graphene.String())
    resolve_ver_empleado = EmpleadoQL.ver_empleado

    ver_recompensas = SQLAlchemyConnectionField(RecompensaConnection)
    resolve_ver_recompensas = RecompensaQL.ver_recompensas

    # Reportes
    ver_reporte_caja = graphene.Field(
        DTOReporteCaja,
        numero_sucursal=graphene.Int(required=True),
        fecha_desde=graphene.Date(required=True),
        fecha_hasta=graphene.Date()
    )
    resolve_ver_reporte_caja = ReporteQL.reporte_caja

    ver_reporte_mozos = graphene.Field(
        DTOReporteMozos,
        numero_sucursal=graphene.Int(required=True),
        fecha_desde=graphene.Date(required=True),
        fecha_hasta=graphene.Date()
    )
    resolve_ver_reporte_mozos = ReporteQL.reporte_mozos

    ver_reporte_calificaciones = graphene.Field(
        DTOReporteCalificaciones,
        numero_sucursal=graphene.Int(required=True),
        fecha_desde=graphene.Date(required=True),
        fecha_hasta=graphene.Date()
    )
    resolve_ver_reporte_calificaciones = ReporteQL.reporte_calificaciones

    ver_reporte_ventas = graphene.Field(
        DTOReporteVentas,
        numero_sucursal=graphene.Int(),
        fecha_desde=graphene.Date(required=True),
        fecha_hasta=graphene.Date()
    )
    resolve_ver_reporte_ventas = ReporteQL.reporte_ventas

    ver_reporte_productos = graphene.Field(
        DTOReporteProductos,
        numero_sucursal=graphene.Int(),
        fecha_desde=graphene.Date(required=True),
        fecha_hasta=graphene.Date()
    )
    resolve_ver_reporte_productos = ReporteQL.reporte_productos


schema = graphene.Schema(query=Query, mutation=Mutation)
