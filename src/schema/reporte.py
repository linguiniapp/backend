from datetime import date, datetime

from graphql import GraphQLError

from exp_sucursal import ExpSucursal
from exp_reporte import ExpReporte
from auth.exp_auth import ExpAuth
from schema.nodes import DTOReporteCaja
from schema.nodes import DTOReporteMozos, ReporteMozoNode
from schema.nodes import DTOReporteCalificaciones
from schema.nodes import DTOReporteVentas, ReporteVentaNodo
from schema.nodes import DTOReporteProductos, ReporteProductoNode


class ReporteQL:

    def reporte_caja(self, info, numero_sucursal, fecha_desde,
                     fecha_hasta=date.today(), **kwargs):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)

        user = info.context.user
        if user.tipo_usuario.nombre not in ['admin', 'empleado']:
            raise GraphQLError('403:Acceso denegado al método')
        elif (ExpAuth.verificar_permiso('reporte_caja', user.persona.rol)
                and (user.persona.sucursal == sucursal
                     if (user.tipo_usuario.nombre == 'empleado'
                         and user.persona.sucursal) else True)):
            cajas = ExpReporte.obtener_reporte_caja(sucursal,
                                                    fecha_desde,
                                                    fecha_hasta)
        else:
            raise GraphQLError('401:No tiene permisos para ejecutar el método')

        return DTOReporteCaja(
            fecha_hora=datetime.now(),
            fecha_desde=fecha_desde,
            fecha_hasta=fecha_hasta,
            sucursal=sucursal,
            cajas=cajas
        )

    def reporte_mozos(self, info, numero_sucursal, fecha_desde,
                      fecha_hasta=date.today(), **kwargs):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)

        user = info.context.user
        if user.tipo_usuario.nombre not in ['admin', 'empleado']:
            raise GraphQLError('403:Acceso denegado al método')
        elif ExpAuth.verificar_permiso('reporte_mozos', user.persona.rol):
            mozos = ExpReporte.obtener_reporte_mozos(sucursal,
                                                     fecha_desde,
                                                     fecha_hasta)
        else:
            raise GraphQLError('401:No tiene permisos para ejecutar el método')

        return DTOReporteMozos(
            fecha_hora=datetime.now(),
            fecha_desde=fecha_desde,
            fecha_hasta=fecha_hasta,
            mozos=mozos,
            numero_sucursal=sucursal.numero,
            nombre_sucursal=sucursal.nombre
        )

    def reporte_calificaciones(self, info, numero_sucursal, fecha_desde,
                               fecha_hasta=date.today(), **kwargs):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)

        user = info.context.user
        if user.tipo_usuario.nombre not in ['admin', 'empleado']:
            raise GraphQLError('403:Acceso denegado al método')
        elif (ExpAuth.verificar_permiso('reporte_calificaciones',
                                        user.persona.rol)):
            califs, prom, det = ExpReporte.obtener_reporte_calificaciones(
                sucursal,
                fecha_desde,
                fecha_hasta
            )
        else:
            raise GraphQLError('401:No tiene permisos para ejecutar el método')

        return DTOReporteCalificaciones(
            numero_sucursal=numero_sucursal,
            nombre_sucursal=sucursal.nombre,
            fecha_hora=datetime.now(),
            fecha_desde=fecha_desde,
            fecha_hasta=fecha_hasta,
            calificaciones=califs,
            promedio_sucursal=prom,
            detalle_sucursal=det
        )

    def reporte_ventas(self, info, fecha_desde, fecha_hasta=date.today(),
                       numero_sucursal=None, **kwargs):
        user = info.context.user
        if user.tipo_usuario.nombre not in ['admin', 'empleado']:
            raise GraphQLError('403:Acceso denegado al método')
        elif ExpAuth.verificar_permiso('reporte_ventas', user.persona.rol):
            if numero_sucursal:
                sucursal = ExpSucursal.get_sucursal(numero_sucursal)
                if sucursal is None:
                    raise GraphQLError('404:No existe la sucursal')
            else:
                sucursal = None
            ventas = ExpReporte.obtener_reporte_ventas(
                fecha_desde,
                fecha_hasta,
                sucursal
            )
        else:
            raise GraphQLError('401:No tiene permisos para ejecutar el método')

        return DTOReporteVentas(
            fecha_hora=datetime.now(),
            fecha_desde=fecha_desde,
            fecha_hasta=fecha_hasta,
            ventas=ventas
        )

    def reporte_productos(self, info, fecha_desde,
                          fecha_hasta=date.today(), **kwargs):
        user = info.context.user
        if user.tipo_usuario.nombre not in ['admin', 'empleado']:
            raise GraphQLError('403:Acceso denegado al método')
        elif ExpAuth.verificar_permiso('reporte_productos', user.persona.rol):
            productos = ExpReporte.obtener_reporte_productos(
                fecha_desde,
                fecha_hasta
            )
        else:
            raise GraphQLError('401:No tiene permisos para ejecutar el método')

        return DTOReporteProductos(
            fecha_hora=datetime.now(),
            fecha_desde=fecha_desde,
            fecha_hasta=fecha_hasta,
            productos=productos
        )
