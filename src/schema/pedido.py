import graphene
from graphene import Mutation

from auth.exp_auth import ExpAuth
from exp_carta import ExpCarta
from exp_estadia import ExpEstadia
from exp_mesa import ExpMesa
from exp_pedido import ExpPedido
from exp_producto import ExpProducto
from exp_promocion import ExpPromocion
from exp_recompensa import ExpRecompensa
from exp_reserva import ExpReserva
from exp_sucursal import ExpSucursal
from schema.extra_types import CartaProductoInput
from schema.nodes import PedidoNode


class PedidoQL:

    def ver_pedido_reserva(self, info, numero_reserva, **kwargs):
        reserva = ExpReserva.get_reserva(numero_reserva)

        user = info.context.user
        if (user.tipo_usuario.nombre == 'cliente'
                and user.persona in [reserva.cliente, reserva.owner]):
            return ExpPedido.ver_pedido_reserva(reserva)
        elif (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_pedidos',
                                                  user.persona.rol)
                    and ExpAuth.verificar_permiso('ver_reservas',
                                                  user.persona.rol))):
            return ExpPedido.ver_pedido_reserva(reserva)
        else:
            return 401

    def ver_pedido(self, info, numero_pedido=None,
                   numero_reserva=None, numero_sucursal=None,
                   numero_mesa=None, **kwargs):
        user = info.context.user

        if numero_pedido is None:
            if numero_reserva is not None:
                reserva = ExpReserva.get_reserva(numero_reserva)
                if reserva is not None:
                    pedido = ExpPedido.ver_pedido_reserva(reserva)
                else:
                    return 404
            elif user is not None and user.tipo_usuario.nombre == 'cliente':
                estadia = ExpEstadia.ver_estadia_actual_cliente(user.persona)
                if estadia is not None:
                    pedido = ExpPedido.get_pedido_abierto_estadia(estadia)
                else:
                    return 404
            elif numero_mesa is not None and numero_sucursal is not None:
                sucursal = ExpSucursal.get_sucursal(numero_sucursal)
                if sucursal is not None:
                    mesa = ExpMesa.get_mesa(sucursal, numero_mesa)
                else:
                    return 404
                if mesa is not None:
                    estadia = ExpEstadia.ver_estadia_actual_mesa(mesa)
                else:
                    return 404
                if estadia is not None:
                    pedido = ExpPedido.get_pedido_abierto_estadia(estadia)
                else:
                    return 404
            else:
                return 401
        else:
            pedido = ExpPedido.get_pedido(numero_pedido)
            if pedido is None:
                return 404

        if user.tipo_usuario.nombre == 'cliente':
            if pedido.tipo.nombre == 'remoto':
                cliente = pedido.reserva.cliente
                owner = pedido.reserva.owner
                if user.persona == cliente or user.persona == owner:
                    return pedido or 404
                else:
                    return 401
            elif (pedido.tipo.nombre == 'presencial cliente'
                    or pedido.tipo.nombre == 'presencial mozo'):
                clientes = pedido.estadia.clientes
                if user.persona in clientes:
                    return pedido or 404
                else:
                    return 401
            else:
                return 500
        elif (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_pedidos',
                                                  user.persona.rol))):
            return pedido or 404
        else:
            return 401

    def ver_pedidos(self, info, numero_sucursal=None, nombre_estados=tuple([]),
                    monitor=False, **kwargs):

        sucursal = ExpSucursal.get_sucursal(numero_sucursal)

        user = info.context.user
        if user.tipo_usuario.nombre == 'cliente':
            return ExpPedido.ver_pedidos(user.persona, estados=nombre_estados)
        elif (user.tipo_usuario.nombre == 'admin'
                or (user.tipo_usuario.nombre == 'empleado'
                    and ExpAuth.verificar_permiso('ver_pedidos',
                                                  user.persona.rol))):
            return ExpPedido.ver_pedidos(monitor=monitor,
                                         estados=nombre_estados,
                                         sucursal=sucursal)
        else:
            return [401]

    class AgregarPedidoRemoto(Mutation):
        class Arguments:
            numero_reserva = graphene.Int(required=True)
            lista_productos = graphene.List(CartaProductoInput)
            lista_promociones = graphene.List(CartaProductoInput)
            lista_recompensas = graphene.List(CartaProductoInput)
            comentarios = graphene.String()

        ok = graphene.Boolean()
        pedido = graphene.Field(PedidoNode)

        def mutate(self, info, numero_reserva, lista_productos=tuple([]),
                   comentarios=None, lista_promociones=tuple([]),
                   lista_recompensas=tuple([])):

            reserva = ExpReserva.get_reserva(numero_reserva)
            carta = ExpCarta.ver_carta(reserva.sucursal)

            pedido_productos = []
            for item in lista_productos:
                producto = ExpProducto.get_producto(item.numero)
                carta_prod = ExpCarta.ver_producto_carta(carta, producto)
                if carta_prod is None:
                    continue
                else:
                    pedido_productos.append((carta_prod,
                                             item.cantidad,
                                             item.comentarios))

            pedido_promociones = [(ExpPromocion.get_promocion(prom.numero),
                                   prom.cantidad)
                                  for prom in lista_promociones]
            pedido_recompensas = [(ExpRecompensa.get_recompensa(rec.numero),
                                   rec.cantidad)
                                  for rec in lista_recompensas]

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('pedido_remoto',
                                                      user.persona.rol))
                    or (user.tipo_usuario.nombre == 'cliente'
                        and user.persona in [reserva.cliente, reserva.owner])):
                pedido_existente = ExpPedido.ver_pedido_reserva(reserva)
                if pedido_existente:
                    pedido, ok = ExpPedido.modificar_pedido(
                        pedido_existente, pedido_productos, comentarios,
                        pedido_promociones, pedido_recompensas
                    )
                else:
                    pedido, ok = ExpPedido.crear_pedido("remoto",
                                                        reserva,
                                                        pedido_productos,
                                                        comentarios,
                                                        pedido_promociones,
                                                        pedido_recompensas)
            else:
                ok = False
                pedido = None

            return PedidoQL.AgregarPedidoRemoto(ok=ok, pedido=pedido)

    class AgregarPedidoPresencial(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int(required=True)
            numero_mesa = graphene.Int()
            comentarios = graphene.String()
            lista_productos = graphene.List(CartaProductoInput)
            lista_promociones = graphene.List(CartaProductoInput)
            lista_recompensas = graphene.List(CartaProductoInput)

        ok = graphene.Int()
        pedido = graphene.Field(PedidoNode)

        def mutate(self, info, numero_sucursal, comentarios=None,
                   lista_productos=tuple([]), numero_mesa=None,
                   lista_promociones=tuple([]), lista_recompensas=tuple([])):

            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            carta = ExpCarta.ver_carta(sucursal)

            pedido_productos = []
            for item in lista_productos:
                producto = ExpProducto.get_producto(item.numero)
                carta_prod = ExpCarta.ver_producto_carta(carta, producto)
                if carta_prod is None:
                    return PedidoQL.AgregarPedidoPresencial(ok=404,
                                                            pedido=None)
                else:
                    pedido_productos.append((carta_prod,
                                             item.cantidad,
                                             item.comentarios))

            pedido_promociones = [(ExpPromocion.get_promocion(prom.numero),
                                   prom.cantidad)
                                  for prom in lista_promociones]
            pedido_recompensas = [(ExpRecompensa.get_recompensa(rec.numero),
                                   rec.cantidad)
                                  for rec in lista_recompensas]

            user = info.context.user
            if user.tipo_usuario.nombre == 'cliente':
                tipo_pedido = 'presencial cliente'
                estadia = ExpEstadia.ver_estadia_actual_cliente(user.persona)
            elif (user.tipo_usuario.nombre == 'admin' or
                    (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('pedido_remoto',
                                                      user.persona.rol)
                        and numero_mesa is not None)):
                tipo_pedido = 'presencial mozo'
                mesa = ExpMesa.get_mesa(sucursal, numero_mesa)
                if mesa is None:
                    return PedidoQL.AgregarPedidoPresencial(ok=404,
                                                            pedido=None)
                estadia = ExpEstadia.ver_estadia_actual_mesa(mesa)
                if estadia is None:
                    estadia = ExpEstadia.autogenerar_estadia(sucursal,
                                                             mesa,
                                                             user.persona)
            else:
                return PedidoQL.AgregarPedidoPresencial(ok=401, pedido=None)

            if estadia is None:
                return PedidoQL.AgregarPedidoPresencial(ok=400, pedido=None)

            pedido_existente = ExpPedido.get_pedido_abierto_estadia(
                estadia
            )
            if pedido_existente is None:
                pedido, ok = ExpPedido.crear_pedido(
                    tipo_pedido,
                    estadia,
                    pedido_productos,
                    comentarios,
                    pedido_promociones,
                    pedido_recompensas
                )
            else:
                pedido, ok = ExpPedido.modificar_pedido(
                    pedido_existente,
                    pedido_productos,
                    comentarios,
                    pedido_promociones,
                    pedido_recompensas
                )

            return PedidoQL.AgregarPedidoPresencial(ok=ok, pedido=pedido)

    class AnularPedido(Mutation):
        class Arguments:
            numero_pedido = graphene.Int(required=True)

        ok = graphene.Int()
        pedido = graphene.Field(PedidoNode)

        def mutate(self, info, numero_pedido):
            pedido = ExpPedido.get_pedido(numero_pedido)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'cliente'
                        and ((user.persona in pedido.estadia.clientes)
                             if pedido.tipo.nombre == 'remoto'
                             else user.persona in [pedido.reserva.cliente,
                                                   pedido.reserva.owner]))
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('anular_pedido',
                                                      user.persona.rol))):
                pedido, ok = ExpPedido.anular_pedido(pedido)
            else:
                ok = 401
                pedido = None

            return PedidoQL.AnularPedido(ok=ok, pedido=pedido)

    class ConfirmarPedido(Mutation):
        class Arguments:
            numero_pedido = graphene.Int(required=True)

        ok = graphene.Int()
        pedido = graphene.Field(PedidoNode)

        def mutate(self, info, numero_pedido):
            pedido = ExpPedido.get_pedido(numero_pedido)

            if pedido.tipo.nombre == 'remoto':
                return PedidoQL.AnularPedido(ok=400, pedido=None)

            user = info.context.user
            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'cliente'
                        and user.persona in pedido.estadia.clientes)
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('confirmar_pedido',
                                                      user.persona.rol))):
                pedido, ok = ExpPedido.confirmar_pedido(pedido)
            else:
                ok = 401
                pedido = None

            return PedidoQL.AnularPedido(ok=ok, pedido=pedido)

    # def get_pedido_qr(self, info, numero_pedido, **kwargs):
    #     pedido = ExpPedido.get_pedido(numero_pedido)
    #     return ExpPedido.get_pedido_qr(pedido)

    class AvanzarEstado(Mutation):
        class Arguments:
            token = graphene.String(required=True)

        ok = graphene.Int()
        pedido = graphene.Field(PedidoNode)

        def mutate(self, info, qr):
            pedido = ExpPedido.get_pedido_from_qr(qr)
            if pedido is None:
                ok = 400
            else:
                pedido, ok = ExpPedido.avanzar_estado(pedido)

            return PedidoQL.AvanzarEstado(ok=ok, pedido=pedido)
