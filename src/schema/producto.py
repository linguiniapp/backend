import graphene
from graphene import Mutation

from exp_producto import ExpProducto
# from schema.extra_types import FileUpload
from schema.nodes import CategoriaNode
from schema.nodes import ProductoNode


class ProductoQL:

    def get_producto(self, info, codigo_producto, **kwargs):
        return ExpProducto.get_producto(codigo_producto)

    def ver_productos(self, info, categorias=None, **kwargs):
        if categorias is None:
            return ExpProducto.ver_productos()
        else:
            categorias_verificadas = []
            for c in categorias:
                cat = ExpProducto.get_categoria(c)
                if cat is not None:
                    categorias_verificadas.append(cat)
            return ExpProducto.ver_productos(categorias_verificadas)

    def ver_categorias(self, info, **kwargs):
        return ExpProducto.ver_categorias()

    def ver_categoria(self, info, cod_categoria, **kwargs):
        return ExpProducto.get_categoria(cod_categoria)

    class CrearProducto(Mutation):
        class Arguments:
            nombre = graphene.String(required=True)
            codigo = graphene.Int()
            # foto = FileUpload()
            foto = graphene.String()
            numeros_categorias = graphene.List(graphene.Int)
            descripcion = graphene.String()

        ok = graphene.Boolean()
        producto = graphene.Field(ProductoNode)

        def mutate(self, info, nombre, codigo=None, foto=None,
                   numeros_categorias=tuple([]), descripcion=''):
            # if foto is not None:
                # foto = foto.stream.read()

            categorias = []
            for cat in numeros_categorias:
                categorias.append(ExpProducto.get_categoria(cat))

            producto, ok = ExpProducto.crear_producto(nombre, codigo,
                                                      foto, categorias,
                                                      descripcion)

            return ProductoQL.CrearProducto(ok=ok, producto=producto)

    class ModificarProducto(Mutation):
        class Arguments:
            codigo = graphene.Int(required=True)
            nombre = graphene.String()
            foto = graphene.String()
            numeros_categorias = graphene.List(graphene.Int)
            descripcion = graphene.String()

        ok = graphene.Boolean()
        producto = graphene.Field(ProductoNode)

        def mutate(self, info, codigo, nombre=None, foto=None,
                   numeros_categorias=tuple([]), descripcion=''):
            producto = ExpProducto.get_producto(codigo)

            if producto is None:
                return ProductoQL.ModificarProducto(ok=False, producto=None)

            categorias = []
            for cat in numeros_categorias:
                categorias.append(ExpProducto.get_categoria(cat))

            producto, ok = ExpProducto.modificar_producto(producto, nombre,
                                                          foto, categorias,
                                                          descripcion)
            return ProductoQL.ModificarProducto(ok=ok, producto=producto)

    class EliminarProducto(Mutation):
        class Arguments:
            codigo = graphene.Int(required=True)

        ok = graphene.Boolean()
        codigo = graphene.Int()

        def mutate(self, info, codigo):
            ok, producto = ExpProducto.eliminar_producto(codigo)
            return ProductoQL.EliminarProducto(ok=ok, codigo=codigo)

    class CrearCategoria(Mutation):
        class Arguments:
            nombre = graphene.String(required=True)
            foto = graphene.String()

        ok = graphene.Boolean()
        categoria = graphene.Field(CategoriaNode)

        def mutate(self, info, nombre, foto=None):

            cat, ok = ExpProducto.crear_categoria(nombre, foto)
            return ProductoQL.CrearCategoria(ok=ok, categoria=cat)

    class ModificarCategoria(Mutation):
        class Arguments:
            codigo = graphene.Int(required=True)
            nombre = graphene.String()
            foto = graphene.String()

        ok = graphene.Boolean()
        categoria = graphene.Field(CategoriaNode)

        def mutate(self, info, codigo, nombre=None, foto=None):
            cat, ok = ExpProducto.modificar_categoria(codigo, nombre, foto)
            return ProductoQL.ModificarCategoria(ok=ok, categoria=cat)

    class EliminarCategoria(Mutation):
        class Arguments:
            codigo = graphene.Int(required=True)

        ok = graphene.Boolean()
        codigo = graphene.Int()

        def mutate(self, info, codigo):
            codigo, ok = ExpProducto.eliminar_categoria(codigo)
            return ProductoQL.EliminarCategoria(ok=ok, codigo=codigo)
