import graphene
from graphene import Mutation

from auth.exp_auth import ExpAuth
from exp_cliente import ExpCliente
from exp_empleado import ExpEmpleado
from exp_estadia import ExpEstadia
from exp_mesa import ExpMesa
from exp_reserva import ExpReserva
from exp_sucursal import ExpSucursal
from schema.nodes import EstadiaNode, PuntuacionNode


class EstadiaQL:

    class CheckIn(Mutation):
        class Arguments:
            numero_reserva = graphene.Int()
            numero_o_dni_clientes = graphene.List(graphene.String)

            # Obligatorio si no hay reserva
            numero_sucursal = graphene.Int()
            numeros_mesas = graphene.List(graphene.Int)
            cantidad_personas = graphene.Int()

        ok = graphene.Int()
        estadia = graphene.Field(EstadiaNode)

        def mutate(self, info, numero_sucursal=None, numero_reserva=None,
                   numero_o_dni_clientes=tuple([]), numeros_mesas=tuple([]),
                   cantidad_personas=None):

            user = info.context.user

            if numero_reserva is not None:
                reserva = ExpReserva.get_reserva(numero_reserva)
                if reserva is None:
                    return EstadiaQL.CheckIn(ok=404, estadia=None)
            else:
                reserva = None
                sucursal = ExpSucursal.get_sucursal(numero_sucursal)
                if sucursal is None:
                    return EstadiaQL.CheckIn(ok=404, estadia=None)
                mesas = []
                for numero_mesa in numeros_mesas:
                    mesa = ExpMesa.get_mesa(sucursal, numero_mesa)
                    if mesa is not None:
                        mesas.append(mesa)

            if user.tipo_usuario.nombre == 'cliente':
                cliente = user.persona
                clientes = None
            elif numero_o_dni_clientes is not None:
                clientes = [ExpCliente.get_cliente(idx)
                            for idx in numero_o_dni_clientes]
                cliente = None
            else:
                if reserva is not None:
                    cliente = reserva.user or reserva.owner
                else:
                    cliente = None
                clientes = []

            if (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'cliente'
                        and (user.persona in [reserva.owner, reserva.cliente]
                            if reserva is not None else True))
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('check_in',
                                                      user.persona.rol))):
                if reserva is not None:
                    estadia = ExpEstadia.ver_estadia_reserva(reserva)
                    if estadia is None:
                        estadia, ok = ExpEstadia.abrir_estadia_reserva(
                            reserva,
                            [cliente] or clientes
                        )
                    else:
                        if cliente or clientes:
                            ExpEstadia.agregar_cliente_estadia(
                                estadia,
                                [cliente] or clientes
                            )
                        ok = 0
                else:
                    if cliente:
                        estadia, ok = ExpEstadia.abrir_estadia_cliente(
                            sucursal,
                            cliente,
                            cantidad_personas,
                            mesas
                        )
                    else:
                        estadia, ok = ExpEstadia.abrir_estadia_mozo(
                            sucursal,
                            cantidad_personas,
                            mesas,
                            clientes
                        )
                ExpEmpleado.asignar_mozo(estadia, user.persona)
            else:
                ok = 401
                estadia = None

            return EstadiaQL.CheckIn(ok=ok, estadia=estadia)

    class CheckOut(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int()
            numero_mesa = graphene.Int()

        ok = graphene.Int()
        estadia = graphene.Field(EstadiaNode)

        def mutate(self, info, numero_sucursal=None, numero_mesa=None):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)

            user = info.context.user
            if user.tipo_usuario.nombre == 'cliente':
                estadia = ExpEstadia.ver_estadia_actual_cliente(user.persona)
                if estadia is None:
                    ok = 404
                else:
                    cliente = user.persona
                    estadia, ok = ExpEstadia.cerrar_estadia_cliente(cliente)
            elif (user.tipo_usuario.nombre == 'admin'
                    or (user.tipo_usuario.nombre == 'empleado'
                        and ExpAuth.verificar_permiso('check_out',
                                                      user.persona.rol)
                        and user.persona.sucursal == sucursal)):
                mesa = ExpMesa.get_mesa(sucursal, numero_mesa)
                if mesa is None:
                    ok = 404
                    estadia = None
                else:
                    estadia = ExpEstadia.ver_estadia_actual_mesa(mesa)
                    if estadia is None:
                        ok = 404
                    else:
                        estadia, ok = ExpEstadia.cerrar_estadia_mozo(estadia)
            else:
                estadia = None
                ok = 401

            return EstadiaQL.CheckOut(ok=ok, estadia=estadia)

    class CalificarEstadia(Mutation):
        class Arguments:
            puntuacion = graphene.Int(required=True)
            comentarios = graphene.String()
            numero_estadia = graphene.Int()

        ok = graphene.Int()
        calificacion = graphene.Field(PuntuacionNode)

        def mutate(self, info, puntuacion, comentarios=None,
                   numero_estadia=None):

            user = info.context.user
            if (user.tipo_usuario.nombre == 'cliente'
                    and ExpAuth.verificar_permiso('calificar_estadia',
                                                  user.persona.rol)):
                if numero_estadia:
                    estadia = ExpEstadia.get_estadia(numero_estadia)
                else:
                    estadia = ExpEstadia.ver_estadia_actual_cliente(
                        user.persona)

                if estadia:
                    sucursal = estadia.sucursal
                    calificacion, ok = ExpEstadia.puntuar_sucursal(
                        puntuacion, user.persona,
                        sucursal, estadia, comentarios)
                else:
                    ok = 400
                    calificacion = None
            elif user.tipo_usuario.nombre != 'cliente':
                ok = 403
                calificacion = None
            else:
                ok = 401
                calificacion = None

            return EstadiaQL.CalificarEstadia(ok=ok, calificacion=calificacion)
