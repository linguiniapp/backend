import graphene
from graphene import Mutation

from schema.nodes import ListaEsperaDetalleNode

from lista_espera import ExpListaEspera
from exp_sucursal import ExpSucursal


class ListaEsperaQL:
    class AbrirListaEspera(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int()

        ok = graphene.Boolean()
        fecha_hora_apertura = graphene.DateTime()

        def mutate(self, info, numero_sucursal):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            lista = ExpListaEspera.abrir_lista_espera(sucursal)
            if lista is None:
                result = False
                fha = None
            else:
                result = True
                fha = lista.fecha_hora_apertura
            return ListaEsperaQL.AbrirListaEspera(ok=result,
                                                  fecha_hora_apertura=fha)

    class AgregarListaEspera(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int()
            nombre = graphene.String()
            cantidad_personas = graphene.Int()

        ok = graphene.Boolean()
        detalle = graphene.Field(ListaEsperaDetalleNode)

        def mutate(self, info, numero_sucursal, nombre, cantidad_personas):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            lista = ExpListaEspera.get_lista_espera_actual(sucursal)
            if lista is None:
                lista = ExpListaEspera.abrir_lista_espera(sucursal)
            detalle = ExpListaEspera.agregar_lista_espera(lista,
                                                          nombre,
                                                          cantidad_personas)
            ok = True
            return ListaEsperaQL.AgregarListaEspera(ok=ok, detalle=detalle)

    class EliminarListaEspera(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int()
            numero_lista = graphene.Int()

        ok = graphene.Boolean()

        def mutate(self, info, numero_sucursal, numero_lista):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            result = ExpListaEspera.eliminar_lista_espera(sucursal,
                                                          numero_lista)
            return ListaEsperaQL.EliminarListaEspera(ok=result)

    class CerrarListaEspera(Mutation):
        class Arguments:
            numero_sucursal = graphene.Int()

        ok = graphene.Boolean()
        fecha_hora_cierre = graphene.DateTime()

        def mutate(self, info, numero_sucursal):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            lista = ExpListaEspera.cerrar_lista_espera(sucursal)
            if lista is None:
                result = False
                fhc = None
            else:
                result = True
                fhc = lista.fecha_hora_cierre
            return ListaEsperaQL.CerrarListaEspera(ok=result,
                                                   fecha_hora_cierre=fhc)

    def ver_lista_espera(self, info, numero_sucursal, **kwargs):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)
        return ExpListaEspera.get_lista_espera_actual(sucursal)
