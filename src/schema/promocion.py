import graphene
from graphene import Mutation

from schema.extra_types import WeekMaskInput, DetalleProductoInput
from schema.nodes import PromocionNode

from exp_promocion import ExpPromocion
from exp_sucursal import ExpSucursal
from exp_producto import ExpProducto


class PromocionQL:

    def ver_promociones(self, info, numero_sucursal, **kwargs):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)
        return ExpPromocion.ver_promociones(sucursal)

    def ver_promociones_activas(self, info, numero_sucursal, **kwargs):
        sucursal = ExpSucursal.get_sucursal(numero_sucursal)
        return ExpPromocion.ver_promociones_activas(sucursal)

    def ver_periodicidad(self, info, **kwargs):
        return ExpPromocion.ver_periodicidad()

    class CrearPromocion(Mutation):
        class Arguments:
            nombre = graphene.String(required=True)
            descripcion = graphene.String()
            precio = graphene.Float()
            descuento = graphene.Float()
            mes_desde = graphene.Int()
            mes_hasta = graphene.Int()
            dia_desde = graphene.Int()
            dia_hasta = graphene.Int()
            hora_desde = graphene.Time()
            hora_hasta = graphene.Time()
            dias_activa = WeekMaskInput()
            periodicidad = graphene.String()
            detalle_productos = graphene.List(DetalleProductoInput,
                                              required=True)

        ok = graphene.Boolean()
        promocion = graphene.Field(PromocionNode)

        def mutate(self, info, nombre, detalle_productos,
                   descripcion=None, precio=None, descuento=None,
                   mes_desde=None, mes_hasta=None, dia_desde=None, dia_hasta=None,
                   hora_desde=None, hora_hasta=None, dias_activa=None,
                   periodicidad='unica'):
            if precio is None and descuento is None:
                ok = False
                promocion = None
            else:
                productos = []
                for det in detalle_productos:
                    prod = ExpProducto.get_producto(det.numero)
                    if prod is not None:
                        productos.append((prod, det.cantidad))
                promocion, ok = ExpPromocion.crear_promocion(
                    nombre, descripcion, precio, descuento,
                    mes_desde, mes_hasta, dia_desde, dia_hasta,
                    hora_desde, hora_hasta, dias_activa,
                    periodicidad, productos)
            return PromocionQL.CrearPromocion(ok=ok, promocion=promocion)

    class ModificarPromocion(Mutation):
        class Arguments:
            numero = graphene.Int(required=True)
            nombre = graphene.String()
            descripcion = graphene.String()
            precio = graphene.Float()
            descuento = graphene.Float()
            mes_desde = graphene.Int()
            mes_hasta = graphene.Int()
            dia_desde = graphene.Int()
            dia_hasta = graphene.Int()
            hora_desde = graphene.Time()
            hora_hasta = graphene.Time()
            dias_activa = WeekMaskInput()
            periodicidad = graphene.String()
            detalle_productos = graphene.List(DetalleProductoInput)

        ok = graphene.Boolean()
        promocion = graphene.Field(PromocionNode)

        def mutate(self, info, numero, nombre, detalle_productos,
                   descripcion=None, precio=None, descuento=None,
                   mes_desde=None, mes_hasta=None,
                   dia_desde=None, dia_hasta=None,
                   hora_desde=None, hora_hasta=None, dias_activa=None,
                   periodicidad='unica'):
            productos = []
            for det in detalle_productos:
                prod = ExpProducto.get_producto(det.numero)
                if prod is not None:
                    productos.append((prod, det.cantidad))

            promocion, ok = ExpPromocion.modificar_promocion(
                numero, nombre, descripcion, precio,
                descuento, mes_desde, mes_hasta,
                dia_desde, dia_hasta, hora_desde,
                hora_hasta, dias_activa, periodicidad, productos)
            return PromocionQL.ModificarPromocion(ok=ok, promocion=promocion)

    class DeshabilitarPromocion(Mutation):
        class Arguments:
            numero = graphene.Int(required=True)

        ok = graphene.Boolean()
        codigo = graphene.Int()

        def mutate(self, info, numero):
            ok = ExpPromocion.deshabilitar_promocion(numero)
            return PromocionQL.DeshabilitarPromocion(ok=ok, codigo=numero)
