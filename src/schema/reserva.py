import graphene
from graphene import Mutation
from graphql import GraphQLError

from auth.exp_auth import ExpAuth
from exp_cliente import ExpCliente
from exp_mesa import ExpMesa
from exp_reserva import ExpReserva
from exp_sucursal import ExpSucursal
from schema.nodes import ReservaNode


class ReservaQL:

    class CrearReserva(Mutation):
        class Arguments:
            fecha_hora = graphene.DateTime(required=True)
            numero_cliente = graphene.Int()  # -----> Uno de estos es requerido
            datos_cliente = graphene.String()  # --->
            cantidad_personas = graphene.Int(required=True)
            numero_sucursal = graphene.Int(required=True)
            numeros_mesas = graphene.List(graphene.Int, required=True)

        ok = graphene.Int()
        reserva = graphene.Field(ReservaNode)

        def mutate(self, info, fecha_hora, numero_sucursal, cantidad_personas,
                   numeros_mesas, numero_cliente=None, datos_cliente=None):

            user = info.context.user
            if ExpAuth.verificar_permiso('crear_reserva', user.persona.rol):
                if numero_cliente is not None:
                    cliente = ExpCliente.get_cliente(numero_cliente)
                elif datos_cliente is not None:
                    cliente = datos_cliente
                elif user.tipo_usuario.nombre == 'cliente':
                    cliente = user.persona
                else:
                    return ReservaQL.CrearReserva(ok=404, reserva=None)

                sucursal = ExpSucursal.get_sucursal(numero_sucursal)
                if sucursal is None:
                    return ReservaQL.CrearReserva(ok=404, reserva=None)

                mesas = []
                for numero_mesa in numeros_mesas:
                    mesas.append(ExpMesa.get_mesa(sucursal, numero_mesa))

                if not mesas:
                    return ReservaQL.CrearReserva(ok=404, reserva=None)

                owner = info.context.user.persona
                reserva, ok = ExpReserva.crear_reserva(
                    fecha_hora, owner, cliente, sucursal,
                    mesas, cantidad_personas)
            else:
                ok = 401
                reserva = None

            return ReservaQL.CrearReserva(ok=ok, reserva=reserva)

    class ModificarReserva(Mutation):
        class Arguments:
            numero = graphene.Int(required=True)
            fecha_hora = graphene.DateTime()
            numero_cliente = graphene.Int()
            numero_sucursal = graphene.Int()
            numeros_mesas = graphene.List(graphene.Int)
            cantidad_personas = graphene.Int()

        ok = graphene.Int()
        reserva = graphene.Field(ReservaNode)

        def mutate(self, info, numero, numero_sucursal,
                   cantidad_personas=None, fecha_hora=None,
                   numero_cliente=None, numeros_mesas=tuple()):

            user = info.context.user
            if ExpAuth.verificar_permiso('modificar_reserva',
                                         user.persona.rol):
                reserva = ExpReserva.get_reserva(numero)
                cliente = ExpCliente.get_cliente(numero_cliente)
                sucursal = ExpSucursal.get_sucursal(numero_sucursal)

                mesas = []
                for numero_mesa in numeros_mesas:
                    mesas.append(ExpMesa.get_mesa(sucursal, numero_mesa))

                ok, reserva = ExpReserva.modificar_reserva(
                    reserva, cantidad_personas, fecha_hora, cliente, mesas)
            else:
                ok = 401
                reserva = None

            return ReservaQL.ModificarReserva(ok=ok, reserva=reserva)

    class AnularReserva(Mutation):
        class Arguments:
            numero = graphene.Int(required=True)
            comentario = graphene.String()

        ok = graphene.Int()
        reserva = graphene.Field(ReservaNode)

        def mutate(self, info, numero, comentario):

            user = info.context.user
            if ExpAuth.verificar_permiso('anular_reserva',
                                         user.persona.rol):
                ok, reserva = ExpReserva.anular_reserva(
                    numero,
                    user,
                    comentario=comentario
                )
            else:
                ok = 401
                reserva = None

            return ReservaQL.AnularReserva(ok=ok, reserva=reserva)

    def ver_reservas(self, info, numero_sucursal, fecha_hora_desde,
                     fecha_hora_hasta, **kwargs):

        user = info.context.user
        if ExpAuth.verificar_permiso('ver_reservas',
                                     user.persona.rol):
            sucursal = ExpSucursal.get_sucursal(numero_sucursal)
            if sucursal is None:
                raise GraphQLError('404 - Sucursal no encontrada')

            return ExpReserva.ver_reservas(sucursal,
                                           fecha_hora_desde,
                                           fecha_hora_hasta)
        else:
            raise GraphQLError('401 - No está autorizado a ver reservas')

    def ver_reserva(self, info, numero_reserva, **kwargs):
        reserva = ExpReserva.get_reserva(numero_reserva)
        if reserva is None:
            raise GraphQLError('404 - La reserva no existe')
        else:
            return reserva
