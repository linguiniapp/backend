from datetime import datetime

from modelo.entidades import Cliente, EstadoCliente, Rol
from modelo.entidades import PuntosCliente, TransaccionPuntos
from modelo.persistencia import Persistencia


class ExpCliente:

    @staticmethod
    def get_cliente(numero_o_dni=None, numero=None, dni=None):
        if numero:
            cliente = Persistencia.get_session().query(Cliente)\
                .filter(Cliente.numero == numero).one_or_none()
        elif dni:
            cliente = Persistencia.get_session().query(Cliente)\
                .filter(Cliente.dni == dni).one_or_none()
        elif numero_o_dni:
            cliente = ExpCliente.get_cliente(numero=numero_o_dni)
            if cliente is None:
                cliente = ExpCliente.get_cliente(dni=numero_o_dni)
        else:
            cliente = None

        return cliente

    @staticmethod
    def ver_clientes(lista_estados):
        estados = []

        for estado in lista_estados:
            if estado == 'habilitado':
                estados.append(Persistencia.get_session().query(EstadoCliente)
                               .filter(EstadoCliente.nombre == 'habilitado'))
            elif estado == 'suspendido':
                estados.append(Persistencia.get_session().query(EstadoCliente)
                               .filter(EstadoCliente.nombre == 'suspendido'))
            elif estado == 'sinusuario':
                estados.append(Persistencia.get_session().query(EstadoCliente)
                               .filter(EstadoCliente.nombre == 'sin usuario'))

        clientes_query = Persistencia.get_session().query(Cliente)

        if estados:
            clientes = []
            for estado in estados:
                clientes.append(clientes_query.filter(Cliente.estado == estado)
                                .all())
        else:
            clientes = clientes_query.all()

        return clientes

    @staticmethod
    def crear_cliente_rapido(nombre, apellido, dni):
        cliente = Persistencia.get_session().query(Cliente)\
            .filter(Cliente.dni == dni).one_or_none()

        rol_cliente = Persistencia.query(Rol).filter(Rol.codigo == 1).one()

        if cliente is None:
            ultimo_cliente = Persistencia.get_session().query(Cliente) \
                .order_by(Cliente.numero.desc()).first()

            if ultimo_cliente is None:
                numero = 1
            else:
                numero = ultimo_cliente.numero + 1

            estado = Persistencia.get_session().query(EstadoCliente)\
                .filter(EstadoCliente.nombre == 'sin usuario').one()

            puntos = PuntosCliente(
                total=0,
                fecha_hora_actualizacion=datetime.now()
            )

            cliente = Cliente(numero=numero,
                              nombre=nombre,
                              apellido=apellido,
                              dni=dni,
                              email='tmp.' + str(dni) + '@linguiniapp.com',
                              estado=estado,
                              rol=rol_cliente,
                              tipo='cliente',
                              puntos=puntos)

            Persistencia.get_session().add(cliente)
        elif cliente.estado.nombre == 'suspendido':
            return False, cliente

        return True, cliente

    @staticmethod
    def crear_cliente(datos_cliente, commit=True):
        ultimo_cliente = Persistencia.get_session().query(Cliente)\
            .order_by(Cliente.numero.desc()).first()

        rol_cliente = Persistencia.query(Rol).filter(Rol.codigo == 1).one()

        if ultimo_cliente is None:
            numero = 1
        else:
            numero = ultimo_cliente.numero + 1

        estado = Persistencia.get_session().query(EstadoCliente)\
            .filter(EstadoCliente.nombre == 'habilitado').one()

        puntos = PuntosCliente(
            total=0,
            fecha_hora_actualizacion=datetime.now()
        )

        cliente = Cliente(nombre=datos_cliente.nombre,
                          apellido=datos_cliente.apellido,
                          email=datos_cliente.email,
                          fecha_nacimiento=datos_cliente.fecha_nacimiento,
                          dni=datos_cliente.dni,
                          numero=numero,
                          estado=estado,
                          rol=rol_cliente,
                          tipo='cliente',
                          puntos=puntos)

        if datos_cliente.sexo is not None:
            cliente.sexo = datos_cliente.sexo

        if datos_cliente.telefono is not None:
            cliente.telefono = datos_cliente.telefono

        if datos_cliente.direccion is not None:
            cliente.direccion = datos_cliente.direccion

        Persistencia.get_session().add(cliente)

        if commit:
            Persistencia.get_session().commit()

        return cliente

    @staticmethod
    def modificar_cliente(cliente, info_cliente):
        if info_cliente.nombre:
            cliente.nombre = info_cliente.nombre

        if info_cliente.apellido:
            cliente.apellido = info_cliente.apellido

        if info_cliente.sexo:
            cliente.sexo = info_cliente.sexo

        if info_cliente.email:
            cliente.email = info_cliente.email

        if info_cliente.telefono:
            cliente.telefono = info_cliente.telefono

        if info_cliente.fecha_nacimiento:
            cliente.fecha_nacimiento = info_cliente.fecha_nacimiento

        if info_cliente.dni:
            cliente.dni = info_cliente.dni

        if info_cliente.direccion:
            cliente.direccion = info_cliente.direccion

        return cliente, True

    @staticmethod
    def deshabilitar_cliente(cliente):
        estado_deshabilitado = Persistencia.query(EstadoCliente).filter(
            EstadoCliente.nombre == 'deshabilitado').one()

        if cliente.estado.nombre != 'suspendido':
            cliente.estado = estado_deshabilitado
            ok = True
        else:
            ok = False

        return cliente, ok

    @staticmethod
    def suspender_cliente(cliente):
        estado_suspendido = Persistencia.query(EstadoCliente).filter(
            EstadoCliente.nombre == 'suspendido').one()

        cliente.estado = estado_suspendido

        return cliente, True
