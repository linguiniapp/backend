import smtplib
from email.mime.text import MIMEText
from os import environ


class ExpMail:

    @staticmethod
    def send_pass_recovery(email, token):
        msg = MIMEText(
            'Para reestablecer su contraseña ingrese al siguiente link: '
            'https://www.linguiniapp.com/resetPassword/' + token
        )

        msg['Subject'] = 'LingüiniApp: Recuperación de contraseña'
        msg['From'] = 'recoverpassword@linguiniapp.com'
        msg['To'] = email

        mail_server = 'smtp.mailgun.org'
        server = smtplib.SMTP(mail_server)
        server.starttls()

        mail_password = environ.get('LINGUINI_APP_EMAIL_PASSWORD', 'password')
        server.login('postmaster@mg.linguiniapp.com', mail_password)

        server.sendmail(msg['From'], msg['To'], msg.as_string())

        server.close()

        return 0
