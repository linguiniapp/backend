"""
:Created on: 2018-10-08
:Version: 1
:Author: Andrés Bonilla
"""

from base64 import b64encode, b64decode
from datetime import datetime
from hashlib import sha1
from random import random

from exp_carta import ExpCarta
from exp_producto import ExpProducto
from modelo.entidades import CartaProducto
from modelo.entidades import Cliente
from modelo.entidades import EstadoPedido, PedidoEstado
from modelo.entidades import EstadoReserva
from modelo.entidades import Pedido, TipoPedido, DetallePedido
from modelo.entidades import Promocion, PedidoPromocion
from modelo.entidades import Recompensa, PedidoRecompensa
from modelo.entidades import Reserva, Estadia
from modelo.entidades import Sucursal
from modelo.persistencia import Persistencia


class ExpPedido:
    """Clase encargada de la gestión de pedidos.

    Esta clase realiza la creación, búsqueda, modificación y eliminación
    lógica de los pedidos. Agrega elementos recibidos del cliente al pedido
    o genera uno nuevo si no existe uno abierto. Cuando el cliente confirma
    el pedido, lo cierra y dispara el envío a la comandera.
    """

    @staticmethod
    def get_pedido(numero_pedido: int) -> Pedido:
        """Recupera un pedido por número de pedido.

        Recupera de la base de datos el pedido correspondiente al
        número recibido como parámetro.

        Args:
            numero_pedido: Número correspondiente al pedido deseado

        Returns:
            Un objeto Pedido con los datos del pedido recuperado
            de la base de datos.
        """
        return Persistencia.get_session().query(Pedido)\
            .filter(Pedido.numero == numero_pedido).one_or_none()

    @staticmethod
    def ver_pedidos(cliente: Cliente=None, monitor: bool=False,
                    sucursal: Sucursal=None, estados: [str]=tuple([])) \
            -> [Pedido]:
        """Recupera todos los pedidos o filtra por cliente activo o estado.

        Recupera de la base de datos todos los pedidos cargados,
        si recibe un cliente devuelve únicamente los pedidos
        que le pertenecen. Si recibe True en el parámetro monitor, devuelve
        sólo los pedidos que deban mostrarse en la cocina.

        Args:
            cliente: Cliente asociado al usuario activo.
                Enviado por el controlador.
            monitor: Booleano que indica si se deben pasar todos los
                pedidos o sólo los que se muestran en el monitor de cocina.
            sucursal: Sucursal para la que se buscan los pedidos.
            estados: Estados por los que se filtran los pedidos.

        Returns:
            Una lista de objetos Pedido, correspondiente a los pedidos
            recuperados y filtrados.
        """
        lista_pedidos = []

        if cliente:
            pedidos = Persistencia.get_session().query(Pedido).all()
            for pedido in pedidos:
                if ((pedido.reserva
                     and pedido.reserva.cliente == cliente)
                        or (pedido.estadia
                            and pedido.estadia.cliente == cliente)):
                    lista_pedidos.append(pedido)
        else:
            if monitor:
                estados_permitidos = [
                    Persistencia.query(EstadoPedido)
                        .filter(EstadoPedido.nombre == 'confirmado')
                        .one().oid,
                    Persistencia.query(EstadoPedido)
                        .filter(EstadoPedido.nombre == 'anulado')
                        .one().oid,
                    Persistencia.query(EstadoPedido)
                        .filter(EstadoPedido.nombre == 'en preparacion')
                        .one().oid,
                    Persistencia.query(EstadoPedido)
                        .filter(EstadoPedido.nombre == 'listo')
                        .one().oid
                ]

                lista_pedidos = []
                pedidos = Persistencia.query(Pedido).all()
                for pedido in pedidos:
                    estado_pedido = Persistencia.query(PedidoEstado)\
                        .filter(PedidoEstado.pedido == pedido)\
                        .order_by(PedidoEstado.fecha_hora.desc()).first()
                    if estado_pedido.estado_oid in estados_permitidos:
                        lista_pedidos.append(pedido)
            else:
                lista_pedidos = Persistencia.get_session().query(Pedido).all()

        filtro_estados = []
        for nombre_estado in estados:
            estado = (Persistencia.query(EstadoPedido)
                      .filter(EstadoPedido.nombre == nombre_estado)
                      .one_or_none())
            if estado:
                filtro_estados.append(estado.oid)
        filtro_estados = list(set(filtro_estados))

        for pedido in lista_pedidos:
            if filtro_estados:
                estado_actual = ExpPedido.ver_estado_actual_pedido(pedido)
                if estado_actual not in filtro_estados:
                    lista_pedidos.remove(pedido)
            if sucursal is not None:
                if pedido.estadia and pedido.estadia.sucursal != sucursal:
                    lista_pedidos.remove(pedido)
                elif pedido.reserva and pedido.reserva.sucursal != sucursal:
                    lista_pedidos.remove(pedido)

        return lista_pedidos

    @staticmethod
    def ver_pedido_reserva(reserva: Reserva)\
            -> (Pedido, bool):
        """Obtiene el pedido asociado a una reserva.

        Recupera de la base de datos el pedido, en estado 'solicitado',
        asociado a una reserva en estado 'solicitada'.

        Args:
            reserva: Reserva sobre la que se busca el pedido, debe encontrarse
                en estado 'solicitada'.

        Returns:
            Un objeto Pedido, correspondiente al pedido abierto,
            asociado a la reserva recibida.
        """
        estado_solicitada = Persistencia.get_session().query(EstadoReserva)\
            .filter(EstadoReserva.nombre == 'solicitada').one()

        for reserva_estado in reserva.historico_estados:
            if reserva_estado.estado != estado_solicitada:
                return None

        estado_solicitado = Persistencia.get_session().query(EstadoPedido)\
            .filter(EstadoPedido.nombre == 'solicitado').one()

        for pedido in reserva.pedidos:
            solicitado = True
            for pedido_estado in pedido.historico_estados:
                if pedido_estado.estado != estado_solicitado:
                    solicitado = False
                    break
            if solicitado:
                return pedido

        return None

    @staticmethod
    def get_pedido_abierto_estadia(estadia: Estadia)\
            -> (Pedido or None, bool):
        """Obtiene el pedido abierto actualmente para la estadía.

        Busca un pedido con estado 'solicitado' relacionado a la estadía.

        Args:
            estadia: Estadia activo para la que se busca el pedido.

        Returns:
            Un objeto Pedido que corresponde al último pedido en estado
                'solicitado', relacionado a una estadía.
                Si no existe devuelve None.
        """
        pedidos_estadia = Persistencia.get_session().query(Pedido)\
            .filter(Pedido.estadia == estadia).all()

        for pedido in pedidos_estadia:
            abierto = True
            for elem_hist in pedido.historico_estados:
                if elem_hist.estado.nombre != 'solicitado':
                    abierto = False
                    break
            if abierto:
                return pedido

        return None

    @staticmethod
    def ver_estado_actual_pedido(pedido: Pedido) -> PedidoEstado:
        """Obtiene el estado actual de un pedido

        Busca el estado más reciente asociado al pedido indicado.

        Args:
            pedido: Pedido para el que se busca el estado actual.

        Returns:
            Un objeto PedidoEstado asociado al pedido y a un estado
            (clase EstadoPedido) que contiene la fecha y hora a la que
            se realizó la asignación.
        """

        return Persistencia.query(PedidoEstado)\
            .filter(PedidoEstado.pedido == pedido)\
            .order_by(PedidoEstado.fecha_hora.desc()).first()

    @staticmethod
    def crear_pedido(
            tipo: str,
            estadia_o_reserva: Estadia or Reserva,
            lista_detalles: [(CartaProducto, int, str)]=tuple([]),
            comentarios=None,
            lista_promociones: [(Promocion, int)]=tuple([]),
            lista_recompensas: [(Recompensa, int)]=tuple([])
    ) -> (Pedido, bool):
        """Crea un nuevo pedido para el cliente indicado.

        Crea un nuevo pedido para el cliente y lo asocia a estadía o reserva
        según corresponda. Luego genera los detalles y los asocia al pedido
        creado.

        Args:
            tipo: Tipo de pedido según quién y cuándo lo realice, puede ser
                remoto o presencial, en el último caso por el cliente o mozo.
            estadia_o_reserva: Indica la estadía o reserva a la que se debe
                asociar el pedido en caso de pedidos presenciales.
            lista_detalles: Lista de tuplas que representan los detalles del
                pedido, cada tupla tiene el formato (producto, cantidad,
                comentario).
            comentarios: Comentarios sobre el pedido para la cocina.
            lista_promociones: Lista de tuplas que representan las promociones
                incluidas en el pedido, cada tupla tiene el formato (promocion,
                cantidad).
            lista_recompensas: Lista de tuplas que representan las recompensas
                incluidas en el pedido, cada tupla tiene el formato
                (recompensa, cantidad).

        Returns:
            Una tupla con un nuevo objeto Pedido que contiene los detalles
            asociados a él, junto con la referencia a la estadía o reserva
            indicada. Como segunda componente devuelve un booleano para
            indicar éxito o fracaso del método.
        """

        tipo_pedido = Persistencia.get_session().query(TipoPedido)\
            .filter(TipoPedido.nombre == tipo).one()

        ultimo = Persistencia.get_session().query(Pedido)\
            .order_by(Pedido.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        nuevo_pedido = Pedido(
            fecha_hora=datetime.now(),
            numero=numero,
            tipo=tipo_pedido
        )

        if comentarios is not None:
            nuevo_pedido.comentario = comentarios

        if tipo == 'remoto':
            nuevo_pedido.reserva = estadia_o_reserva
        elif tipo == 'presencial cliente' or tipo == 'presencial mozo':
            nuevo_pedido.estadia = estadia_o_reserva
        else:
            return None, False

        productos_vistos = []
        for producto, cantidad, comentario in lista_detalles:
            if producto in productos_vistos:
                continue
            else:
                productos_vistos.append(producto)
            det = DetallePedido(carta_producto=producto,
                                cantidad=cantidad,
                                pedido=nuevo_pedido)
            if comentario:
                det.comentarios = comentario
            Persistencia.get_session().add(det)

        promociones_vistas = []
        for prom, cant in lista_promociones:
            if prom in promociones_vistas:
                continue
            else:
                promociones_vistas.append(prom)
            det = PedidoPromocion(
                promocion=prom,
                cantidad=cant,
                pedido=nuevo_pedido
            )
            Persistencia.add(det)

        recompensas_vistas = []
        for rec, cant in lista_recompensas:
            if rec in recompensas_vistas:
                continue
            else:
                recompensas_vistas.append(rec)
            det = PedidoRecompensa(
                recompensa=rec,
                cantidad=cant,
                pedido=nuevo_pedido
            )
            Persistencia.add(det)

        estado_solicitado = Persistencia.query(EstadoPedido)\
            .filter(EstadoPedido.nombre == 'solicitado').one()

        pedido_estado = PedidoEstado(
            fecha_hora=datetime.now(),
            estado=estado_solicitado,
            pedido=nuevo_pedido
        )

        Persistencia.add(nuevo_pedido)
        Persistencia.add(pedido_estado)

        if Persistencia.commit():
            return nuevo_pedido, True
        else:
            return None, False

    @staticmethod
    def modificar_pedido(
            pedido: Pedido,
            detalles: [(CartaProducto, int, str)]=tuple([]),
            comentarios: str=None,
            promociones: [(Promocion, int)]=tuple([]),
            recompensas: [(Recompensa, int)]=tuple([])
    ) -> (Pedido, bool):
        """Modifica un pedido para agregar, modificar o quitar detalles.

        Modifica un pedido existente, que aún no esté en preparación,
        para agregar nuevos detalles o modificar detalles existentes.
        Una cantidad 0 en el detalle lo remueve del pedido.

        Args:
            pedido: Referencia al objeto Pedido que se desea modificar.
            detalles: Lista de detalles para agregar o modificar. Cada
                detalle es una tupla con formato (producto, cantidad,
                comentario).
            comentarios: Comentarios sobre el pedido para la cocina.
            promociones: Lista de detalles con las promociones para agregar o
                modificar. Cada detalle es una tupla con formato (Promocion,
                cantidad).
            recompensas: Lista de detalles con las recompensas para agregar o
                modificar. Cada detalle es una tupla con formato (Recompensa,
                cantidad).

        Returns:
            Una tupla con el objeto Pedido modificado, con sus detalles y su
            referencia a estadía o reserva. Junto al objeto retorna un
            booleano para indicar éxito o fracaso del método.
        """

        if comentarios is not None:
            pedido.comentario = comentarios

        productos_vistos = []
        for producto, cantidad, comentario in detalles:
            if producto in productos_vistos:
                continue
            else:
                productos_vistos.append(producto)
            reemplazar_producto = False
            for det in pedido.detalles:
                if producto == det.carta_producto:
                    if comentario is not None:
                        det.comentarios = comentario
                    if cantidad is None:
                        pass
                    elif cantidad > 0:
                        det.cantidad = cantidad
                    else:
                        Persistencia.get_session().delete(det)
                    reemplazar_producto = True
                    break
            if not reemplazar_producto:
                nuevo_detalle = DetallePedido(
                    carta_producto=producto,
                    cantidad=cantidad,
                    pedido=pedido
                )
                if comentario is not None:
                    nuevo_detalle.comentarios = comentario
                Persistencia.get_session().add(nuevo_detalle)

        promociones_vistas = []
        for prom, cant in promociones:
            if prom in promociones_vistas:
                continue
            else:
                promociones_vistas.append(prom)
            reemplazar = False
            for det in pedido.promociones:
                if prom == det.promocion:
                    if cant is None:
                        pass
                    elif cant > 0:
                        det.cantidad = cant
                    else:
                        Persistencia.delete(det)
                    reemplazar = True
                    break
            if not reemplazar:
                det = PedidoPromocion(
                    promocion=prom,
                    cantidad=cant,
                    pedido=pedido
                )
                Persistencia.add(det)

        recompensas_vistas = []
        for rec, cant in recompensas:
            if rec in recompensas_vistas:
                continue
            else:
                recompensas_vistas.append(rec)
            reemplazar = False
            for det in pedido.recompensas:
                if rec == det.recompensa:
                    if cant is None:
                        pass
                    elif cant > 0:
                        det.cantidad = cant
                    else:
                        Persistencia.delete(det)
                    reemplazar = True
                    break
            if not reemplazar:
                det = PedidoRecompensa(
                    recompensa=rec,
                    cantidad=cant,
                    pedido=pedido
                )
                Persistencia.add(det)

        Persistencia.get_session().commit()

        return pedido, True

    @staticmethod
    def anular_pedido(pedido: Pedido) -> (Pedido, int):
        """Anula un pedido para que no sea realizado o entregado.

        Cambia el estado de un pedido que aún no esté en preparación
        para que no se elabore o entregue.

        Args:
            pedido: Pedido que se desea anular.

        Returns:
            Una tupla formada por el pedido y un entero para indicar código
            de error en caso de fracaso o 0 si se completó con éxito.
        """
        estado_anulado = Persistencia.query(EstadoPedido)\
            .filter(EstadoPedido.nombre == 'anulado').one()

        pedido_estado = PedidoEstado(
            fecha_hora=datetime.now(),
            pedido=pedido,
            estado=estado_anulado
        )
        Persistencia.add(pedido_estado)

        if Persistencia.commit():
            return pedido, 0
        else:
            return pedido, 500

    @staticmethod
    def confirmar_pedido(pedido: Pedido) -> (Pedido, int):
        """Confirma un pedido para que se muestre en monitor de cocina.

        Cambia el estado de un pedido recibido por parámetro, en estado
        'solicitado', a estado 'confirmado' para que sea mostrado en el
        montior de cocina y se habilite su avance de estado. Opcionalmente
        se imprime la comanda con código QR.

        Args:
            pedido: Pedido que se desea confirmar.

        Returns:
            Una tupla formada por el pedido recibido y un entero para
            indicar código de error en caso de fracaso o 0 en caso de éxito.
        """
        estado_confirmado = Persistencia.query(EstadoPedido) \
            .filter(EstadoPedido.nombre == 'confirmado').one()

        pedido_estado = PedidoEstado(
            fecha_hora=datetime.now(),
            pedido=pedido,
            estado=estado_confirmado
        )
        Persistencia.add(pedido_estado)

        if Persistencia.commit():
            return pedido, 0
        else:
            return pedido, 500

    @staticmethod
    def get_pedido_qr(pedido: Pedido) -> str:
        """Genera una cadena de texto para representar en el código QR.

        Genera una cadena de texto formada por el número de pedido y un
        hash, codificada en base64, utilizada para generar un código QR
        que identifica al pedido y se utiliza para los cambios de estado
        en la cocina.

        Args:
            pedido: Pedido para el que se desea generar el código.

        Returns:
            Una cadena de texto que representa al pedido con un mínimo
            de seguridad, para imprimir como QR.
        """
        rnd_int = str(int(random()*10000000)).zfill(7)
        pedido.rnd_int = rnd_int

        token_string = (
            str(pedido.numero)
            + pedido.fecha_hora.strftime("%Y%m%d%H%M%S%f%w")
            + rnd_int
        )
        token = sha1(token_string.encode()).hexdigest()[:7]

        return (b64encode('.'.join([str(pedido.numero).zfill(4), token])
                          .encode()).decode())

    @staticmethod
    def get_pedido_from_qr(qr: str) -> Pedido or None:
        """Recupera un pedido a partir de la cadena de texto de un código QR.

        Lee una cadena de texto obtenida de la lectura de un código QR,
        recupera el pedido asociado al código, verifica el hash y retorna
        el objeto Pedido referenciado.

        Args:
            qr: Cadena de texto obtenida de la lectura del código QR.

        Returns:
            El objeto Pedido referenciado por el código o None si no se
            encontró o no se pudo verificar el hash.
        """
        numero_pedido, token = b64decode(qr).decode().split('.')
        pedido = ExpPedido.get_pedido(numero_pedido)

        token_string = (
                str(pedido.numero)
                + pedido.fecha_hora.strftime("%Y%m%d%H%M%S%f%w")
                + pedido.rnd_int
        )
        if token == sha1(token_string.encode()).hexdigest()[:7]:
            return pedido
        else:
            return None

    @staticmethod
    def avanzar_estado(pedido: Pedido) -> (Pedido, int):
        """Avanza un pedido a su próximo estado en la cadena de producción.

        Verifica que el pedido recibido como parámetro no esté anulado y
        cambia el estado al siguiente en la cadena de producción de cocina:
        solicitado -> en preparación -> listo.

        Args:
            pedido: Pedido al que se desea avanzar el estado.

        Returns:
            Una tupla formada por el pedido recibido y un código de error
            en caso de fracaso, en caso de éxito el código es 0.
            Si el estado no está entre los esperados retorna código 400 y no
            se debe reintentar el cambio de estado.
            Si el estado es anulado retorna código 410 y no
            se debe reintentar el cambio de estado.
            Si no se pudo persistir el cambio de estado retorna código 500
            y se debe reintentar la lectura del código un tiempo después.
        """
        pedido_confirmado = Persistencia.query(EstadoPedido)\
            .filter(EstadoPedido.nombre == 'confirmado').one()
        pedido_anulado = Persistencia.query(EstadoPedido)\
            .filter(EstadoPedido.nombre == 'anulado').one()
        pedido_enpreparacion = Persistencia.query(EstadoPedido)\
            .filter(EstadoPedido.nombre == 'en preparacion').one()
        pedido_listo = Persistencia.query(EstadoPedido)\
            .filter(EstadoPedido.nombre == 'listo').one()

        pedido_estado_actual = Persistencia.query(PedidoEstado)\
            .filter(PedidoEstado.pedido == pedido)\
            .order_by(PedidoEstado.fecha_hora.desc()).first()

        if pedido_estado_actual.estado == pedido_anulado:
            return pedido, 410
        elif pedido_estado_actual.estado == pedido_confirmado:
            nuevo_estado = pedido_enpreparacion
        elif pedido_estado_actual.estado == pedido_enpreparacion:
            nuevo_estado = pedido_listo
        else:
            return pedido, 400

        pedido_estado = PedidoEstado(
            fecha_hora=datetime.now(),
            pedido=pedido,
            estado=nuevo_estado
        )
        Persistencia.add(pedido_estado)

        if Persistencia.commit():
            return pedido, 0
        else:
            return pedido, 500

    @staticmethod
    def ver_pedidos_impagos(estadia: Estadia,
                            incluir_devueltos: bool=False) -> [Pedido]:
        """Recupera los pedidos pendientes de pago de la estadía

        Busca todos los pedidos asociados a la estadía con estado
        diferente a 'pagado', 'anulado' y opcionalmente 'devuelto'.

        Args:
            estadia: Estadia para la cual se buscan pedidos.
            incluir_devueltos: Bandera que indica si se cobran los pedidos
                devueltos.

        Returns:
            Una lista de objetos Pedido que se encuentran pendientes de pago.
        """
        filtro_estados = [
            Persistencia.query(EstadoPedido)
                        .filter(EstadoPedido.nombre == 'pagado').one(),
            Persistencia.query(EstadoPedido)
                        .filter(EstadoPedido.nombre == 'anulado').one()
        ]

        if not incluir_devueltos:
            filtro_estados.append(
                Persistencia.query(EstadoPedido)
                            .filter(EstadoPedido.nombre == 'devuelto').one()
            )

        pedidos_estadia = Persistencia.query(Pedido)\
            .filter(Pedido.estadia == estadia).all()

        pedidos = []
        for pedido in pedidos_estadia:
            estado = Persistencia.query(PedidoEstado)\
                .filter(PedidoEstado.pedido == pedido)\
                .order_by(PedidoEstado.fecha_hora.desc()).first()
            if estado not in filtro_estados:
                pedidos.append(pedido)

        return pedidos

    @staticmethod
    def promrec_a_productos(sucursal, promocion_o_recompensa):
        productos = []
        for item in promocion_o_recompensa.detalles:
            producto = ExpProducto.get_producto(item.producto.numero)
            carta = ExpCarta.ver_carta(sucursal)
            carta_prod = ExpCarta.ver_producto_carta(carta, producto)
            if carta_prod is None:
                continue
            else:
                productos.append((
                    carta_prod,
                    item.cantidad
                ))
