from threading import Thread
from time import sleep

from flask import Flask
from flask_cors import CORS
from flask_graphql import GraphQLView

from auth.auth_view import auth_middleware
from auth.crypto import ExpCrypto
from auth.exp_auth import ExpAuth
from modelo.db_init import db_check, db_reset, db_load
from modelo.persistencia import Persistencia
from schema.schema import schema

app = Flask(__name__)
CORS(app)


app.add_url_rule(
    '/graphiql',
    view_func=GraphQLView.as_view(
        'graphiql',
        schema=schema,
        graphiql=True,
        middleware=[auth_middleware]
    )
)


app.add_url_rule(
    '/graphql',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=False,
        middleware=[auth_middleware]
    )
)


def init_app(init=True):
    while db_check(init):
        print('Esperando base de datos...')
        sleep(3)

    if not db_load():
        return init_app(False)

    if not ExpCrypto.auth_init():
        db_reset()
        init_app(False)
    else:
        ExpAuth.create_default_users()


@app.teardown_appcontext
def shutdown_session(exception=None):
    Persistencia.get_session().remove()


@app.route('/reset')
def reset_db():
    db_reset()
    t = Thread(target=init_app, args=(False,))
    t.start()
    return "Listo"


@app.route('/reset_test')
def reset_db_with_test_data():
    t = db_reset(True)
    t.join()
    ExpAuth.create_default_users()
    from data_load import test_data
    test_data()

    return "Listo"


def on_startup():
    Persistencia.init()
    t = Thread(target=init_app)
    t.start()
    return "Iniciando..."


def before_request_check():
    if db_check():
        return 'La base de datos no está lista'
    else:
        return None


app.before_first_request(on_startup)

app.before_request(before_request_check)
    # lambda: "La base de datos no está lista" if db_check() else None)


if __name__ == '__main__':
    app.run(debug=True)
