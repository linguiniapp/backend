from decimal import Decimal

from modelo.persistencia import Persistencia

from modelo.entidades import Caja
from modelo.entidades import PuntuacionSucursal
from modelo.entidades import Factura
from modelo.entidades import Estadia, EstadoEstadia
from modelo.entidades import Pedido
from schema.nodes import ReporteVentaNodo
from schema.nodes import ReporteMozoNode
from schema.nodes import ReporteProductoNode


class ExpReporte:

    @staticmethod
    def obtener_reporte_caja(sucursal, fecha_desde, fecha_hasta):
        return (
            Persistencia.query(Caja)
            .filter(
                Caja.sucursal == sucursal,
                Caja.fecha_hora_apertura >= fecha_desde,
                Caja.fecha_hora_cierre <= fecha_hasta
            ).all()
        )

    @staticmethod
    def obtener_reporte_mozos(sucursal, fecha_desde, fecha_hasta):

        estado_cerrada = Persistencia.query(EstadoEstadia).filter(
            EstadoEstadia.nombre == 'cerrada'
        ).one()

        estadias = Persistencia.query(Estadia).filter(
            Estadia.fecha_hora_llegada >= fecha_desde,
            Estadia.fecha_hora_llegada <= fecha_hasta,
            Estadia.estado == estado_cerrada,
            Estadia.sucursal == sucursal
        ).all()

        mozos = {}
        for estadia in estadias:
            if estadia.mozo is None:
                continue
            cant_pedidos_estadia = 0
            cant_productos_estadia = 0
            monto_estadia = Decimal('0.00')
            for pedido in estadia.pedidos:
                cant_pedidos_estadia += 1
                for det in pedido.detalles:
                    cant_productos_estadia += 1
                    monto_estadia += det.carta_producto.precio * det.cantidad
                for prom in pedido.promociones:
                    monto_estadia += prom.precio
                    for _ in prom.detalles:
                        cant_productos_estadia += 1
            if estadia.mozo.numero not in mozos.keys():
                mozos[estadia.mozo.numero] = ReporteMozoNode(
                    empleado=estadia.mozo,
                    pedidos=estadia.pedidos,
                    cantidad_pedidos=cant_pedidos_estadia,
                    cantidad_productos=cant_productos_estadia,
                    monto_productos=monto_estadia
                )
            else:
                mozo = mozos[estadia.mozo.numero]
                mozo.pedidos += estadia.pedidos
                mozo.cantidad_pedidos += cant_pedidos_estadia
                mozo.cantidad_productos += cant_productos_estadia
                mozo.monto_productos += monto_estadia

        return list(mozos.values())

    @staticmethod
    def obtener_reporte_calificaciones(sucursal, fecha_desde, fecha_hasta):
        califs = Persistencia.query(PuntuacionSucursal).filter(
            PuntuacionSucursal.sucursal == sucursal,
            PuntuacionSucursal.fecha_hora >= fecha_desde,
            PuntuacionSucursal.fecha_hora <= fecha_hasta,
        ).all()

        total = 0
        cantidad = 0
        det = [0, 0, 0, 0, 0]
        for c in califs:
            total += c.calificacion
            cantidad += 1
            det[c.calificacion-1] += 1

        if cantidad:
            prom = total / cantidad
        else:
            prom = 0

        return califs, prom, det

    @staticmethod
    def obtener_reporte_ventas(fecha_desde, fecha_hasta, sucursal):
        facturas = Persistencia.query(Factura).filter(
            Factura.fecha >= fecha_desde,
            Factura.fecha <= fecha_hasta
        ).all()

        ventas = []
        for f in facturas:
            sucursal = f.pedidos[0].estadia.sucursal
            ventas.append(ReporteVentaNodo(
                fecha_factura=f.fecha,
                numero_factura=f.numero,
                numero_sucursal=sucursal.numero,
                nombre_sucursal=sucursal.nombre,
                monto_facturado=f.total
            ))

        if sucursal:
            for v in ventas:
                if v.numero_sucursal != sucursal.numero:
                    ventas.remove(v)

        return ventas

    @staticmethod
    def obtener_reporte_productos(fecha_desde, fecha_hasta):
        pedidos = Persistencia.query(Pedido).filter(
            Pedido.fecha_hora >= fecha_desde,
            Pedido.fecha_hora <= fecha_hasta
        ).all()

        productos = {}
        for pedido in pedidos:
            for detalle in pedido.detalles:
                prod_id = 'prod_' + str(detalle.carta_producto.oid)
                if prod_id in productos.keys():
                    prod = productos[prod_id]
                    prod.cantidad_vendida += detalle.cantidad
                    prod.monto_ventas += (detalle.cantidad
                                          * detalle.carta_producto.precio)
                else:
                    productos[prod_id] = (
                        ReporteProductoNode(
                            codigo=detalle.carta_producto.producto.codigo,
                            nombre=detalle.carta_producto.producto.nombre,
                            precio_unitario=detalle.carta_producto.precio,
                            cantidad_vendida=detalle.cantidad,
                            monto_ventas=(detalle.cantidad
                                          * detalle.carta_producto.precio)
                        )
                    )
            for prom in pedido.promociones:
                prom_id = 'prom_' + str(prom.promocion.oid)
                if prom_id in productos.keys():
                    prod = productos[prom_id]
                    prod.cantidad_vendida += prom.cantidad
                    prod.monto_ventas += (prom.cantidad
                                          * prom.promocion.precio)
                else:
                    productos[prom_id] = (
                        ReporteProductoNode(
                            codigo=prom.promocion.numero,
                            nombre=prom.promocion.nombre,
                            precio_unitario=prom.promocion.precio,
                            cantidad_vendida=prom.cantidad,
                            monto_ventas=(prom.cantidad
                                          * prom.promocion.precio)
                        )
                    )

        return list(productos.values())
