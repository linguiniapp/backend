from datetime import datetime

from modelo.entidades import ListaEspera, ListaEsperaDetalle
from modelo.persistencia import Persistencia


class ExpListaEspera:

    @staticmethod
    def get_lista_espera_actual(sucursal):
        return Persistencia.get_session().query(ListaEspera)\
            .filter(ListaEspera.sucursal == sucursal).one_or_none()

    @staticmethod
    def abrir_lista_espera(sucursal):
        if sucursal is None:
            return None

        lista = ExpListaEspera.get_lista_espera_actual(sucursal)

        if lista is None:
            lista = ListaEspera(fecha_hora_apertura=datetime.now(),
                                sucursal=sucursal)
        elif lista.fecha_hora_cierre is None:
            return lista
        else:
            lista.fecha_hora_apertura = datetime.now()
            lista.fecha_hora_cierre = None

        Persistencia.get_session().add(lista)
        Persistencia.get_session().commit()

        return lista

    @staticmethod
    def agregar_lista_espera(lista, nombre, cantidad_personas):
        detalle = Persistencia.get_session().query(ListaEsperaDetalle)\
            .filter(ListaEsperaDetalle.lista_espera == lista)\
            .order_by(ListaEsperaDetalle.numero.desc()).first()

        if detalle is None:
            numero = 1
        else:
            numero = detalle.numero + 1

        detalle = ListaEsperaDetalle(nombre=nombre,
                                     numero=numero,
                                     cantidad_personas=cantidad_personas,
                                     fecha_hora_llegada=datetime.now(),
                                     lista_espera=lista)
        Persistencia.get_session().add(detalle)
        Persistencia.get_session().commit()
        return detalle

    @staticmethod
    def eliminar_lista_espera(sucursal, numero_lista):
        lista = ExpListaEspera.get_lista_espera_actual(sucursal)
        rows = Persistencia.get_session().query(ListaEsperaDetalle)\
            .filter(ListaEsperaDetalle.lista_espera == lista, ListaEsperaDetalle.numero == numero_lista).delete()
        if rows == 1:
            Persistencia.get_session().commit()
            return True
        else:
            Persistencia.get_session().rollback()
            return False

    @staticmethod
    def cerrar_lista_espera(sucursal):
        if sucursal is None:
            return None

        lista = ExpListaEspera.get_lista_espera_actual(sucursal)
        if lista is None:
            return None

        if lista.fecha_hora_cierre is None:
            lista.fecha_hora_cierre = datetime.now()

        Persistencia.get_session().add(lista)
        Persistencia.get_session().commit()

        return lista
