from datetime import datetime

from jwt.exceptions import DecodeError, ExpiredSignature

from auth.crypto import ExpCrypto
from auth.exp_auth import ExpAuth


def valid_header(header):
    token = ExpCrypto.decode_token(header)
    if token is not None and token.get('exp') >= datetime.utcnow().timestamp():
        return True
    else:
        return False


def get_user(header):
    return ExpCrypto.decode_token(header).get('sub')


def auth_middleware(next, root, info, **args):
    try:
        auth_type, auth_token = info.context.headers\
            .get('Authorization').split()
        if auth_type == 'JWT' and valid_header(auth_token):
            username = get_user(auth_token)
            info.context.user = ExpAuth.get_usuario(username)
        else:
            info.context.user = ExpAuth.get_usuario_anonimo()
    except (AttributeError, ValueError, DecodeError):
        info.context.user = ExpAuth.get_usuario_anonimo()
    except ExpiredSignature:
        info.context.user = ExpAuth.get_usuario_anonimo()
    except Exception as e:
        print(info.context.headers.get('Authorization'))
        print(type(e))
        print(e)
        info.context.user = ExpAuth.get_usuario_anonimo()
    return next(root, info, **args)
