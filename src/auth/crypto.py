from base64 import urlsafe_b64encode
from datetime import datetime, timedelta
from os import environ
from random import SystemRandom

import jwt
from argon2 import Argon2Type, argon2_hash

from modelo.metadata import CryptoSettings
from modelo.persistencia import Persistencia


class ExpCrypto:

    # crypto = None

    argon_t = 0
    argon_m = 0
    argon_p = 0

    tipo = Argon2Type.Argon2_i

    jwt_server_secret = urlsafe_b64encode(
        str(environ.get('LINGUINI_APP_JWT_SECRET')).encode())
    jwt_algorithm = 'HS256'

    @staticmethod
    def auth_init():
        crypto = None
        try:
            crypto = Persistencia.get_session().query(CryptoSettings)\
                .order_by(CryptoSettings.fecha_hora.desc())\
                .first()
        finally:
            if crypto is None:
                Persistencia.get_session().close()
                return False

        ExpCrypto.argon_t = crypto.argon2_t
        ExpCrypto.argon_m = crypto.argon2_m
        ExpCrypto.argon_p = crypto.argon2_p

        Persistencia.get_session().close()
        return True

    @staticmethod
    def get_random_hex_bytes(rnd_bytes=32):
        return hex(SystemRandom().getrandbits(
            rnd_bytes * 8))[2:].zfill(rnd_bytes * 2)

    @staticmethod
    def hash_password(password, salt, params=None):
        if params is None:
            t = ExpCrypto.argon_t
            m = ExpCrypto.argon_m
            p = ExpCrypto.argon_p
        else:
            t, m, p = [int(a) for a in params.split(',')]

        return urlsafe_b64encode(argon2_hash(
            password,
            salt,
            t, m, p,
            argon_type=ExpCrypto.tipo)).decode()

    @staticmethod
    def get_pass_params():
        return ",".join(
            [str(p) for p in
                [ExpCrypto.argon_t, ExpCrypto.argon_m, ExpCrypto.argon_p]])

    @staticmethod
    def generar_token(sujeto, tipo):
        token_data = {
            'sub': sujeto,
            'exp': datetime.utcnow() + timedelta(days=1),
            'type': tipo
        }

        return jwt.encode(token_data, ExpCrypto.jwt_server_secret,
                          algorithm=ExpCrypto.jwt_algorithm).decode()

    @staticmethod
    def decode_token(token):
        return jwt.decode(token, ExpCrypto.jwt_server_secret)
