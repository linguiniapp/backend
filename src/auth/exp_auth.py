from auth.crypto import ExpCrypto
from exp_mail import ExpMail
from modelo.entidades import Accion, Rol
from modelo.entidades import Admin, Persona
from modelo.entidades import Usuario, TipoUsuario, EstadoUsuario
from modelo.persistencia import Persistencia


class ExpAuth:

    @staticmethod
    def get_usuario_anonimo():
        tipo_anonimo = Persistencia.query(TipoUsuario)\
            .filter(TipoUsuario.nombre == 'anonimo').one()

        anon = Usuario(
            nombre='Anonimo',
            tipo_usuario=tipo_anonimo
        )

        return anon

    @staticmethod
    def get_permisos(rol):
        return rol.acciones

    @staticmethod
    def verificar_permiso(permiso, rol):
        for accion in rol.acciones:
            if permiso == accion.nombre:
                return True
        return False

    @staticmethod
    def get_accion(codigo):
        return Persistencia.query(Accion)\
            .filter(Accion.codigo == codigo).one_or_none()

    @staticmethod
    def get_accion_por_nombre(nombre):
        return Persistencia.query(Accion) \
            .filter(Accion.nombre == nombre).one_or_none()

    @staticmethod
    def get_acciones():
        return Persistencia.query(Accion).all()

    @staticmethod
    def get_rol(codigo):
        return Persistencia.query(Rol)\
            .filter(Rol.codigo == codigo).one_or_none()

    @staticmethod
    def get_roles():
        return Persistencia.query(Rol).all()

    @staticmethod
    def crear_rol(nombre, acciones):
        ultimo = Persistencia.query(Rol).order_by(Rol.codigo.desc()).first()

        if ultimo is None:
            codigo = 1
        else:
            codigo = ultimo.codigo + 1

        rol = Rol(codigo=codigo,
                  nombre=nombre)

        for a in acciones:
            rol.acciones.append(a)

        Persistencia.add(rol)
        if Persistencia.commit():
            return rol, 0
        else:
            return None, 500

    @staticmethod
    def modificar_rol(rol, nombre, agregar_acciones, quitar_acciones):

        if nombre is not None:
            rol.nombre = nombre

        for a in agregar_acciones:
            if a not in rol.acciones:
                rol.acciones.append(a)

        for a in quitar_acciones:
            try:
                rol.acciones.remove(a)
            except ValueError:
                continue

        Persistencia.add(rol)
        if Persistencia.commit():
            return rol, 0
        else:
            return None, 500

    @staticmethod
    def eliminar_rol(rol):
        Persistencia.delete(rol)

        if Persistencia.commit():
            return 0
        else:
            return 500

    @staticmethod
    def asignar_rol(rol, empleado):
        empleado.rol = rol
        Persistencia.add(empleado)
        if Persistencia.commit():
            return 0
        else:
            return 500

    @staticmethod
    def quitar_rol(empleado):
        empleado.rol = None
        Persistencia.add(empleado)
        if Persistencia.commit():
            return 0
        else:
            return 500

    @staticmethod
    def create_default_users():
        tipo_admin = Persistencia.get_session().query(TipoUsuario)\
            .filter(TipoUsuario.nombre == 'admin').one()

        admin = Persistencia.get_session().query(Usuario)\
            .filter(Usuario.tipo_usuario == tipo_admin).all()
        if len(admin) != 0:
            Persistencia.get_session().close()
            return

        print("Creando usuario administrador por defecto")
        print("Usuario: admin")
        print("Contraseña: admin")

        pass_salt = ExpCrypto.get_random_hex_bytes()
        pass_hash = ExpCrypto.hash_password('admin', pass_salt)
        pass_params = ExpCrypto.get_pass_params()

        estado_habilitado = Persistencia.get_session().query(EstadoUsuario)\
            .filter(EstadoUsuario.nombre == 'habilitado').one()

        rol_admin = Persistencia.query(Rol).filter(Rol.codigo == 0).one()

        persona_admin = Admin(
            nombre='admin',
            apellido='admin',
            email='admin@linguiniapp.com',
            admin=True,
            tipo='admin',
            rol=rol_admin
        )

        # empleado_admin = Persona(
        #     nombre='admin',
        #     apellido='admin',
        #     email='admin@linguiniapp.com',
        #     cuil='00-00000000-1',
        #     numero=0
        # )
        #
        # empleado_habilitado = Persistencia.query(EstadoEmpleado)\
        #     .filter(EstadoEmpleado.nombre == 'habilitado').one()
        #
        # empleado_rol = EmpleadoEstado(
        #     fecha_hora=datetime.now(),
        #     descripcion='admin',
        #     estado=empleado_habilitado,
        #     empleado=empleado_admin
        # )

        admin = Usuario(
            nombre='admin',
            passHash=pass_hash,
            passSalt=pass_salt,
            passParams=pass_params,
            tipo_usuario=tipo_admin,
            estado=estado_habilitado,
            persona=persona_admin
        )

        Persistencia.get_session().add(admin)
        Persistencia.get_session().commit()

    @staticmethod
    def autenticar_usuario(username, password):
        estado = {
            'habilitado': Persistencia.get_session().query(EstadoUsuario)
                .filter(EstadoUsuario.nombre == 'habilitado').one(),
            'deshabilitado': Persistencia.get_session().query(EstadoUsuario)
                .filter(EstadoUsuario.nombre == 'deshabilitado').one(),
            'bloqueado': Persistencia.get_session().query(EstadoUsuario)
                .filter(EstadoUsuario.nombre == 'bloqueado').one(),
            'noverificado': Persistencia.get_session().query(EstadoUsuario)
                .filter(EstadoUsuario.nombre == 'no verificado').one(),
        }

        usuario = Persistencia.get_session().query(Usuario)\
            .filter(Usuario.nombre == username).one_or_none()

        if usuario is None:
            return None
        elif usuario.estado == estado['noverificado']:
            # Informar o reenviar verificación por email
            return None
        elif usuario.estado == estado['deshabilitado']:
            # Mostrar mensaje de regreso (welcome back, etc)
            pass
        elif usuario.estado == estado['bloqueado']:
            return None
        elif usuario.estado != estado['habilitado']:
            return None

        hashed_passwd = ExpCrypto.hash_password(password,
                                                usuario.passSalt,
                                                usuario.passParams)
        if hashed_passwd == usuario.passHash:
            return ExpCrypto.generar_token(
                usuario.nombre,
                usuario.tipo_usuario.nombre), usuario
        else:
            return None

    @staticmethod
    def crear_usuario_cliente(username, password, cliente):
        try:
            tipo = Persistencia.get_session().query(TipoUsuario)\
                .filter(TipoUsuario.nombre == 'cliente').one()
            estado = Persistencia.get_session().query(EstadoUsuario)\
                .filter(EstadoUsuario.nombre == 'habilitado').one()

            pass_salt = ExpCrypto.get_random_hex_bytes()
            pass_hash = ExpCrypto.hash_password(password, pass_salt)
            pass_params = ExpCrypto.get_pass_params()

            usuario = Usuario(nombre=username,
                              passHash=pass_hash,
                              passSalt=pass_salt,
                              passParams=pass_params,
                              tipo_usuario=tipo,
                              persona=cliente,
                              estado=estado)

            Persistencia.get_session().add(usuario)
            Persistencia.get_session().commit()
            return True

        except Exception as e:
            Persistencia.get_session().rollback()
            print(e)

    @staticmethod
    def crear_usuario_empleado(empleado):
        username = empleado.nombre + '.' + empleado.apellido
        password = 'linguiniapp1234'

        user_exist = Persistencia.query(Usuario)\
            .filter(Usuario.nombre == username).one_or_none()
        if user_exist is not None:
            return 400

        tipo = Persistencia.get_session().query(TipoUsuario) \
            .filter(TipoUsuario.nombre == 'empleado').one()
        estado = Persistencia.get_session().query(EstadoUsuario) \
            .filter(EstadoUsuario.nombre == 'habilitado').one()

        pass_salt = ExpCrypto.get_random_hex_bytes()
        pass_hash = ExpCrypto.hash_password(password, pass_salt)
        pass_params = ExpCrypto.get_pass_params()

        usuario = Usuario(nombre=username,
                          passHash=pass_hash,
                          passSalt=pass_salt,
                          passParams=pass_params,
                          tipo_usuario=tipo,
                          persona=empleado,
                          estado=estado)

        Persistencia.get_session().add(usuario)
        if Persistencia.commit():
            return 0
        else:
            return 500

    @staticmethod
    def get_usuario(username):
        return Persistencia.get_session().query(Usuario)\
            .filter(Usuario.nombre == username).one_or_none()

    @staticmethod
    def get_usuario_email(email):
        return Persistencia.get_session().query(Usuario)\
            .join(Usuario.persona, aliased=True)\
            .filter(Persona.email == email).one_or_none()

    @staticmethod
    def cambiar_estado_usuario(usuario, nombre_estado):
        estado = Persistencia.get_session().query(EstadoUsuario)\
            .filter(EstadoUsuario.nombre == nombre_estado).one()
        if estado is None or usuario.estado is estado:
            return False
        else:
            usuario.estado = estado
            Persistencia.add(usuario)
            if Persistencia.commit():
                return True
            else:
                return False

    @staticmethod
    def change_password(user, password_old, password_new):

        if not ExpAuth.autenticar_usuario(user.nombre, password_old):
            return 400

        pass_salt = ExpCrypto.get_random_hex_bytes()
        pass_hash = ExpCrypto.hash_password(password_new, pass_salt)
        pass_params = ExpCrypto.get_pass_params()

        user.passSalt = pass_salt
        user.passHash = pass_hash
        user.passParams = pass_params

        Persistencia.add(user)

        if Persistencia.commit():
            return 0
        else:
            return 500

    @staticmethod
    def recover_password(email):
        user = ExpAuth.get_usuario_email(email)
        if user is None:
            return 404
        else:
            token = ExpCrypto.generar_token(user.nombre, 'recover')
            return ExpMail.send_pass_recovery(email, token)

    @staticmethod
    def reset_password(token, password):
        token_info = ExpCrypto.decode_token(token)
        user = ExpAuth.get_usuario(token_info.get('sub'))
        # return ExpAuth.change_password(user, password)

        pass_salt = ExpCrypto.get_random_hex_bytes()
        pass_hash = ExpCrypto.hash_password(password, pass_salt)
        pass_params = ExpCrypto.get_pass_params()

        user.passSalt = pass_salt
        user.passHash = pass_hash
        user.passParams = pass_params

        Persistencia.add(user)

        if Persistencia.commit():
            return 0
        else:
            return 500

    @staticmethod
    def cambiar_email_admin(admin, email):
        admin.persona.email = email

        return 0
