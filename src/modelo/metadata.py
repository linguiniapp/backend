from modelo.persistencia import Entidad
from sqlalchemy import Column, Integer, DateTime, Boolean


class CryptoSettings(Entidad):
    __tablename__ = '__crypto_settings'
    fecha_hora = Column(DateTime, nullable=False)
    argon2_t = Column(Integer, nullable=False)
    argon2_m = Column(Integer, nullable=False)
    argon2_p = Column(Integer, nullable=False)


class DatabaseSettings(Entidad):
    __tablename__ = '__database_settings'
    fecha_hora = Column(DateTime, nullable=False)
    reset = Column(Boolean, nullable=False)
    version = Column(Integer, nullable=False)
