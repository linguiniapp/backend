from datetime import datetime
from os import environ
from threading import Thread, Lock

from sqlalchemy.exc import InterfaceError, ProgrammingError

from auth.exp_auth import ExpAuth
from modelo.entidades import *
from modelo.metadata import CryptoSettings
from modelo.metadata import DatabaseSettings
from modelo.persistencia import Persistencia

version = int(environ.get('LINGUINI_DB_VERSION'))
db_working = True
gil_lock = Lock()


def reset_db_task():
    global db_working
    Persistencia.get_session().close()
    print('Limpiando base de datos...')
    # Entidad.metadata.drop_all(bind=Persistencia.get_engine())
    con = Persistencia.get_engine().connect()
    for table in reversed(Entidad.metadata.sorted_tables):
        trans = con.begin()
        try:
            query = 'DROP TABLE "{}" CASCADE;'.format(str(table))
            con.execute(query)
            trans.commit()
        except Exception as e:
            print(e)
            trans.rollback()
    print('Recreando tablas...')
    Entidad.metadata.create_all(bind=Persistencia.get_engine())
    db_init()
    print('Base de datos lista!')
    db_working = False


def db_reset(hnd=False):
    with gil_lock:
        global db_working
        db_working = True
        t = Thread(target=reset_db_task)
        t.start()
        if hnd:
            return t


def db_load():
    global db_working
    # if db_working:
    #     print("La base de datos esta ocupada")
    #     return False
    # else:
    #     db_working = True

    try:
        print('Buscando configuracion de base de datos')
        db_settings = Persistencia.get_session().query(DatabaseSettings)\
            .order_by(DatabaseSettings.fecha_hora.desc()).first()
    except InterfaceError:
        print('La base de datos no esta lista')
        return False
    except ProgrammingError:
        print('No existe tabla de configuracion')
        db_settings = None

    Persistencia.get_session().close()

    if db_settings is None or db_settings.reset:
        db_reset()
        return False
    elif version is not None and db_settings.version < version:
        print('Version anterior detectada')
        db_reset()
        return False

    print("¡Base de datos lista!")
    db_working = False
    return True


def db_check(init=False):
    with gil_lock:
        global db_working
        if db_working and not init:
            return True

        try:
            if Persistencia.get_session():
                Persistencia.get_session().query(DatabaseSettings).first()
                ret = False
            else:
                ret = True
        except InterfaceError:
            ret = True
        except ProgrammingError:
            ret = False
        except Exception as e:
            print(e)
            ret = True
        finally:
            if Persistencia.get_session():
                Persistencia.get_session().close()

        return ret


def db_init():
    print('Cargando datos iniciales...')

    crypto_settings = CryptoSettings(fecha_hora=datetime.now(),
                                     argon2_t=16,
                                     argon2_m=8,
                                     argon2_p=1)
    Persistencia.get_session().add(crypto_settings)

    tipo_usuario = {
        'admin': TipoUsuario(codigo=0, nombre='admin'),
        'cliente': TipoUsuario(codigo=1, nombre='cliente'),
        'empleado': TipoUsuario(codigo=2, nombre='empleado'),
        'anonimo': TipoUsuario(codigo=3, nombre='anonimo'),
    }
    [Persistencia.get_session().add(e) for e in tipo_usuario.values()]

    estado_usuario = {
        'habilitado': EstadoUsuario(codigo=1, nombre='habilitado'),
        'deshabilitado': EstadoUsuario(codigo=2, nombre='deshabilitado'),
        'noverificado': EstadoUsuario(codigo=3, nombre='no verificado'),
        'bloqueado': EstadoUsuario(codigo=4, nombre='bloqueado'),
    }
    [Persistencia.get_session().add(e) for e in estado_usuario.values()]

    tipo_medio_pago = {
        'efectivo': TipoMedioPago(codigo=1, nombre='efectivo'),
        'debito': TipoMedioPago(codigo=2, nombre='debito'),
        'credito': TipoMedioPago(codigo=3, nombre='credito'),
    }
    [Persistencia.get_session().add(e) for e in tipo_medio_pago.values()]

    estado_cliente = {
        'habilitado': EstadoCliente(codigo=1, nombre='habilitado'),
        'sin usuario': EstadoCliente(codigo=2, nombre='sin usuario'),
        'suspendido': EstadoCliente(codigo=3, nombre='suspendido'),
        'deshabilitado': EstadoCliente(codigo=4, nombre='deshabilitado'),
    }
    [Persistencia.get_session().add(e) for e in estado_cliente.values()]

    estado_empleado = {
        'activo': EstadoEmpleado(codigo=1, nombre='activo'),
        'inactivo': EstadoEmpleado(codigo=2, nombre='inactivo'),
        'suspendido': EstadoEmpleado(codigo=3, nombre='suspendido'),
        'deshabilitado': EstadoEmpleado(codigo=4, nombre='deshabilitado'),
    }
    [Persistencia.get_session().add(e) for e in estado_empleado.values()]

    estado_sucursal = {
        'abierta': EstadoSucursal(codigo=1, nombre='abierta'),
        'cerrada': EstadoSucursal(codigo=2, nombre='cerrada'),
        'clausurada': EstadoSucursal(codigo=3, nombre='clausurada'),
        'suspendida': EstadoSucursal(codigo=4, nombre='suspendida'),
    }
    [Persistencia.get_session().add(e) for e in estado_sucursal.values()]

    # estado_sector = {
    #     'disponible': EstadoSector(codigo=1, nombre='disponible'),
    #     'no disponible': EstadoSector(codigo=2, nombre='no disponible'),
    # }
    # [Persistencia.get_session().add(e) for e in estado_sector.values()]

    estado_mesa = {
        'disponible': EstadoMesa(codigo=1, nombre='disponible'),
        'ocupada': EstadoMesa(codigo=2, nombre='ocupada'),
        'removida': EstadoMesa(codigo=3, nombre='removida'),
    }
    [Persistencia.get_session().add(e) for e in estado_mesa.values()]

    estado_reserva = {
        'solicitada': EstadoReserva(codigo=1, nombre='solicitada'),
        'anulada': EstadoReserva(codigo=2, nombre='anulada'),
        'utilizada': EstadoReserva(codigo=3, nombre='utilizada'),
    }
    [Persistencia.get_session().add(e) for e in estado_reserva.values()]

    estado_pedido = {
        'solicitado': EstadoPedido(codigo=1, nombre='solicitado'),
        'confirmado': EstadoPedido(codigo=7, nombre='confirmado'),
        'anulado': EstadoPedido(codigo=2, nombre='anulado'),
        'enpreparacion': EstadoPedido(codigo=3, nombre='en preparacion'),
        'listo': EstadoPedido(codigo=4, nombre='listo'),
        'entregado': EstadoPedido(codigo=5, nombre='entregado'),
        'devuelto': EstadoPedido(codigo=6, nombre='devuelto'),
        'pagado': EstadoPedido(codigo=8, nombre='pagado'),
        'pago parcial': EstadoPedido(codigo=9, nombre='pago parcial'),
    }
    [Persistencia.get_session().add(e) for e in estado_pedido.values()]

    tipo_pedido = {
        'remoto': TipoPedido(codigo=1, nombre='remoto'),
        'cliente': TipoPedido(codigo=2, nombre='presencial cliente'),
        'mozo': TipoPedido(codigo=3, nombre='presencial mozo'),
    }
    [Persistencia.get_session().add(e) for e in tipo_pedido.values()]

    estado_carta = {
        'habilitada': EstadoCarta(codigo=1, nombre='habilitada'),
        'deshabilitada': EstadoCarta(codigo=2, nombre='deshabilitada'),
    }
    [Persistencia.get_session().add(e) for e in estado_carta.values()]

    estado_promocion = {
        'vigente': EstadoPromocion(codigo=1, nombre='vigente'),
        'novigente': EstadoPromocion(codigo=2, nombre='no vigente'),
        'agotada': EstadoPromocion(codigo=3, nombre='agotada'),
    }
    [Persistencia.get_session().add(e) for e in estado_promocion.values()]

    estado_recompensa = {
        'vigente': EstadoRecompensa(codigo=1, nombre='vigente'),
        'novigente': EstadoRecompensa(codigo=2, nombre='no vigente'),
        'agotada': EstadoRecompensa(codigo=3, nombre='agotada'),
    }
    [Persistencia.get_session().add(e) for e in estado_recompensa.values()]

    estado_estadia = {
        'activa': EstadoEstadia(codigo=1, nombre='activa'),
        'finalizada': EstadoEstadia(codigo=2, nombre='finalizada'),
        'cerrada': EstadoEstadia(codigo=3, nombre='cerrada'),
    }
    [Persistencia.get_session().add(e) for e in estado_estadia.values()]

    estado_producto = {
        'disponible': EstadoProducto(codigo=1, nombre='disponible'),
        'retirado': EstadoProducto(codigo=2, nombre='retirado'),
        # 'agotado': EstadoProducto(codigo=3, nombre='agotado'),
    }
    [Persistencia.get_session().add(e) for e in estado_producto.values()]

    estado_factura = {
        'emitida': EstadoFactura(codigo=1, nombre='emitida'),
        'anulada': EstadoFactura(codigo=2, nombre='anulada'),
        'cobrada': EstadoFactura(codigo=3, nombre='cobrada'),
        'reembolsada': EstadoFactura(codigo=4, nombre='reembolsada'),
    }
    [Persistencia.get_session().add(e) for e in estado_factura.values()]

    estado_carta_producto = {
        'enstock': EstadoCartaProducto(codigo=1, nombre='en stock'),
        'agotado': EstadoCartaProducto(codigo=2, nombre='agotado'),
    }
    [Persistencia.get_session().add(e) for e in estado_carta_producto.values()]

    periodicidad = {
        'unica': Periodicidad(codigo=1, nombre='unica'),
        'diaria': Periodicidad(codigo=2, nombre='diaria'),
        'semanal': Periodicidad(codigo=3, nombre='semanal'),
        'mensual': Periodicidad(codigo=4, nombre='mensual'),
        'anual': Periodicidad(codigo=5, nombre='anual'),
    }
    [Persistencia.get_session().add(e) for e in periodicidad.values()]

    acciones = [
        Accion(codigo=0, nombre='asd'),
        Accion(codigo=1, nombre='ver_roles'),
        Accion(codigo=2, nombre='ver_acciones'),
        Accion(codigo=3, nombre='crear_rol'),
        Accion(codigo=4, nombre='modificar_rol'),
        Accion(codigo=5, nombre='eliminar_rol'),
        Accion(codigo=6, nombre='crear_carta'),
        Accion(codigo=7, nombre='ver_caja'),
        Accion(codigo=8, nombre='abrir_caja'),
        Accion(codigo=9, nombre='cerrar_caja'),
        Accion(codigo=10, nombre='crear_sucursal'),
        Accion(codigo=11, nombre='modificar_sucursal'),
        Accion(codigo=12, nombre='deshabilitar_sucursal'),
        # Accion(codigo=12, nombre='habilitar_sucursal'),
        Accion(codigo=13, nombre='ver_clientes'),
        Accion(codigo=14, nombre='modificar_cliente'),
        Accion(codigo=15, nombre='deshabilitar_cliente'),
        # Accion(codigo=15, nombre='habilitar_cliente'),
        Accion(codigo=16, nombre='suspender_cliente'),
        Accion(codigo=17, nombre='ver_pedidos'),
        Accion(codigo=18, nombre='pedido_remoto'),
        Accion(codigo=19, nombre='pedido_presencial'),
        Accion(codigo=20, nombre='ver_reservas'),
        Accion(codigo=21, nombre='crear_reserva'),
        Accion(codigo=22, nombre='modificar_reserva'),
        Accion(codigo=23, nombre='anular_reserva'),
        Accion(codigo=24, nombre='consumir_reserva'),
        Accion(codigo=25, nombre='ver_empleados'),
        Accion(codigo=26, nombre='crear_empleado'),
        Accion(codigo=27, nombre='modificar_empleado'),
        Accion(codigo=28, nombre='deshabilitar_empleado'),
        Accion(codigo=29, nombre='suspender_empleado'),
        Accion(codigo=30, nombre='habilitar_empleado'),
        Accion(codigo=31, nombre='asignar_rol'),
        Accion(codigo=32, nombre='quitar_rol'),
        Accion(codigo=33, nombre='anular_pedido'),
        Accion(codigo=34, nombre='check_in'),
        Accion(codigo=35, nombre='check_out'),
        Accion(codigo=36, nombre='ver_panel_admin'),
        Accion(codigo=37, nombre='ver_lista_espera'),
        Accion(codigo=38, nombre='agregar_lista_espera'),
        Accion(codigo=39, nombre='quitar_lista_espera'),
        Accion(codigo=40, nombre='ver_sucursal'),
        Accion(codigo=41, nombre='ver_categorias'),
        Accion(codigo=42, nombre='crear_categoria'),
        Accion(codigo=43, nombre='modificar_categoria'),
        Accion(codigo=44, nombre='eliminar_categoria'),
        Accion(codigo=45, nombre='ver_productos'),
        Accion(codigo=46, nombre='crear_producto'),
        Accion(codigo=47, nombre='modificar_producto'),
        Accion(codigo=48, nombre='deshabilitar_producto'),
        Accion(codigo=49, nombre='ver_promociones'),
        Accion(codigo=50, nombre='crear_promocion'),
        Accion(codigo=51, nombre='modificar_promocion'),
        Accion(codigo=52, nombre='deshabilitar_promocion'),
        Accion(codigo=53, nombre='ver_recompensas'),
        Accion(codigo=54, nombre='crear_recompensa'),
        Accion(codigo=55, nombre='modificar_recompensa'),
        Accion(codigo=56, nombre='deshabilitar_recompensa'),
        Accion(codigo=57, nombre='realizar_pago'),
        Accion(codigo=58, nombre='calificar_estadia'),
        Accion(codigo=59, nombre='crear_mesa'),
        Accion(codigo=60, nombre='modificar_mesa'),
        Accion(codigo=61, nombre='reporte_caja'),
        Accion(codigo=62, nombre='reporte_mozos'),
        Accion(codigo=63, nombre='reporte_calificaciones'),
        Accion(codigo=64, nombre='reporte_ventas'),
        Accion(codigo=65, nombre='reporte_productos'),
    ]
    [Persistencia.get_session().add(e) for e in acciones]

    Persistencia.add(
        Rol(
            codigo=0,
            nombre='admin',
            acciones=acciones
        )
    )

    acciones_cliente = (
            acciones[13:15]
            + acciones[17:23]
            + acciones[34:35]
            + acciones[37:41]
            + [acciones[45]]
            + [acciones[49]]
            + [acciones[53]]
            + [acciones[57]]
            + [acciones[59]]
    )

    Persistencia.add(
        Rol(
            codigo=1,
            nombre='cliente',
            acciones=acciones_cliente
        )
    )

    # Proveedores y medios de pago

    proveedores_pago = {
        'mercadopago': ProveedorPago(codigo=1, nombre='Mercado Pago'),
    }
    [Persistencia.add(e) for e in proveedores_pago.values()]

    tipos_factura = {
        'A': TipoFactura(codigo=1, nombre='A'),
        'B': TipoFactura(codigo=2, nombre='B'),
        'C': TipoFactura(codigo=3, nombre='C'),
        'D': TipoFactura(codigo=4, nombre='D'),
    }
    [Persistencia.add(e) for e in tipos_factura.values()]

    medios_pago = {
        'pesos': MedioPago(
            codigo=1,
            nombre='Pago en pesos (ARS)',
            tipo=tipo_medio_pago['efectivo']
        ),
        'visa': MedioPago(
            codigo=2,
            codigo_proveedor='visa',
            nombre='Visa',
            # proveedor=proveedores_pago['mercadopago'],
            tipo=tipo_medio_pago['credito']
        ),
        'mastercard': MedioPago(
            codigo=3,
            codigo_proveedor='master',
            nombre='MasterCard',
            proveedor=proveedores_pago['mercadopago'],
            tipo=tipo_medio_pago['credito']
        ),
        'americanexpress': MedioPago(
            codigo=4,
            codigo_proveedor='amex',
            nombre='American Express',
            proveedor=proveedores_pago['mercadopago'],
            tipo=tipo_medio_pago['credito']
        ),
        'visadebito': MedioPago(
            codigo=5,
            codigo_proveedor='debvisa',
            nombre='Visa Débito',
            proveedor=proveedores_pago['mercadopago'],
            tipo=tipo_medio_pago['debito']
        ),
        'maestro': MedioPago(
            codigo=6,
            codigo_proveedor='maestro',
            nombre='Maestro',
            proveedor=proveedores_pago['mercadopago'],
            tipo=tipo_medio_pago['debito']
        ),
    }
    [Persistencia.add(e) for e in medios_pago.values()]

    cuotas = {
        'mercadopago_visa_3': Cuotas(
            cantidad=3,
            cft=Decimal('276.10'),
            medio_pago=medios_pago['visa']
        ),
        'mercadopago_visa_6': Cuotas(
            cantidad=6,
            cft=Decimal('266.20'),
            medio_pago=medios_pago['visa']
        ),
        'mercadopago_visa_9': Cuotas(
            cantidad=9,
            cft=Decimal('247.77'),
            medio_pago=medios_pago['visa']
        )
    }
    [Persistencia.add(e) for e in cuotas.values()]

    # Configuracion de base de datos
    database_settings = DatabaseSettings(fecha_hora=datetime.now(),
                                         reset=False,
                                         version=version)
    Persistencia.get_session().add(database_settings)

    print('Guardando cambios en las tablas...')
    Persistencia.get_session().commit()
