from decimal import Decimal

from sqlalchemy import Date, Time, DateTime
from sqlalchemy import ForeignKey, Column, Table
from sqlalchemy import Integer, String, Boolean
from sqlalchemy import Numeric, Text
from sqlalchemy.orm import relationship, backref

from modelo.persistencia import Entidad


class Usuario(Entidad):
    __tablename__ = 'usuario'

    nombre = Column(String, unique=True, nullable=False)

    passHash = Column(String, nullable=False)
    passSalt = Column(String, nullable=False)
    passParams = Column(String, nullable=False)  # "t,m,p"
    # passArgon2T = Column(Integer, nullable=False)
    # passArgon2M = Column(Integer, nullable=False)
    # passArgon2P = Column(Integer, nullable=False)

    tipo_usuario = relationship('TipoUsuario')
    persona = relationship('Persona', backref=backref('usuario',
                                                      uselist=False))
    estado = relationship('EstadoUsuario')

    tipo_usuario_oid = Column(ForeignKey('tipo_usuario.oid'), nullable=False)
    persona_oid = Column(ForeignKey('persona.oid'), unique=True, nullable=True)
    estado_oid = Column(ForeignKey('estado_usuario.oid'), nullable=False)


class TipoUsuario(Entidad):
    __tablename__ = 'tipo_usuario'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class EstadoUsuario(Entidad):
    __tablename__ = 'estado_usuario'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Persona(Entidad):
    __tablename__ = 'persona'

    nombre = Column(String, nullable=False)
    apellido = Column(String, nullable=False)
    sexo = Column(String, nullable=True)
    email = Column(String, unique=True, nullable=False)
    telefono = Column(String, nullable=True)
    fecha_nacimiento = Column(Date, nullable=True)

    rol = relationship('Rol', backref='personas')

    rol_oid = Column(ForeignKey('rol.oid'), nullable=True)

    tipo = Column(String, nullable=False)
    __mapper_args__ = {
        'polymorphic_identity': 'persona',
        'polymorphic_on': tipo
    }


class Admin(Persona):
    __tablename__ = 'admin'
    oid = Column(Integer, ForeignKey('persona.oid'), primary_key=True)

    admin = Column(Boolean, nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'admin'
    }


class Cliente(Persona):
    __tablename__ = 'cliente'
    oid = Column(Integer, ForeignKey('persona.oid'), primary_key=True)

    dni = Column(String, nullable=False)
    direccion = Column(String, nullable=True)
    numero = Column(Integer, unique=True, nullable=False)

    estado = relationship('EstadoCliente')

    estado_oid = Column(ForeignKey('estado_cliente.oid'), nullable=False)

    __mapper_args__ = {
        'polymorphic_identity': 'cliente'
    }


class EstadoCliente(Entidad):
    __tablename__ = 'estado_cliente'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class PuntosCliente(Entidad):
    __tablename__ = 'puntos_cliente'

    total = Column(Integer, nullable=False)
    fecha_hora_actualizacion = Column(DateTime, nullable=False)

    historico_puntos = relationship('TransaccionPuntos',
                                    backref='cliente_puntos')
    cliente = relationship('Cliente', backref=backref('puntos',
                                                      uselist=False))

    cliente_oid = Column(ForeignKey('cliente.oid'), nullable=False)


class TransaccionPuntos(Entidad):
    __tablename__ = 'transaccion_puntos'

    fecha_hora = Column(DateTime, nullable=False)
    cantidad = Column(Integer, nullable=False)

    puntos_oid = Column(ForeignKey('puntos_cliente.oid'), nullable=False)


class Empleado(Persona):
    __tablename__ = 'empleado'
    oid = Column(Integer, ForeignKey('persona.oid'), primary_key=True)

    cuil = Column(String, unique=True, nullable=False)
    numero = Column(Integer, unique=True, nullable=False)

    historico_estados = relationship('EmpleadoEstado', backref='empleado')
    # historico_roles = relationship('EmpleadoRol', backref='empleado')

    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=True)

    __mapper_args__ = {
        'polymorphic_identity': 'empleado'
    }


# class EmpleadoRol(Entidad):
#     __tablename__ = 'empleado_rol'
#
#     fecha_desde = Column(Date, nullable=False)
#     fecha_hasta = Column(Date, nullable=True)
#
#     rol = relationship('Rol', backref='historico_empleados')
#
#     rol_oid = Column(ForeignKey('rol.oid'), nullable=False)
#     empleado_oid = Column(ForeignKey('empleado.oid'), nullable=False)


class EmpleadoEstado(Entidad):
    __tablename__ = 'empleado_estado'

    fecha_hora = Column(DateTime, nullable=False)
    descripcion = Column(String, nullable=False)

    estado = relationship('EstadoEmpleado')

    estado_oid = Column(ForeignKey('estado_empleado.oid'), nullable=False)
    empleado_oid = Column(ForeignKey('empleado.oid'), nullable=False)


class EstadoEmpleado(Entidad):
    __tablename__ = 'estado_empleado'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Rol(Entidad):
    __tablename__ = 'rol'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)

    acciones = relationship('Accion', secondary='rol_accion')


class Accion(Entidad):
    __tablename__ = 'accion'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


RolAccion = Table(
    'rol_accion',
    Entidad.metadata,
    Column('rol_oid', Integer, ForeignKey('rol.oid')),
    Column('accion_oid', Integer, ForeignKey('accion.oid'))
)


class Restaurante(Entidad):
    __tablename__ = 'restaurante'

    cuit = Column(String, unique=True, nullable=False)
    descripcion = Column(String, nullable=True)
    nombre = Column(String, nullable=False)
    foto = Column(Text, nullable=True)

    sucursales = relationship('Sucursal', backref='restaurante')


class Sucursal(Entidad):
    __tablename__ = 'sucursal'

    direccion = Column(String, nullable=False)
    nombre = Column(String, nullable=False)
    numero = Column(Integer, unique=True, nullable=False)
    foto = Column(Text, nullable=True)
    plano = Column(Text, nullable=True)
    telefono = Column(String, nullable=True)
    info_contacto = Column(Text, nullable=True)
    puntuacion = Column(String, nullable=False)

    latitud = Column(Numeric(precision=10, scale=7))
    longitud = Column(Numeric(precision=10, scale=7))

    hora_inicio_reserva = Column(Time, nullable=True)
    hora_fin_reserva = Column(Time, nullable=True)
    dias_reservables = relationship('WeekMask')

    # sectores = relationship('Sector', backref='sucursal')
    mesas = relationship('Mesa', backref='sucursal')
    estado = relationship('EstadoSucursal')
    empleados = relationship('Empleado', backref='sucursal')

    restaurante_oid = Column(ForeignKey('restaurante.oid'), nullable=False)
    estado_oid = Column(ForeignKey('estado_sucursal.oid'), nullable=False)
    dias_reservables_oid = Column(ForeignKey('week_mask.oid'), nullable=True)


class EstadoSucursal(Entidad):
    __tablename__ = 'estado_sucursal'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class PuntuacionSucursal(Entidad):
    __tablename__ = 'puntuacion_sucursal'

    fecha_hora = Column(DateTime, nullable=False)
    calificacion = Column(Integer, nullable=False)
    comentarios = Column(Text, nullable=True)

    sucursal = relationship('Sucursal', backref='puntuaciones')
    cliente = relationship('Cliente', backref='puntuaciones')
    estadia = relationship('Estadia', backref=backref('puntuacion',
                                                      uselist=False))

    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)
    cliente_oid = Column(ForeignKey('cliente.oid'), nullable=False)
    estadia_oid = Column(ForeignKey('estadia.oid'), nullable=False)


class Caja(Entidad):
    __tablename__ = 'caja'

    numero = Column(Integer, unique=True, nullable=False)
    fecha_hora_apertura = Column(DateTime, nullable=False)
    fecha_hora_cierre = Column(DateTime, nullable=True)
    monto_inicial = Column(Numeric(precision=8, scale=2), nullable=False)
    monto_final_estimado = Column(Numeric(precision=8, scale=2),
                                  nullable=False)
    monto_final_real = Column(Numeric(precision=8, scale=2), nullable=True)
    diferencia = Column(Numeric(precision=8, scale=2), nullable=True)
    comentarios = Column(String, nullable=True)

    sucursal = relationship('Sucursal')
    cajero = relationship('Empleado', backref='cajas')

    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)
    cajero_oid = Column(ForeignKey('empleado.oid'), nullable=True)


# class Sector(Entidad):
#     __tablename__ = 'sector'
#
#     numero = Column(Integer, nullable=False)
#     piso = Column(Integer, nullable=False)
#     # Lista de tuplas en formato "x,y-x,y-x,y"
#     vertices = Column(String, nullable=True)
#
#     mesas = relationship('Mesa', backref='sector')
#     estado = relationship('EstadoSector')
#
#     sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)
#     estado_oid = Column(ForeignKey('estado_sector.oid'), nullable=False)


# class EstadoSector(Entidad):
#     __tablename__ = 'estado_sector'
#
#     codigo = Column(Integer, unique=True, nullable=False)
#     nombre = Column(String, unique=True, nullable=False)


class Mesa(Entidad):
    __tablename__ = 'mesa'

    numero = Column(Integer, nullable=False)
    capacidad_personas = Column(Integer, nullable=False)
    reservable = Column(Boolean, nullable=False, default=True)
    posicion_x = Column(Integer, nullable=True)
    posicion_y = Column(Integer, nullable=True)

    estado = relationship('EstadoMesa', backref='mesas')

    estado_oid = Column(ForeignKey('estado_mesa.oid'), nullable=False)
    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)
    # sector_oid = Column(ForeignKey('sector.oid'), nullable=False)


class EstadoMesa(Entidad):
    __tablename__ = 'estado_mesa'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Reserva(Entidad):
    __tablename__ = 'reserva'

    fecha_hora = Column(DateTime, nullable=False)
    numero = Column(Integer, unique=True, nullable=False)
    cantidad_personas = Column(Integer, nullable=False)

    cliente = relationship('Cliente', foreign_keys='[Reserva.user_oid]',
                           backref='reservas_user')
    owner = relationship('Persona', foreign_keys='[Reserva.owner_oid]',
                         backref='reservas_owner')
    historico_estados = relationship('ReservaEstado', backref='reserva')
    mesas = relationship('Mesa', secondary='reserva_mesa', backref='reservas')
    pedidos = relationship('Pedido', backref='reserva')
    sucursal = relationship('Sucursal')

    user_oid = Column(ForeignKey('persona.oid'), nullable=False)
    owner_oid = Column(ForeignKey('persona.oid'), nullable=False)
    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)


ReservaMesa = Table('reserva_mesa',
                    Entidad.metadata,
                    Column('reserva_oid', Integer, ForeignKey('reserva.oid')),
                    Column('mesa_oid', Integer, ForeignKey('mesa.oid')))


class ReservaEstado(Entidad):
    __tablename__ = 'reserva_estado'

    fecha_hora = Column(DateTime, nullable=False)
    comentario = Column(String, nullable=True)

    estado = relationship('EstadoReserva', backref='historico_reservas')

    estado_oid = Column(ForeignKey('estado_reserva.oid'), nullable=False)
    reserva_oid = Column(ForeignKey('reserva.oid'), nullable=False)


class EstadoReserva(Entidad):
    __tablename__ = 'estado_reserva'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Estadia(Entidad):
    __tablename__ = 'estadia'

    cantidad_personas = Column(Integer, nullable=True)
    fecha_hora_llegada = Column(DateTime, nullable=False)
    fecha_hora_salida = Column(DateTime, nullable=True)
    numero = Column(Integer, unique=True, nullable=False)
    cierre_forzado = Column(Boolean, nullable=False, default=False)

    reserva = relationship('Reserva')
    mesas = relationship('Mesa',
                         secondary='estadia_mesa')
    sucursal = relationship('Sucursal', backref='estadias')
    mozo = relationship('Empleado', backref='estadia')
    pedidos = relationship('Pedido', backref='estadia')
    facturas = relationship('Factura', backref='estadia')
    clientes = relationship('Cliente',
                            secondary='estadia_cliente')
    clientes_retirados = relationship('Cliente',
                                      secondary='estadia_retirados')
    estado = relationship('EstadoEstadia')

    reserva_oid = Column(ForeignKey('reserva.oid'), nullable=True)
    mozo_oid = Column(ForeignKey('empleado.oid'), nullable=True)
    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)
    estado_oid = Column(ForeignKey('estado_estadia.oid'), nullable=False)


class EstadoEstadia(Entidad):
    __tablename__ = 'estado_estadia'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


EstadiaMesa = Table(
    'estadia_mesa',
    Entidad.metadata,
    Column('estadia_oid', Integer, ForeignKey('estadia.oid')),
    Column('mesa_oid', Integer, ForeignKey('mesa.oid'))
)


EstadiaCliente = Table(
    'estadia_cliente',
    Entidad.metadata,
    Column('estadia_oid', Integer, ForeignKey('estadia.oid')),
    Column('cliente_oid', Integer, ForeignKey('cliente.oid'))
)


EstadiaRetirados = Table(
    'estadia_retirados',
    Entidad.metadata,
    Column('estadia_oid', Integer, ForeignKey('estadia.oid')),
    Column('cliente_oid', Integer, ForeignKey('cliente.oid'))
)


class Pedido(Entidad):
    __tablename__ = 'pedido'

    fecha_hora = Column(DateTime, nullable=False)
    numero = Column(Integer, unique=True, nullable=False)
    comentario = Column(String, nullable=True)

    # Usado para verificar el token QR
    rnd_int = Column(String, nullable=True)

    tipo = relationship('TipoPedido', backref='pedidos')
    detalles = relationship('DetallePedido', backref='pedido')
    promociones = relationship('PedidoPromocion', backref='pedido')
    recompensas = relationship('PedidoRecompensa', backref='pedido')
    historico_estados = relationship('PedidoEstado', backref='pedido')

    estadia_oid = Column(ForeignKey('estadia.oid'), nullable=True)
    reserva_oid = Column(ForeignKey('reserva.oid'), nullable=True)
    tipo_oid = Column(ForeignKey('tipo_pedido.oid'), nullable=False)


class TipoPedido(Entidad):
    __tablename__ = 'tipo_pedido'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class PedidoEstado(Entidad):
    __tablename__ = 'pedido_estado'

    fecha_hora = Column(DateTime, nullable=False)

    estado = relationship('EstadoPedido', backref='historico_pedidos')

    pedido_oid = Column(ForeignKey('pedido.oid'), nullable=False)
    estado_oid = Column(ForeignKey('estado_pedido.oid'), nullable=False)


class DetallePedido(Entidad):
    __tablename__ = 'detalle_pedido'

    cantidad = Column(Integer, nullable=False)
    comentarios = Column(String, nullable=True)

    carta_producto = relationship('CartaProducto')

    pedido_oid = Column(ForeignKey('pedido.oid'), nullable=False)
    cartaProducto_oid = Column(ForeignKey('carta_producto.oid'),
                               nullable=False)


class PedidoPromocion(Entidad):
    __tablename__ = 'pedido_promocion'

    cantidad = Column(Integer, nullable=False)
    promocion = relationship('Promocion')
    pedido_oid = Column(ForeignKey('pedido.oid'), nullable=False)
    promocion_oid = Column(ForeignKey('promocion.oid'), nullable=False)


class PedidoRecompensa(Entidad):
    __tablename__ = 'pedido_recompensa'

    cantidad = Column(Integer, nullable=False)
    recompensa = relationship('Recompensa')
    pedido_oid = Column(ForeignKey('pedido.oid'), nullable=False)
    recompensa_oid = Column(ForeignKey('recompensa.oid'), nullable=False)


class EstadoPedido(Entidad):
    __tablename__ = 'estado_pedido'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Producto(Entidad):
    __tablename__ = 'producto'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, nullable=False)
    foto = Column(Text, nullable=True)
    descripcion = Column(Text, nullable=True)

    estado = relationship('EstadoProducto', backref='productos')
    categorias = relationship('CategoriaProducto',
                              secondary='producto_categoria',
                              backref='productos')

    estado_oid = Column(ForeignKey('estado_producto.oid'), nullable=False)


class EstadoProducto(Entidad):
    __tablename__ = 'estado_producto'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, nullable=False)


class CategoriaProducto(Entidad):
    __tablename__ = 'categoria_producto'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)
    foto = Column(Text, nullable=True)


ProductoCategoria = Table(
    'producto_categoria',
    Entidad.metadata,
    Column('producto_oid', Integer, ForeignKey('producto.oid')),
    Column('categoria_oid', Integer, ForeignKey('categoria_producto.oid'))
)


class SucursalOferta(Entidad):
    __tablename__ = 'sucursal_oferta'

    fecha_desde = Column(Date, nullable=False)
    fecha_hasta = Column(Date, nullable=True)

    sucursal = relationship('Sucursal', backref='historico_ofertas')
    carta = relationship('Carta')
    promociones = relationship('Promocion',
                               secondary='oferta_promocion',
                               backref='sucursales_oferentes')
    recompensas = relationship('Recompensa',
                               secondary='oferta_recompensa',
                               backref='sucursales_oferentes')

    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)
    carta_oid = Column(ForeignKey('carta.oid'), nullable=False)


class Promocion(Entidad):
    __tablename__ = 'promocion'

    numero = Column(Integer, unique=True, nullable=False)
    precio = Column(Numeric(precision=8, scale=2), nullable=True)
    descuento = Column(Numeric(precision=8, scale=2), nullable=True)
    nombre = Column(String, nullable=True)
    descripcion = Column(String, nullable=True)
    foto = Column(Text, nullable=True)

    mes_desde = Column(Integer, nullable=True)
    mes_hasta = Column(Integer, nullable=True)
    dia_desde = Column(Integer, nullable=True)
    dia_hasta = Column(Integer, nullable=True)
    hora_desde = Column(Time, nullable=True)
    hora_hasta = Column(Time, nullable=True)

    week_day = relationship('WeekMask')
    estado = relationship('EstadoPromocion', backref='promociones')
    periodicidad = relationship('Periodicidad', backref='promociones')
    detalles = relationship('DetallePromocion', backref='promocion')

    estado_oid = Column(ForeignKey('estado_promocion.oid'), nullable=False)
    periodicidad_oid = Column(ForeignKey('periodicidad.oid'), nullable=False)
    week_oid = Column(ForeignKey('week_mask.oid'), nullable=True)


OfertaPromocion = Table('oferta_promocion',
                        Entidad.metadata,
                        Column('oferta_oid', Integer,
                               ForeignKey('sucursal_oferta.oid')),
                        Column('promocion_oid', Integer,
                               ForeignKey('promocion.oid')))


class WeekMask(Entidad):
    __tablename__ = 'week_mask'

    lunes = Column(Boolean, nullable=False)
    martes = Column(Boolean, nullable=False)
    miercoles = Column(Boolean, nullable=False)
    jueves = Column(Boolean, nullable=False)
    viernes = Column(Boolean, nullable=False)
    sabado = Column(Boolean, nullable=False)
    domingo = Column(Boolean, nullable=False)


class DetallePromocion(Entidad):
    __tablename__ = 'detalle_promocion'

    cantidad = Column(Integer, nullable=False)

    producto = relationship('Producto')

    promocion_oid = Column(ForeignKey('promocion.oid'), nullable=False)
    producto_oid = Column(ForeignKey('producto.oid'), nullable=False)


class EstadoPromocion(Entidad):
    __tablename__ = 'estado_promocion'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Periodicidad(Entidad):
    __tablename__ = 'periodicidad'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Recompensa(Entidad):
    __tablename__ = 'recompensa'

    numero = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, nullable=True)
    puntos = Column(Integer, nullable=False)
    foto = Column(Text, nullable=True)

    detalles = relationship('DetalleRecompensa', backref='recompensa')
    estado = relationship('EstadoRecompensa', backref='recompensas')

    estado_oid = Column(ForeignKey('estado_recompensa.oid'), nullable=False)


OfertaRecompensa = Table('oferta_recompensa',
                         Entidad.metadata,
                         Column('oferta_oid',
                                Integer,
                                ForeignKey('sucursal_oferta.oid')),
                         Column('recompensa_oid',
                                Integer,
                                ForeignKey('recompensa.oid')))


class EstadoRecompensa(Entidad):
    __tablename__ = 'estado_recompensa'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class DetalleRecompensa(Entidad):
    __tablename__ = 'detalle_recompensa'

    cantidad = Column(Integer, nullable=False)

    producto = relationship('Producto')

    producto_oid = Column(ForeignKey('producto.oid'), nullable=False)
    recompensa_oid = Column(ForeignKey('recompensa.oid'), nullable=False)


class Carta(Entidad):
    __tablename__ = 'carta'

    numero = Column(Integer, unique=True, nullable=False)

    carta_productos = relationship('CartaProducto', backref='carta')
    estado = relationship('EstadoCarta', backref='carta')

    estado_oid = Column(ForeignKey('estado_carta.oid'), nullable=False)


class CartaProducto(Entidad):
    __tablename__ = 'carta_producto'

    precio = Column(Numeric(precision=6, scale=2), nullable=True)

    producto = relationship('Producto')
    estado = relationship('EstadoCartaProducto')

    estado_oid = Column(ForeignKey('estado_carta_producto.oid'),
                        nullable=False)
    producto_oid = Column(ForeignKey('producto.oid'), nullable=False)
    carta_oid = Column(ForeignKey('carta.oid'), nullable=False)


class EstadoCartaProducto(Entidad):
    __tablename__ = 'estado_carta_producto'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class EstadoCarta(Entidad):
    __tablename__ = 'estado_carta'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Factura(Entidad):
    __tablename__ = 'factura'

    fecha = Column(Date, nullable=False)
    numero = Column(Integer, unique=True, nullable=False)
    total = Column(Numeric(precision=8, scale=2), nullable=False)
    cantidad_cuotas = Column(Integer, nullable=False)
    cuit_empresa = Column(String, nullable=False)
    id_cliente = Column(String, nullable=False)
    tipo_id_cliente = Column(String, nullable=False)
    nombre_cliente = Column(String, nullable=False)
    apellido_cliente = Column(String, nullable=False)
    email_cliente = Column(String, nullable=True)
    qr_hash = Column(String, nullable=True)
    external_id = Column(String, nullable=True)

    pedidos = relationship('Pedido', backref='facturas',
                           secondary='factura_pedido')
    detalles = relationship('DetalleFactura', backref='factura')
    tipo = relationship('TipoFactura')
    medio_pago = relationship('MedioPago')
    estado = relationship('EstadoFactura', backref='factura')

    tipo_oid = Column(ForeignKey('tipo_factura.oid'), nullable=False)
    medio_pago_oid = Column(ForeignKey('medio_pago.oid'), nullable=False)
    estadia_oid = Column(ForeignKey('estadia.oid'), nullable=False)
    estado_oid = Column(ForeignKey('estado_factura.oid'), nullable=False)


FacturaPedido = Table(
    'factura_pedido',
    Entidad.metadata,
    Column('factura_oid', Integer, ForeignKey('factura.oid')),
    Column('pedido_oid', Integer, ForeignKey('pedido.oid'))
)


class DetalleFactura(Entidad):
    __tablename__ = 'detalle_factura'

    cantidad = Column(Integer, nullable=False)
    precio_unitario = Column(Numeric(precision=8, scale=2), nullable=False)
    subtotal = Column(Numeric(precision=8, scale=2), nullable=False)
    descripcion = Column(String, nullable=False)

    factura_oid = Column(ForeignKey('factura.oid'), nullable=False)


class EstadoFactura(Entidad):
    __tablename__ = 'estado_factura'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class TipoFactura(Entidad):
    __tablename__ = 'tipo_factura'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class ProveedorPago(Entidad):
    __tablename__ = 'proveedor_pago'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)
    url_api = Column(String, nullable=True)

    medios_pago = relationship('MedioPago', backref='proveedor')


class MedioPago(Entidad):
    __tablename__ = 'medio_pago'

    codigo = Column(Integer, unique=True, nullable=False)
    codigo_proveedor = Column(String, unique=False, nullable=True)
    nombre = Column(String, unique=False, nullable=False)
    comision_fija = Column(Numeric(precision=6, scale=2), nullable=True)
    comision_porcentual = Column(Numeric(precision=5, scale=2), nullable=True)

    cuotas = relationship('Cuotas', backref='medio_pago')
    tipo = relationship('TipoMedioPago')

    proveedor_oid = Column(ForeignKey('proveedor_pago.oid'), nullable=True)
    tipo_medio_pago = Column(ForeignKey('tipo_medio_pago.oid'), nullable=False)


class TipoMedioPago(Entidad):
    __tablename__ = 'tipo_medio_pago'

    codigo = Column(Integer, unique=True, nullable=False)
    nombre = Column(String, unique=True, nullable=False)


class Cuotas(Entidad):
    __tablename__ = 'cuotas'

    cantidad = Column(Integer, nullable=False)
    # recargo = Column(Numeric(precision=5, scale=2), nullable=False)
    cft = Column(Numeric(precision=5, scale=2), nullable=False)

    medio_pago_oid = Column(ForeignKey('medio_pago.oid'), nullable=False)


# Cosas que no deberian ser
class ListaEspera(Entidad):
    __tablename__ = '--lista_espera'

    fecha_hora_apertura = Column(DateTime, nullable=False)
    fecha_hora_cierre = Column(DateTime, nullable=True)

    detalles = relationship('ListaEsperaDetalle', backref='lista_espera')
    sucursal = relationship('Sucursal', backref=backref('lista_espera',
                                                        uselist=False))

    sucursal_oid = Column(ForeignKey('sucursal.oid'), nullable=False)


class ListaEsperaDetalle(Entidad):
    __tablename__ = '--lista_espera_detalle'

    nombre = Column(String, nullable=False)
    numero = Column(Integer, nullable=False)
    fecha_hora_llegada = Column(DateTime, nullable=False)
    cantidad_personas = Column(Integer, nullable=False)

    lista_espera_oid = Column(ForeignKey('--lista_espera.oid'),
                              nullable=False)


# No persistente / DTOs
class DTOPreFactura:
    sucursal = None
    mesas = []
    estadia = None
    detalles = []
    total = Decimal('0.00')

    def __init__(self, sucursal, estadia, mesas, detalles, total):
        self.sucursal = sucursal
        self.estadia = estadia
        self.mesas = mesas
        self.detalles = detalles
        self.total = total


class DTOPreFacturaDetalle:
    nombre = None
    precio = Decimal('0.00')
    cantidad = 0
    subtotal = Decimal('0.00')

    def __init__(self, nombre, precio, cantidad):
        self.nombre = nombre
        self.precio = precio
        self.cantidad = cantidad
