from os import environ

from sqlalchemy import Column, Integer
from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.pool import NullPool

meta = MetaData(schema="app")


class EntidadCls:
    oid = Column(Integer, primary_key=True)


Entidad = declarative_base(cls=EntidadCls)


class Persistencia:

    engine = None
    db_session = None

    @staticmethod
    def init():
        db_uri = environ.get('LINGUINI_DB_URL')
        db_user = environ.get('LINGUINI_DB_USER')
        db_passwd = environ.get('LINGUINI_DB_PASSWORD')
        db_connstring = 'postgresql+pg8000://' + db_user + ':' + db_passwd + '@' + db_uri

        Persistencia.engine = create_engine(
            db_connstring,
            convert_unicode=True,
            poolclass=NullPool
        )

        Persistencia.db_session = scoped_session(sessionmaker(
            autocommit=False,
            autoflush=False,
            bind=Persistencia.engine
        ))

        Entidad.query = Persistencia.db_session.query_property()

    @staticmethod
    def get_session():
        return Persistencia.db_session

    @staticmethod
    def get_engine():
        return Persistencia.engine

    @staticmethod
    def query(entity):
        return Persistencia.db_session.query(entity)

    @staticmethod
    def commit():
        try:
            Persistencia.db_session.commit()
            return True
        except Exception as e:
            print(e)
            Persistencia.db_session.rollback()
            return False

    @staticmethod
    def rollback():
        Persistencia.db_session.rollback()

    @staticmethod
    def add(entity):
        Persistencia.db_session.add(entity)

    @staticmethod
    def delete(entity):
        Persistencia.db_session.delete(entity)
