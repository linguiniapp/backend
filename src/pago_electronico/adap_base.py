from abc import ABC, abstractmethod


class AdapPagoElectronico(ABC):

    @abstractmethod
    def efectuar_pago(self, factura, params):
        pass
