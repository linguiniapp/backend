from .adap_base import AdapPagoElectronico


class AdapMockValido(AdapPagoElectronico):

    @staticmethod
    def efectuar_pago(factura, params):
        return 0
