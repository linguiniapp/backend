from json import dumps

from mercadopago import MP

from .adap_base import AdapPagoElectronico

from modelo.persistencia import Persistencia


class AdapMercadoPago(AdapPagoElectronico):

    def __init__(self, mp_key):
        # Obtiene el access token de una variable de entorno
        # e inicializa la conexion a mercado pago
        self.mp_conn = MP(mp_key)

    def efectuar_pago(self, factura, params):

        # Obtiene datos de la factura para solicitar el pago
        sucursal = factura.estadia.sucursal
        restaurante = sucursal.restaurante
        descripcion = ' '.join([restaurante.nombre,
                                '- sucursal:',
                                sucursal.nombre])
        cuotas = factura.cantidad_cuotas

        if factura.tipo_id_cliente == 'dni':
            tipo_id = 'DNI'
        elif factura.tipo_id_cliente == 'le':
            tipo_id = 'LE'
        elif factura.tipo_id_cliente == 'lc':
            tipo_id = 'LC'
        elif factura.tipo_id_cliente == 'ci':
            tipo_id = 'CI'
        else:
            tipo_id = 'Otro'

        payment_data = {
            'transaction_amount': factura.total,
            'token': params.get('id'),
            'description': descripcion,
            'installments': cuotas,
            'payer': {
                'first_name': factura.nombre_cliente,
                'last_name': factura.apellido_cliente,
                'email': factura.email_cliente,
                'identification': {
                    'type': tipo_id,
                    'number': factura.id_cliente
                }
            },
            'binary_mode': True,
            'external_reference': str(factura.numero),
            'statement_descriptor': ' '.join(['LinguiniApp:', restaurante])
        }
        if factura.medio_pago.codigo_proveedor:
            payment_data['payment_method_id'] = (
                factura.medio_pago.codigo_proveedor)

        payment = self.mp_conn.post('/v1/payments', payment_data)

        try:
            response = dumps(payment)
        except Exception as e:
            print(e)
            return 500

        mp_id = response.get('id')
        if mp_id:
            factura.external_id = mp_id
            Persistencia.add(factura)
        else:
            return 500

        if Persistencia.commit():
            return 0
        else:
            return 500
