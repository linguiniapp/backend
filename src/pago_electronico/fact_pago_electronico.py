from os import environ

from .adap_mock_valid import AdapMockValido
from .adap_mercado_pago import AdapMercadoPago


class FactPagoElectronico:

    @staticmethod
    def get_adaptador(medio_pago):

        if not medio_pago.proveedor:
            return AdapMockValido()

        if medio_pago.proveedor.nombre == 'Mercado Pago':
            mp_key = environ.get('LINGUINIAPP_MERCADO_PAGO_ACCESS_TOKEN')
            return AdapMercadoPago(mp_key)
        else:
            return AdapMockValido()
