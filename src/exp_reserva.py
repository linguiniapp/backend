from datetime import datetime

from exp_cliente import ExpCliente
from exp_mesa import ExpMesa
from modelo.entidades import Reserva, EstadoReserva, ReservaEstado
from modelo.persistencia import Persistencia


class ExpReserva:

    @staticmethod
    def get_reserva(numero_reserva):
        reserva = Persistencia.get_session().query(Reserva)\
            .filter(Reserva.numero == numero_reserva).one_or_none()
        return reserva

    @staticmethod
    def ver_reservas(sucursal, fecha_hora_desde, fecha_hora_hasta):

        estado_solicitada = Persistencia.get_session().query(EstadoReserva)\
            .filter(EstadoReserva.nombre == 'solicitada').one()

        reservas = Persistencia.get_session().query(Reserva)\
            .filter(Reserva.sucursal == sucursal)\
            .join(Reserva.historico_estados, aliased=True)\
            .filter_by(estado=estado_solicitada)

        if fecha_hora_desde is not None:
            reservas = reservas.filter(Reserva.fecha_hora > fecha_hora_desde)

        if fecha_hora_hasta is not None:
            reservas = reservas.filter(Reserva.fecha_hora < fecha_hora_hasta)

        return reservas.all()

    @staticmethod
    def crear_reserva(fecha_hora, owner, user,
                      sucursal, mesas, cantidad_personas):
        ultimo = Persistencia.get_session().query(Reserva) \
            .order_by(Reserva.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        if type(user) is str:
            nombre, apellido, dni = user.split(',')
            ok, user = ExpCliente.crear_cliente_rapido(nombre,
                                                       apellido,
                                                       dni)

        if not ExpMesa.check_mesa_disponible(mesas, fecha_hora):
            # Informar que mesa no se pudo reservar
            return None, 403

        reserva = Reserva(fecha_hora=fecha_hora,
                          numero=numero,
                          owner=owner,
                          cliente=user,
                          mesas=mesas,
                          cantidad_personas=cantidad_personas,
                          sucursal=sucursal)

        estado_solicitada = Persistencia.get_session().query(EstadoReserva).\
            filter(EstadoReserva.nombre == 'solicitada').one()

        reserva_estado = ReservaEstado(fecha_hora=datetime.now(),
                                       reserva=reserva,
                                       estado=estado_solicitada)

        Persistencia.get_session().add(reserva)
        Persistencia.get_session().add(reserva_estado)
        Persistencia.get_session().commit()

        return reserva, 0

    @staticmethod
    def modificar_reserva(reserva, cantidad_personas, fecha_hora,
                          cliente, mesas):
        if fecha_hora is not None:
            reserva.fecha_hora = fecha_hora
        else:
            fecha_hora = reserva.fecha_hora

        if cliente is not None:
            reserva.cliente = cliente

        if mesas:
            for mesa in mesas:
                if not ExpMesa.check_mesa_disponible(mesa, fecha_hora):
                    # Informar que mesa no se pudo reservar
                    return 403, reserva
            reserva.mesas = mesas

        if cantidad_personas is not None:
            reserva.cantidad_personas = cantidad_personas

        Persistencia.get_session().add(reserva)
        Persistencia.get_session().commit()

        return 0, reserva

    @staticmethod
    def anular_reserva(numero_reserva, usuario, comentario):
        reserva = Persistencia.get_session().query(Reserva)\
            .filter(Reserva.numero == numero_reserva).one_or_none()

        # estado_solicitada = Persistencia.get_session().query(EstadoReserva)
        # .filter(EstadoReserva.nombre == 'solicitada').one()
        estado_anulada = Persistencia.get_session().query(EstadoReserva)\
            .filter(EstadoReserva.nombre == 'anulada').one()
        estado_utilizada = Persistencia.get_session().query(EstadoReserva)\
            .filter(EstadoReserva.nombre == 'utilizada').one()

        if reserva is None:
            return 404, None

        estado_reserva = Persistencia.get_session().query(ReservaEstado)\
            .filter(ReservaEstado.reserva == reserva)\
            .order_by(ReservaEstado.fecha_hora.desc()).first()

        if (estado_reserva.estado == estado_anulada
                or estado_reserva.estado == estado_utilizada):
            return 404, None

        if (usuario.tipo_usuario.nombre == 'cliente'
                and reserva.cliente == usuario.persona):
            comentario = 'Reserva anulada por el cliente'
        else:
            if comentario is None:
                return 400, None

        estado_reserva = ReservaEstado(fecha_hora=datetime.now(),
                                       comentario=comentario,
                                       reserva=reserva,
                                       estado=estado_anulada)

        Persistencia.get_session().add(estado_reserva)
        Persistencia.get_session().commit()

        return 0, reserva
