from datetime import datetime

from exp_empleado import ExpEmpleado
from exp_pedido import ExpPedido
from exp_mesa import ExpMesa
from modelo.entidades import Estadia, EstadoEstadia
from modelo.entidades import PuntuacionSucursal
from modelo.persistencia import Persistencia


class ExpEstadia:

    @staticmethod
    def get_estadia(numero_estadia):
        Persistencia.get_session().query(Estadia)\
            .filter(Estadia.numero == numero_estadia).one_or_none()

    @staticmethod
    def ver_estadia_actual_cliente(cliente):
        if cliente is None:
            return None

        return Persistencia.get_session().query(Estadia)\
            .filter(Estadia.clientes.contains(cliente),
                    Estadia.fecha_hora_llegada <= datetime.now(),
                    Estadia.fecha_hora_salida.is_(None)).one_or_none()

    @staticmethod
    def ver_estadia_actual_mesa(mesa):
        if mesa is None:
            return None

        return Persistencia.get_session().query(Estadia)\
            .filter(Estadia.mesas.contains(mesa),
                    Estadia.fecha_hora_llegada <= datetime.now(),
                    Estadia.fecha_hora_salida.is_(None)).one_or_none()

    @staticmethod
    def ver_estadia_reserva(reserva):
        return Persistencia.query(Estadia)\
            .filter(Estadia.reserva == reserva).one_or_none()

    @staticmethod
    def autogenerar_estadia(sucursal, mesa, empleado):
        ultimo = Persistencia.query(Estadia)\
            .order_by(Estadia.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        estado_activa = Persistencia.query(EstadoEstadia).filter(
            EstadoEstadia.nombre == 'activa'
        ).one()

        estadia = Estadia(
            numero=numero,
            fecha_hora_llegada=datetime.now(),
            sucursal=sucursal,
            estado=estado_activa
        )

        estadia.mesas.append(mesa)
        ExpMesa.ocupar_mesa(mesa)

        mozo = ExpEmpleado.asignar_mozo(estadia, empleado)
        if mozo:
            # Agregar verificación de mozo existente
            pass

        Persistencia.add(estadia)
        if Persistencia.commit():
            return estadia
        else:
            return None

    @staticmethod
    def abrir_estadia_reserva(reserva, clientes):

        ultimo = Persistencia.query(Estadia)\
            .order_by(Estadia.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        cantidad = reserva.cantidad_personas or 0
        mesas = reserva.mesas or []
        sucursal = reserva.sucursal

        estado_activa = Persistencia.query(EstadoEstadia).filter(
            EstadoEstadia.nombre == 'activa'
        ).one()

        estadia = Estadia(
            numero=numero,
            fecha_hora_llegada=datetime.now(),
            cantidad_personas=cantidad,
            reserva=reserva,
            sucursal=sucursal,
            estado=estado_activa
        )

        if isinstance(mesas, list):
            estadia.mesas += mesas
        else:
            estadia.mesas.append(mesas)

        [ExpMesa.ocupar_mesa(mesa) for mesa in mesas]

        if clientes:
            estadia.clientes += clientes

        for pedido in reserva.pedidos:
            pedido.estadia = estadia
            Persistencia.add(pedido)

        Persistencia.add(estadia)

        if Persistencia.commit():
            return estadia, 0
        else:
            return None, 500

    @staticmethod
    def agregar_cliente_estadia(estadia, cliente):
        estadia.clientes += cliente
        Persistencia.add(estadia)

        if Persistencia.commit():
            return estadia, 0
        else:
            return None, 500

    @staticmethod
    def abrir_estadia_cliente(sucursal, cliente, cantidad, mesas):

        ultimo = Persistencia.query(Estadia) \
            .order_by(Estadia.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        estado_activa = Persistencia.query(EstadoEstadia).filter(
            EstadoEstadia.nombre == 'activa'
        ).one()

        estadia = Estadia(
            numero=numero,
            fecha_hora_llegada=datetime.now(),
            cantidad_personas=cantidad,
            sucursal=sucursal,
            estado=estado_activa
        )

        if isinstance(mesas, list):
            estadia.mesas += mesas
        else:
            estadia.mesas.append(mesas)

        [ExpMesa.ocupar_mesa(mesa) for mesa in mesas]

        if cliente:
            estadia.clientes.append(cliente)

        Persistencia.add(estadia)

        if Persistencia.commit():
            return estadia, 0
        else:
            return None, 500

    @staticmethod
    def abrir_estadia_mozo(sucursal, cantidad, mesas, clientes=tuple([])):

        ultimo = Persistencia.query(Estadia) \
            .order_by(Estadia.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        estado_activa = Persistencia.query(EstadoEstadia).filter(
            EstadoEstadia.nombre == 'activa'
        ).one()

        estadia = Estadia(
            numero=numero,
            fecha_hora_llegada=datetime.now(),
            cantidad_personas=cantidad,
            mesas=mesas,
            sucursal=sucursal,
            estado=estado_activa
        )

        Persistencia.add(estadia)

        if clientes:
            estadia.clientes += clientes

        if Persistencia.commit():
            return estadia, 0
        else:
            return None, 500

    @staticmethod
    def cerrar_estadia_cliente(cliente):
        estadia = ExpEstadia.ver_estadia_actual_cliente(cliente).one_or_none()
        if estadia is not None and cliente not in estadia.clientes_retirados:
            estadia.clientes_retirados.append(cliente)

        sin_clientes = True
        for cliente in estadia.clientes:
            if cliente not in estadia.clientes_retirados:
                sin_clientes = False
                break

        if sin_clientes:
            estadia.fecha_hora_salida = datetime.now()
            [ExpMesa.liberar_mesa(mesa) for mesa in estadia.mesas]
            estado_cerrada = Persistencia.query(EstadoEstadia).filter(
                EstadoEstadia.nombre == 'cerrada'
            ).one()
            estadia.estado = estado_cerrada

        Persistencia.add(estadia)

        if Persistencia.commit():
            return estadia, 0
        else:
            return None, 500

    @staticmethod
    def cerrar_estadia_mozo(estadia):
        pedidos_pagados = True
        for pedido in estadia.pedidos:
            estado_pedido = ExpPedido.ver_estado_actual_pedido(pedido)
            if estado_pedido.nombre not in ['anulado', 'devuelto', 'pagado']:
                pedidos_pagados = False
                break

        if pedidos_pagados:
            estadia.fecha_hora_salida = datetime.now()
            [ExpMesa.liberar_mesa(mesa) for mesa in estadia.mesas]
            estado_cerrada = Persistencia.query(EstadoEstadia).filter(
                EstadoEstadia.nombre == 'cerrada'
            ).one()
            estadia.estado = estado_cerrada

        Persistencia.add(estadia)

        if Persistencia.commit():
            return estadia, 0
        else:
            return None, 500

    @staticmethod
    def puntuar_sucursal(puntuacion, cliente, sucursal, estadia, comentarios):

        calificacion = PuntuacionSucursal(
            fecha_hora=datetime.now(),
            calificacion=puntuacion,
            cliente=cliente,
            sucursal=sucursal,
            estadia=estadia,
            comentarios=comentarios
        )
        Persistencia.add(calificacion)

        calificaciones = sucursal.puntuacion.split(':')
        calificaciones[puntuacion - 1] += 1
        sucursal.puntuacion = ':'.join(calificaciones)
        Persistencia.add(sucursal)

        if Persistencia.commit():
            return calificacion, 0
        else:
            return None, 500
