from json import loads
from hashlib import sha512

from modelo.persistencia import Persistencia, Entidad

HASH_LEN = 8


class ExpQr:

    @staticmethod
    def decode_qr_string(qr_string, verify=True):
        data = loads(qr_string)

        class_obj = None
        for c in Entidad._decl_class_registry.values():
            if (hasattr(c, '__tablename__')
                    and c.__tablename__ == data.get('object', '')):
                class_obj = c
                break

        data['instance'] = (Persistencia.query(class_obj)
                            .filter(class_obj.oid == data['id']))

        ok = False
        if verify:
            verif_str = []
            for k, v in data.items():
                if k != 'hash' and k != 'instance':
                    verif_str.append(str(v))
            verif_str.sort()
            verif_str = ':'.join(verif_str)
            hash_str = sha512(verif_str.encode()).hexdigest()[:HASH_LEN]

            if hash_str == data['instance'].qr_hash:
                ok = True

        if not verify or ok:
            return data
        else:
            return None

    @staticmethod
    def encode_qr_dict(data):
        verif_str = []
        for k, v in data.items():
                verif_str.append(str(v))
        verif_str.sort()
        verif_str = ':'.join(verif_str)
        data['hash'] = sha512(verif_str.encode()).hexdigest()[:HASH_LEN]

        return data
