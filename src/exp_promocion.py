from datetime import datetime

from exp_sucursal import ExpSucursal
from modelo.entidades import DetallePromocion
from modelo.entidades import EstadoPromocion
from modelo.entidades import Periodicidad
from modelo.entidades import Promocion
from modelo.entidades import WeekMask
from modelo.persistencia import Persistencia


class ExpPromocion:

    @staticmethod
    def get_promocion(numero):
        return (Persistencia.query(Promocion)
                .filter(Promocion.numero == numero)
                .one_or_none())

    @staticmethod
    def ver_promociones(sucursal):
        oferta = ExpSucursal.get_oferta_actual(sucursal)

        if oferta is None:
            return None

        promociones = []
        for promocion in oferta.promociones:
            if promocion.estado.nombre == 'no vigente':
                continue
            promociones.append(promocion)

        return promociones

    @staticmethod
    def ver_promociones_activas(sucursal):
        promociones = ExpPromocion.ver_promociones(sucursal)

        fecha_hora_actual = datetime.now()
        promociones_activas = []

        for promo in promociones:
            anio_desde = datetime.now().year
            if promo.mes_desde > promo.mes_hasta:
                anio_hasta = anio_desde + 1
            else:
                anio_hasta = anio_desde
            inicio_promocion = datetime.strptime(
                str(anio_desde)
                + str(promo.mes_desde.zfill(2))
                + str(promo.dia_desde.zfill(2)),
                '%Y%m%d')
            fin_promocion = datetime.strptime(
                str(anio_hasta)
                + str(promo.mes_hasta.zfill(2))
                + str(promo.dia_hasta.zfill(2)),
                '%Y%m%d')

            if inicio_promocion < fecha_hora_actual < fin_promocion:
                promociones_activas.append(promo)

        return promociones_activas

    @staticmethod
    def ver_periodicidad():
        return Persistencia.get_session().query(Periodicidad).all()

    @staticmethod
    def crear_promocion(nombre, descripcion, precio, descuento,
                        mes_desde, mes_hasta,dia_desde, dia_hasta,
                        hora_desde, hora_hasta,dias_activa, periodicidad,
                        detalle_productos):

        ultimo = Persistencia.get_session().query(Promocion)\
            .order_by(Promocion.numero.desc()).first()

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        estado_vigente = Persistencia.get_session().query(EstadoPromocion)\
            .filter(EstadoPromocion.nombre == 'vigente').one()

        promocion = Promocion(numero=numero,
                              nombre=nombre,
                              estado=estado_vigente)

        if descripcion is not None:
            promocion.descripcion = descripcion

        if precio is not None:
            promocion.precio = precio
        elif descuento is not None:
            promocion.descuento = descuento

        if mes_desde is not None:
            promocion.mes_desde = mes_desde

        if mes_hasta is not None:
            promocion.mes_hasta = mes_hasta

        if dia_desde is not None:
            promocion.dia_desde = dia_desde

        if dia_hasta is not None:
            promocion.dia_hasta = dia_hasta

        if hora_desde is not None:
            promocion.hora_desde = hora_desde

        if hora_hasta is not None:
            promocion.hora_hasta = hora_hasta

        if dias_activa is not None:
            mascara_dias_activa = WeekMask(lunes=dias_activa.lunes,
                                           martes=dias_activa.martes,
                                           miercoles=dias_activa.miercoles,
                                           jueves=dias_activa.jueves,
                                           viernes=dias_activa.viernes,
                                           sabado=dias_activa.sabado,
                                           domingo=dias_activa.domingo)
            Persistencia.get_session().add(mascara_dias_activa)
            promocion.week_day = mascara_dias_activa

        periodicidad_entidad = Persistencia.get_session().query(Periodicidad)\
            .filter(Periodicidad.nombre == periodicidad).one_or_none()
        if periodicidad_entidad is not None:
            promocion.periodicidad = periodicidad_entidad
        else:
            Persistencia.get_session().rollback()
            return None, False

        for producto, cantidad in detalle_productos:
            detalle = DetallePromocion(promocion=promocion, producto=producto,
                                       cantidad=cantidad)
            Persistencia.get_session().add(detalle)

        Persistencia.get_session().add(promocion)
        Persistencia.get_session().commit()

        return promocion, True

    @staticmethod
    def modificar_promocion(numero, nombre, descripcion, precio,
                            descuento, mes_desde, mes_hasta,
                            dia_desde, dia_hasta, hora_desde, hora_hasta,
                            dias_activa, periodicidad, detalle_productos):

        promocion = Persistencia.get_session().query(Promocion)\
            .filter(Promocion.numero == numero).one_or_none()
        if promocion is None:
            return None, False

        if nombre is not None:
            promocion.nombre = nombre

        if descripcion is not None:
            promocion.descripcion = descripcion

        if precio is not None:
            promocion.precio = precio
        elif descuento is not None:
            promocion.descuento = descuento

        if mes_desde is not None:
            promocion.mes_desde = mes_desde

        if mes_hasta is not None:
            promocion.mes_hasta = mes_hasta

        if dia_desde is not None:
            promocion.dia_desde = dia_desde

        if dia_hasta is not None:
            promocion.dia_hasta = dia_hasta

        if hora_desde is not None:
            promocion.hora_desde = hora_desde

        if hora_hasta is not None:
            promocion.hora_hasta = hora_hasta

        if dias_activa is not None:
            mascara_dias_activa = WeekMask(lunes=dias_activa.lunes,
                                           martes=dias_activa.martes,
                                           miercoles=dias_activa.miercoles,
                                           jueves=dias_activa.jueves,
                                           viernes=dias_activa.viernes,
                                           sabado=dias_activa.sabado,
                                           domingo=dias_activa.domingo)
            Persistencia.get_session().add(mascara_dias_activa)
            promocion.week_day = mascara_dias_activa

        periodicidad_entidad = Persistencia.get_session().query(Periodicidad)\
            .filter(Periodicidad.nombre == periodicidad).one_or_none()
        if periodicidad_entidad is not None:
            promocion.periodicidad = periodicidad_entidad
        else:
            Persistencia.get_session().rollback()
            return None, False

        lista_codigos = [detalle.producto.codigo
                         for detalle in promocion.detalles]
        for producto, cantidad in detalle_productos:
            if producto.codigo in lista_codigos:
                detalle = Persistencia.get_session().query(DetallePromocion)\
                    .filter(DetallePromocion.promocion == promocion,
                            DetallePromocion.producto == producto).one()
                detalle.cantidad = cantidad
            else:
                detalle = DetallePromocion(
                    promocion=promocion,
                    producto=producto,
                    cantidad=cantidad)
            Persistencia.get_session().add(detalle)

        Persistencia.get_session().add(promocion)
        Persistencia.get_session().commit()

        return promocion, True

    @staticmethod
    def deshabilitar_promocion(numero):
        promo = Persistencia.get_session().query(Promocion)\
            .filter(Promocion.numero == numero).one_or_none()

        if promo is None:
            return False

        estado_novigente = Persistencia.get_session().query(EstadoPromocion)\
            .filter(EstadoPromocion.nombre == 'no vigente').one()

        promo.estado = estado_novigente

        Persistencia.get_session().add(promo)
        Persistencia.get_session().commit()

        return True

    @staticmethod
    def promocion_agotada(numero):
        promo = Persistencia.get_session().query(Promocion)\
            .filter(Promocion.numero == numero).one_or_none()

        if promo is None:
            return False

        estado_agotada = Persistencia.get_session().query(EstadoPromocion)\
            .filter(EstadoPromocion.nombre == 'agotada').one()

        promo.estado = estado_agotada

        Persistencia.get_session().add(promo)
        Persistencia.get_session().commit()

    @staticmethod
    def get_precio(promocion):
        if promocion.precio is None:
            # TODO: Calcular precio segun descuento
            return 0
        else:
            return promocion.precio
