from modelo.entidades import Producto, CategoriaProducto, EstadoProducto
from modelo.persistencia import Persistencia


class ExpProducto:

    @staticmethod
    def get_producto(codigo):
        # estado = Persistencia.get_session().query(EstadoProducto)
        # .filter(EstadoProducto.nombre == 'disponible').one()
        return Persistencia.get_session().query(Producto)\
            .filter(Producto.codigo == codigo).one_or_none()

    @staticmethod
    def ver_productos(categorias=None):
        estado = Persistencia.get_session().query(EstadoProducto)\
            .filter(EstadoProducto.nombre == 'disponible').one()

        # return Persistencia.get_session().query(Producto)\
        #     .filter(Producto.estado == estado, Producto.categorias.in_(categorias)).all()

        productos = Persistencia.get_session().query(Producto).filter(Producto.estado == estado).all()

        if categorias is None:
            return productos

        productos_filtrados = []
        for p in productos:
            for c in p.categorias:
                if c in categorias:
                    productos_filtrados.append(p)
                    break

        return productos_filtrados

    @staticmethod
    def get_categoria(codigo):
        cat = Persistencia.get_session().query(CategoriaProducto)\
            .filter(CategoriaProducto.codigo == codigo).one_or_none()

        # if cat is None:
        #     cat = Persistencia.get_session().query(CategoriaProducto)\
        #         .filter(CategoriaProducto.nombre == 'indefinido').one()

        return cat

    @staticmethod
    def ver_categorias():
        return Persistencia.get_session().query(CategoriaProducto).all()

    @staticmethod
    def crear_producto(nombre, codigo, foto, categorias, descripcion):
        ultimo = Persistencia.get_session().query(Producto) \
            .order_by(Producto.codigo.desc()).first()

        if codigo is None:
            if ultimo is None:
                codigo = 1
            else:
                codigo = ultimo.codigo + 1
        else:
            prod_codigo = Persistencia.get_session().query(Producto)\
                .filter(Producto.codigo == codigo).one_or_none()
            if prod_codigo is not None:
                return prod_codigo, False

        estado = Persistencia.get_session().query(EstadoProducto)\
            .filter(EstadoProducto.nombre == 'disponible').one()
        producto = Producto(codigo=codigo, nombre=nombre, estado=estado)

        if categorias:
            producto.categorias = categorias

        if foto is not None:
            producto.foto = foto

        if descripcion:
            producto.descripcion = descripcion

        Persistencia.get_session().add(producto)
        Persistencia.get_session().commit()

        return producto, True

    @staticmethod
    def modificar_producto(producto, nombre, foto, categorias, descripcion):
        if nombre is not None:
            producto.nombre = nombre

        if foto is not None:
            producto.foto = foto

        if categorias:
            producto.categorias = categorias

        if descripcion:
            producto.descripcion = descripcion

        Persistencia.get_session().add(producto)
        Persistencia.get_session().commit()

        return producto, True

    @staticmethod
    def eliminar_producto(codigo):
        producto = ExpProducto.get_producto(codigo)

        if producto is None:
            return False, None

        estado_retirado = Persistencia.get_session().query(EstadoProducto)\
            .filter(EstadoProducto.nombre == 'retirado').one()

        producto.estado = estado_retirado

        Persistencia.get_session().add(producto)
        Persistencia.get_session().commit()

        return True, codigo

    @staticmethod
    def crear_categoria(nombre, foto):
        ultimo = Persistencia.get_session().query(CategoriaProducto) \
            .order_by(CategoriaProducto.codigo.desc()).first()

        if ultimo is None:
            codigo = 1
        else:
            codigo = ultimo.codigo + 1

        cat = Persistencia.get_session().query(CategoriaProducto)\
            .filter(CategoriaProducto.nombre == nombre).one_or_none()
        if cat is not None:
            return cat, False

        cat = CategoriaProducto(codigo=codigo, nombre=nombre)

        if foto is not None:
            cat.foto = foto

        Persistencia.get_session().add(cat)
        Persistencia.get_session().commit()

        return cat, True

    @staticmethod
    def modificar_categoria(codigo, nombre, foto):
        cat = Persistencia.get_session().query(CategoriaProducto)\
            .filter(CategoriaProducto.codigo == codigo).one_or_none()
        if cat is None:
            return None, False

        if nombre is not None:
            cat.nombre = nombre

        if foto is not None:
            cat.foto = foto

        Persistencia.get_session().add(cat)
        Persistencia.get_session().commit()

        return cat, True

    @staticmethod
    def eliminar_categoria(codigo):
        cat = Persistencia.get_session().query(CategoriaProducto)\
            .filter(CategoriaProducto.codigo == codigo).one_or_none()
        if cat is None:
            return codigo, False
        else:
            Persistencia.get_session().delete(cat)
            Persistencia.get_session().commit()
            return codigo, True
