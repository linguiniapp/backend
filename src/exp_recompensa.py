from datetime import datetime

from modelo.entidades import Recompensa, DetalleRecompensa, EstadoRecompensa
from modelo.entidades import TransaccionPuntos
from modelo.persistencia import Persistencia


class ExpRecompensa:

    @staticmethod
    def asignar_puntos(cliente, puntos):
        fecha_hora = datetime.now()
        tr = TransaccionPuntos(
            fecha_hora=fecha_hora,
            cantidad=puntos,
            cliente_puntos=cliente
        )
        Persistencia.add(tr)
        cliente.total += puntos
        cliente.fecha_hora_actualizacion = fecha_hora
        Persistencia.add(cliente)

        if Persistencia.commit():
            return tr, 0
        else:
            return None, 500

    @staticmethod
    def get_recompensa(numero):
        return (Persistencia.query(Recompensa)
                .filter(Recompensa.numero == numero)
                .one_or_none())

    @staticmethod
    def ver_recompensas():

        estados = [
            (Persistencia.query(EstadoRecompensa)
             .filter(EstadoRecompensa.nombre == 'vigente')).one().oid,
            (Persistencia.query(EstadoRecompensa)
             .filter(EstadoRecompensa.nombre == 'agotada')).one().oid
        ]

        return (Persistencia.query(Recompensa)
                .filter(Recompensa.estado_oid.in_(estados)).all())

    @staticmethod
    def crear_recompensa(nombre, puntos, detalles):
        estado_vigente = (Persistencia.query(EstadoRecompensa)
                          .filter(EstadoRecompensa.nombre == 'vigente')).one()

        ultimo = (Persistencia.query(Recompensa)
                  .order_by(Recompensa.numero.desc()).first())

        if ultimo is None:
            numero = 1
        else:
            numero = ultimo.numero + 1

        recompensa = Recompensa(
            numero=numero,
            nombre=nombre,
            puntos=puntos,
            estado=estado_vigente
        )

        Persistencia.add(recompensa)

        for producto, cantidad in detalles:
            detalle_recompensa = DetalleRecompensa(
                producto=producto,
                cantidad=cantidad,
                recompensa=recompensa
            )
            Persistencia.add(detalle_recompensa)

        if Persistencia.commit():
            return recompensa, 0
        else:
            return None, 500
