from json import dumps, loads
from datetime import datetime
from decimal import Decimal

from exp_pedido import ExpPedido
from exp_qr import ExpQr
from exp_sucursal import ExpSucursal
from exp_recompensa import ExpRecompensa
from modelo.entidades import DTOPreFactura, DTOPreFacturaDetalle
from modelo.entidades import Factura, DetalleFactura
from modelo.entidades import TipoFactura, EstadoFactura
from modelo.entidades import MedioPago, Cuotas
from modelo.entidades import PedidoEstado, EstadoPedido
from modelo.entidades import Restaurante
from modelo.entidades import Cliente
from modelo.persistencia import Persistencia
from pago_electronico.fact_pago_electronico import FactPagoElectronico

PESOS_POR_PUNTO = 100


class ExpPago:

    @staticmethod
    def emitir_prefactura(estadia):

        pedidos = ExpPedido.ver_pedidos_impagos(estadia)

        total = Decimal('0.00')
        detalles = []
        for pedido in pedidos:
            for detalle in pedido:
                detalle_pedido = DTOPreFacturaDetalle(
                    nombre=detalle.carta_producto.producto.nombre,
                    precio=detalle.carta_producto.precio,
                    cantidad=detalle.cantidad)
                detalle_pedido.subtotal = (detalle_pedido.precio
                                           * detalle_pedido.cantidad)
                detalles.append(detalle_pedido)
                total += detalle_pedido.subtotal

        mesas = [m.numero for m in estadia.mesas]

        prefactura = DTOPreFactura(
            sucursal=estadia.sucursal.numero,
            estadia=estadia.numero,
            mesas=mesas,
            detalles=detalles,
            total=total
        )

        return prefactura, 0

    @staticmethod
    def realizar_pago(pedidos, medios_pago, pago_por_caja=False):

        pagado = Decimal('0.00')
        # total_pedido = Decimal('0.00')

        datos_factura = {}
        facturas_descontadas = []
        for pedido in pedidos:
            for factura in pedido.facturas:
                if factura not in facturas_descontadas:
                    facturas_descontadas.append(factura)
                    pagado += factura.total
            for det in pedido.detalles:
                cod_prod = 'prod' + str(det.carta_producto.producto.codigo)
                if cod_prod in datos_factura.keys():
                    datos_factura[cod_prod] = {
                        'cantidad': (
                                datos_factura[cod_prod]['cantidad']
                                + det.cantidad
                        ),
                        'subtotal': (
                                datos_factura[cod_prod]['subtotal']
                                + det.cantidad * det.carta_producto.precio
                        )
                    }
                else:
                    datos_factura[cod_prod] = {
                        'nombre': det.carta_producto.producto.nombre,
                        'precio': det.carta_producto.precio,
                        'cantidad': det.cantidad,
                        'subtotal': det.cantidad * det.carta_producto.precio
                    }
            for prom in pedido.promociones:
                cod_prom = 'prom_' + str(prom.promocion.numero)
                if cod_prom in datos_factura.keys():
                    datos_factura[cod_prom] = {
                        'cantidad': (
                                datos_factura[cod_prom]['cantidad']
                                + prom.cantidad
                        ),
                        'subtotal': (
                                datos_factura[cod_prom]['subtotal']
                                + prom.cantidad * prom.promocion.precio
                        )
                    }
                else:
                    datos_factura[cod_prom] = {
                        'nombre': prom.promocion.nombre,
                        'precio': prom.promocion.precio,
                        'cantidad': prom.cantidad,
                        'subtotal': prom.cantidad * prom.promocion.precio
                    }
            for rec in pedido.recompensas:
                cod_rec = 'rec_' + str(rec.recompensa.numero)
                if cod_rec in datos_factura.keys():
                    datos_factura[cod_rec] = {
                        'cantidad': (
                                datos_factura[cod_rec]['cantidad']
                                + rec.cantidad
                        ),
                        'subtotal': Decimal('0.00')
                    }
                else:
                    datos_factura[cod_rec] = {
                        'nombre': rec.recompensa.nombre,
                        'precio': Decimal('0.00'),
                        'cantidad': rec.cantidad,
                        'subtotal': Decimal('0.00')
                    }

        total_pedido = sum([df['subtotal'] for df in datos_factura.values()])

        datos_pago = []
        for mp in medios_pago:
            medio_pago = (Persistencia.query(MedioPago)
                          .filter(MedioPago.codigo == mp.codigo)).one_or_none()
            if medio_pago is None:
                return None, 404

            if mp.cuotas and mp.cuotas > 1:
                cuotas_mp = (Persistencia.query(Cuotas)
                             .filter(Cuotas.medio_pago == medio_pago)).all()

                cantidad_cuotas = 0
                recargo_cuotas = Decimal('0.00')
                for cuotas in cuotas_mp:
                    if cuotas.cantidad == mp.cuotas:
                        cantidad_cuotas = cuotas.cantidad
                        recargo_cuotas = cuotas.cft / 12 * cuotas.cantidad
                        break
                if not cantidad_cuotas:
                    return None, 400
            else:
                cantidad_cuotas = 1
                recargo_cuotas = Decimal('0.00')

            pago = {
                'medio_pago': medio_pago,
                'importe': Decimal(mp.importe).quantize(Decimal('1.00')),
                'cuotas': cantidad_cuotas,
                'recargo': recargo_cuotas,
                'tipo_factura': mp.datos_factura.tipo_factura,
                'cliente_nombre_factura': mp.datos_factura.nombre,
                'cliente_apellido_factura': mp.datos_factura.apellido,
                'id_cliente': mp.datos_factura.id_cliente,
                'tipo_id_cliente': mp.datos_factura.tipo_id_cliente
            }

            if mp.dni_cuenta_puntos:
                cliente = (
                    Persistencia.query(Cliente)
                    .filter(Cliente.dni == mp.dni_cuenta_puntos)
                    .one_or_none()
                )
                if not cliente:
                    return None, 404
                pago['cuenta_puntos'] = cliente.puntos
            elif mp.cuenta_puntos:
                pago['cuenta_puntos'] = mp.cuenta_puntos

            if mp.datos_pago:
                pago['extra'] = mp.datos_pago

            if mp.datos_factura.tipo_factura == 'A':
                if mp.datos_factura.tipo_id_cliente != 'cuit':
                    return None, 400

            datos_pago.append(pago)

        estadia = pedidos[0].estadia
        sucursal = estadia.sucursal
        restaurante = (Persistencia.query(Restaurante)
                       .filter(Restaurante.sucursales.contains(sucursal))
                       .one_or_none())
        cuit_empresa = restaurante.cuit

        estado_cobrada = (
            Persistencia.query(EstadoFactura)
                .filter(EstadoFactura.nombre == 'cobrada')
                .one()
        )

        estado_emitida = (Persistencia.query(EstadoFactura)
                          .filter(EstadoFactura.nombre == 'emitida').one())

        facturas = []
        importe_impago = Decimal('0.00')
        acumulado = Decimal('0.00')
        for pago in datos_pago:
            ultima = (Persistencia.query(Factura)
                      .order_by(Factura.numero.desc()).first())
            if ultima:
                numero = ultima.numero + 1
            else:
                numero = 1

            tipo_factura = (
                Persistencia.query(TipoFactura)
                .filter(TipoFactura.nombre == pago.get('tipo_factura'))
                .one_or_none()
            )
            if not tipo_factura:
                Persistencia.rollback()
                return None, 400

            importe_factura = (pago.get('importe')
                               * (1 + pago.get('recargo', 0) / 100))

            fecha = datetime.now().date()

            factura = Factura(
                numero=numero,
                fecha=fecha,
                total=importe_factura,
                cantidad_cuotas=pago.get('cuotas'),
                cuit_empresa=cuit_empresa,
                nombre_cliente=pago.get('cliente_nombre_factura'),
                apellido_cliente=pago.get('cliente_apellido_factura'),
                id_cliente=pago.get('id_cliente'),
                tipo_id_cliente=pago.get('tipo_id_cliente'),
                pedidos=pedidos,
                tipo=tipo_factura,
                medio_pago=pago.get('medio_pago'),
                estado=estado_emitida,
                email_cliente=pago.get('cliente_email'),
                estadia=estadia
            )
            Persistencia.add(factura)
            facturas.append(factura)

            for df in datos_factura.values():
                detalle = DetalleFactura(
                    descripcion=df['nombre'],
                    precio_unitario=df['precio'],
                    cantidad=df['cantidad'],
                    subtotal=df['subtotal'],
                    factura=factura
                )
                Persistencia.add(detalle)

            importe_impago = (total_pedido
                              - pagado
                              - pago.get('importe')
                              - acumulado)
            if importe_impago > 0:
                impago = DetalleFactura(
                    descripcion='Importe restante',
                    precio_unitario=0,
                    cantidad=0,
                    subtotal=-importe_impago,
                    factura=factura
                )
                Persistencia.add(impago)

            if pago.get('cuotas') > 1:
                recargo = DetalleFactura(
                    descripcion='Recargo por cuotas',
                    precio_unitario=0,
                    cantidad=0,
                    subtotal=pago.get('importe') * pago.get('recargo', 0)/100,
                    factura=factura
                )
                Persistencia.add(recargo)

            acumulado += pago.get('importe')

            if pago['medio_pago'].tipo.nombre == 'efectivo':
                # Pago manual por caja (factura emitida)
                if pago_por_caja:
                    factura.estado = estado_cobrada
                    Persistencia.add(factura)
                    caja = ExpSucursal.ver_caja_activa(
                        factura.estadia.sucursal)
                    if not caja:
                        return None, 500
                    caja.monto_final_estimado += factura.total
            else:
                # Pago automático online para el resto de los medios de pago
                adap = FactPagoElectronico.get_adaptador(
                    pago.get('medio_pago'))
                try:
                    params = loads(pago.get('extra', 'null').encode())
                except Exception as e:
                    print(e)
                    return None, 400
                ret = adap.efectuar_pago(factura, params)
                if not ret:
                    factura.estado = estado_cobrada
                    Persistencia.add(factura)
                    if pago.get('cuenta_puntos'):
                        ExpRecompensa.asignar_puntos(
                            pago.get('cuenta_puntos'),
                            round(pago.get('importe') / PESOS_POR_PUNTO)
                        )
                else:
                    Persistencia.rollback()
                    return None, ret

        if (importe_impago < 0
                and not ('emitida' in [f.estado.nombre for f in facturas])):
            estado_pedido = (Persistencia.query(EstadoPedido)
                             .filter(EstadoPedido.nombre == 'pagado').one())
        else:
            estado_pedido = (Persistencia.query(EstadoPedido)
                             .filter(EstadoPedido.nombre == 'pago parcial')
                             .one())

        for pedido in pedidos:
            Persistencia.add(PedidoEstado(
                fecha_hora=datetime.now(),
                estado=estado_pedido,
                pedido=pedido
            ))

        if Persistencia.commit():
            if len(facturas) == 1:
                factura = facturas[0]
                qr_dict = ExpQr.encode_qr_dict({
                    'object': 'factura',
                    'id': factura.oid,
                    'numero': factura.numero,
                    'fecha': str(factura.fecha),
                    'importe': str(factura.total),
                    'nombre_cliente': factura.nombre_cliente
                })
                factura.qr_hash = qr_dict['hash']
                Persistencia.add(factura)
                ret = (factura, dumps(qr_dict))
            else:
                ret = facturas
        else:
            return None, 500

        if Persistencia.commit():
            return ret, 0
        else:
            return None, 500

    @staticmethod
    def acreditar_efectivo(monto, factura):
        monto = Decimal(monto).quantize(Decimal('1.00'))

        if monto < factura.total:
            return None, 400
        else:
            factura.estado = (Persistencia.query(EstadoFactura)
                              .filter(EstadoFactura.nombre == 'cobrada')
                              .one())
            vuelto = monto - factura.total
            caja = ExpSucursal.ver_caja_activa(factura.estadia.sucursal)
            caja.monto_final_estimado += factura.total
            if not caja:
                return None, 500

        if Persistencia.commit():
            return vuelto, 0
        else:
            return None, 500

    @staticmethod
    def leer_codigo_pago(codigo_pago):
        return (Persistencia.query(Factura)
                .filter(Factura.qr_hash == codigo_pago)
                .one_or_none())
