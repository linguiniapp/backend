FROM python:3.7-alpine

MAINTAINER "andresdb91@gmail.com"

RUN apk update && apk add --virtual build-dependencies build-base

RUN pip install gunicorn

RUN mkdir /usr/src/linguiniapp
WORKDIR /usr/src/linguiniapp

COPY src/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8000

CMD [ "gunicorn", "-b 0.0.0.0:80", "main:app"  ]
